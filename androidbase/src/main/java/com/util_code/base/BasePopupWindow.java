package com.util_code.base;

import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import com.util_code.R;
import com.util_code.utils.DisplayUtils;

public class BasePopupWindow extends PopupWindow {
    private View mContentView;
    private View mParentView;
    private boolean isOutsideTouch;
    private boolean isFocus;
    private Drawable mBackgroundDrawable;
    private int mAnimationStyle;
    private boolean isWidthWrap;
    private boolean isHeightWrap;

    private BasePopupWindow(Builder builder) {
        this.mContentView = builder.contentView;
        this.mParentView = builder.parentView;
        this.isOutsideTouch = builder.isOutsideTouch;
        this.isFocus = builder.isFocus;
        this.mBackgroundDrawable = builder.backgroundDrawable;
        this.mAnimationStyle = builder.animationStyle;
        this.isWidthWrap = builder.isWidthWrap;
        this.isHeightWrap = builder.isHeightWrap;
        initLayout();
    }

    public static Builder builder() {
        return new Builder();
    }

    private void initLayout() {
        setWidth(isWidthWrap ? ViewGroup.LayoutParams.WRAP_CONTENT : ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(isHeightWrap ? ViewGroup.LayoutParams.WRAP_CONTENT : ViewGroup.LayoutParams.MATCH_PARENT);
        setFocusable(isFocus);
        setOutsideTouchable(isOutsideTouch);
        setBackgroundDrawable(mBackgroundDrawable);
        if (mAnimationStyle != -1) {
            setAnimationStyle(mAnimationStyle);
        } else {
            setAnimationStyle(R.style.PopupAnimation);
        }
        setContentView(mContentView);
    }

    public View getContentView() {
        return mContentView;
    }

    public static View inflateView(ContextThemeWrapper context, int layoutId) {
        return LayoutInflater.from(context).inflate(layoutId, null);
    }

    public void show() {
        if (Build.VERSION.SDK_INT < 24) {
            showAsDropDown(mParentView, DisplayUtils.dp2px(0), DisplayUtils.dp2px(0));
        } else if (Build.VERSION.SDK_INT == 24) {
            // 适配 android 7.0
            int[] location = new int[2];
            mParentView.getLocationOnScreen(location);
            int x = location[0];
            int y = location[1];
            showAtLocation(mParentView, Gravity.NO_GRAVITY, DisplayUtils.dp2px(0), y + mParentView
                    .getHeight() + DisplayUtils.dp2px(0));
        } else {
            // 适配 android 7.1.1+
            View rootView = mParentView.getRootView();
            Rect rect = new Rect();
            rootView.getWindowVisibleDisplayFrame(rect);
            int[] xy = new int[2];
            mParentView.getLocationInWindow(xy);
            int anchorY = xy[1] + mParentView.getHeight();
            int height = rect.bottom - anchorY;
            if (!isHeightWrap) {
                setHeight(height);
            }
            showAtLocation(mParentView, Gravity.NO_GRAVITY, 0, anchorY);
        }
    }

    public static final class Builder {
        private View contentView;
        private View parentView;
        private boolean isOutsideTouch = true;//默认为true
        private boolean isFocus = true;//默认为true
        private Drawable backgroundDrawable = new ColorDrawable(0x00000000);//默认为透明
        private int animationStyle = -1;
        private boolean isWidthWrap;
        private boolean isHeightWrap;

        private Builder() {
        }

        public Builder contentView(View contentView) {
            this.contentView = contentView;
            return this;
        }

        public Builder parentView(View parentView) {
            this.parentView = parentView;
            return this;
        }

        public Builder isWidthWrap(boolean isWrap) {
            this.isWidthWrap = isWrap;
            return this;
        }

        public Builder isHeightWrap(boolean isWrap) {
            this.isHeightWrap = isWrap;
            return this;
        }

        public Builder isOutsideTouch(boolean isOutsideTouch) {
            this.isOutsideTouch = isOutsideTouch;
            return this;
        }

        public Builder isFocus(boolean isFocus) {
            this.isFocus = isFocus;
            return this;
        }

        public Builder backgroundDrawable(Drawable backgroundDrawable) {
            this.backgroundDrawable = backgroundDrawable;
            return this;
        }

        public Builder animationStyle(int animationStyle) {
            this.animationStyle = animationStyle;
            return this;
        }

        public BasePopupWindow build() {
            if (contentView == null) {
                throw new IllegalStateException("contentView is required");
            }

            return new BasePopupWindow(this);
        }
    }
}

