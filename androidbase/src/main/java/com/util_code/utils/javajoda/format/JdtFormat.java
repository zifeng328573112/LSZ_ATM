package com.util_code.utils.javajoda.format;


import com.util_code.utils.javajoda.DateTimeStamp;
import com.util_code.utils.javajoda.JDateTime;

/**
 * Immutable format-formatter pair.
 */
public class JdtFormat {

    protected final String format;
    protected final JdtFormatter formatter;

    public JdtFormat(final JdtFormatter formatter, final String format) {
        this.format = format;
        this.formatter = formatter;
    }

    /**
     * Returns format.
     */
    public String getFormat() {
        return format;
    }

    /**
     * Returns formatter.
     */
    public JdtFormatter getFormatter() {
        return formatter;
    }


    /**
     * Delegates for {@link jodd.datetime.format.JdtFormatter#convert(jodd.datetime.JDateTime,
     * * String)}.
     */
    public String convert(final JDateTime jdt) {
        return formatter.convert(jdt, format);
    }

    /**
     * Delegates for {@link jodd.datetime.format.JdtFormatter#parse(String, String)}.
     */
    public DateTimeStamp parse(final String value) {
        return formatter.parse(value, format);
    }
}
