package com.util_code.utils.javajoda;

import java.util.TimeZone;

/**
 * Misc timezone utilities.
 */
public class TimeZoneUtil {

    /**
     * Returns raw offset difference in milliseconds.
     */
    public static int getRawOffsetDifference(final TimeZone from, final TimeZone to) {
        int offsetBefore = from.getRawOffset();
        int offsetAfter = to.getRawOffset();
        return offsetAfter - offsetBefore;
    }

    /**
     * Returns offset difference in milliseconds for given time.
     */
    public static int getOffsetDifference(final long now, final TimeZone from, final TimeZone to) {
        int offsetBefore = from.getOffset(now);
        int offsetAfter = to.getOffset(now);
        return offsetAfter - offsetBefore;
    }

    /**
     * Get offset difference in milliseconds for given jdatetime.
     */
    public static int getOffset(final JDateTime jdt, final TimeZone tz) {
        return tz.getOffset(jdt.getEra(), jdt.getYear(), jdt.getMonth() - 1, jdt.getDay(), TimeUtil
                .toCalendarDayOfWeek(jdt.getDayOfWeek()), jdt.getMillisOfDay());
    }

    public static int getOffsetDifference(final JDateTime jdt, final TimeZone from, final TimeZone to) {
        int offsetBefore = getOffset(jdt, from);
        int offsetAfter = getOffset(jdt, to);
        return offsetAfter - offsetBefore;
    }
}
