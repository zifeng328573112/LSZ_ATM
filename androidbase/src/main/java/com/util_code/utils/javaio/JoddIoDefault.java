package com.util_code.utils.javaio;

import com.util_code.utils.javajoda.StringPool;

public class JoddIoDefault {

    public static String tempFilePrefix = "andbase-";
    public static String encoding = StringPool.UTF_8;
    public static int ioBufferSize = 16384;
}
