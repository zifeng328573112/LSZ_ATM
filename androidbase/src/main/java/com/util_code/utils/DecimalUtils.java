package com.util_code.utils;

import java.text.DecimalFormat;

public class DecimalUtils {

    public static String formatDecimalWithZero(double num, int newScale) {
        String pattern = "0.";
        for (int i = 0; i < newScale; i++) {
            pattern += "0";
        }
        DecimalFormat df = new DecimalFormat(pattern);
        return df.format(num);
    }

    public static String formatDecimalWithZero(String numstr, int newScale) {
        String pattern = "0.";
        for (int i = 0; i < newScale; i++) {
            pattern += "0";
        }
        DecimalFormat df = new DecimalFormat(pattern);
        return df.format(Double.valueOf(numstr));
    }

    public static String formatDecimal(double num, int newScale) {
        String pattern = "#.";
        for (int i = 0; i < newScale; i++) {
            pattern += "#";
        }
        DecimalFormat df = new DecimalFormat(pattern);
        return df.format(num);
    }

    public static String formatDecimal(String numstr, int newScale) {
        String pattern = "#.";
        for (int i = 0; i < newScale; i++) {
            pattern += "#";
        }
        DecimalFormat df = new DecimalFormat(pattern);
        return df.format(Double.valueOf(numstr));
    }

    public static String formatThousandthDecimal(double num, int newScale) {
        String pattern = ",##0.";
        for (int i = 0; i < newScale; i++) {
            pattern += "#";
        }
        DecimalFormat df = new DecimalFormat(pattern);
        return df.format(num);
    }

    public static String formatThousandthDecimal(String numstr, int newScale) {
        String pattern = ",##0.";
        for (int i = 0; i < newScale; i++) {
            pattern += "#";
        }
        DecimalFormat df = new DecimalFormat(pattern);
        return df.format(Double.valueOf(numstr));
    }

}
