package com.util_code.okhttp;

public interface ResponProgressListener {

    void onResponseProgress(long bytesRead, long contentLength, boolean done);
}
