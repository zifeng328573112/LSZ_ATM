package com.util_code.okhttp.callback;

import com.util_code.okhttp.RequestProgressListener;
import com.util_code.okhttp.ResponProgressListener;

public interface HttpCallback<T> extends RequestProgressListener, ResponProgressListener {

    void handleResponse(T response);

    boolean handleException(Throwable throwable);
}
