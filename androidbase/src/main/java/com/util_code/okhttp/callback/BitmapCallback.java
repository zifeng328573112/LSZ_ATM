package com.util_code.okhttp.callback;

import android.graphics.Bitmap;

public abstract class BitmapCallback implements HttpCallback<Bitmap> {

    @Override
    public void onRequestProgress(long bytesWritten, long contentLength, long networkSpeed) {

    }

    @Override
    public void onResponseProgress(long bytesRead, long contentLength, boolean done) {

    }
}
