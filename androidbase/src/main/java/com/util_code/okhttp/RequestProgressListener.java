package com.util_code.okhttp;

public interface RequestProgressListener {
    void onRequestProgress(long bytesWritten, long contentLength, long networkSpeed);
}
