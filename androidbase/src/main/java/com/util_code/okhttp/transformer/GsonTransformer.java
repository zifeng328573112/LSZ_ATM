package com.util_code.okhttp.transformer;


import com.google.gson.Gson;

import java.io.IOException;
import java.lang.reflect.Type;

import okhttp3.Response;

public class GsonTransformer<T> implements HttpTransformer<T> {
    private Type mType;
    private Gson mGson;

    public GsonTransformer(final Type type) {
        this.mType = type;
        mGson = new Gson();
    }

    @Override
    public T transform(final Response response) throws IOException {
        return mGson.fromJson(response.body().string(), mType);
    }
}
