package com.util_code.okhttp.transformer;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.IOException;

import okhttp3.Response;

public class BitmapTransformer implements HttpTransformer<Bitmap> {
    @Override
    public Bitmap transform(final Response response) throws IOException {
        return BitmapFactory.decodeStream(response.body().byteStream());
    }
}
