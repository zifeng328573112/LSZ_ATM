package com.util_code.oklog;

import com.util_code.oklog.BaseLogDataInterceptor.RequestLogData;
import com.util_code.oklog.BaseLogDataInterceptor.ResponseLogData;
import com.util_code.room.NetLogInfo;
import com.util_code.room.NetLogInfoDataSource;
import com.util_code.utils.AndroidUtils;
import com.util_code.utils.DateUtils;
import com.util_code.utils.StringUtils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class OkLogInterceptor implements Interceptor {

    private final LogDataInterceptor logDataInterceptor;

    public OkLogInterceptor() {
        this.logDataInterceptor = new LogDataInterceptor();
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        RequestLogData<Request> requestLogData = logDataInterceptor.processRequest(chain);
        LogDataBuilder logDataBuilder = requestLogData.getLogData();
        NetLogInfo netLogInfo = new NetLogInfo();
        long startNs = System.nanoTime();
        Response response;
        try {
            response = chain.proceed(requestLogData.getRequest());
        } catch (Exception e) {
            logDataBuilder.requestFailed();
            /*------respone error--------*/
            logDataBuilder.responseBody("<-- HTTP FAILED: " + e.getMessage());
            netLogInfo.setRequesturl(logDataBuilder.getRequestUrl());
            netLogInfo.setRequestmethod(logDataBuilder.getRequestMethod());
            netLogInfo.setRequestbody(StringUtils.isEmpty(logDataBuilder.getRequestBody()) ? "" : logDataBuilder
                    .getRequestBody());
            netLogInfo.setRequestdate(DateUtils.getCurrentDate(DateUtils.dateFormatYMD));
            netLogInfo.setResponebody(logDataBuilder.toString());
            NetLogInfoDataSource.getInstance(AndroidUtils.getContext())
                    .insertOrUpdateNetLogInfo(netLogInfo);
            throw e;
        }
        long tookMs = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startNs);
        logDataBuilder.responseDurationMs(tookMs);

        ResponseLogData<Response> responseLogData = logDataInterceptor.processResponse(logDataBuilder, response);
        /*------respone success--------*/
        netLogInfo.setRequesturl(logDataBuilder.getRequestUrl());
        netLogInfo.setRequestmethod(logDataBuilder.getRequestMethod());
        netLogInfo.setRequestbody(StringUtils.isEmpty(logDataBuilder.getRequestBody()) ? "" : logDataBuilder
                .getRequestBody());
        netLogInfo.setRequestdate(DateUtils.getCurrentDate(DateUtils.dateFormatYMD));
        netLogInfo.setResponebody(logDataBuilder.toString());
        if (logDataBuilder.getResponseBodySize() <= 32 * 1024 && logDataBuilder.getRequestContentLength() <= 32 * 1024/*Max 32K*/) {
            NetLogInfoDataSource.getInstance(AndroidUtils.getContext())
                    .insertOrUpdateNetLogInfo(netLogInfo);
        }
        return responseLogData.getResponse();
    }

}
