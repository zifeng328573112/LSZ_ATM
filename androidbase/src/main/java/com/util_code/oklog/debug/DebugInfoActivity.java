package com.util_code.oklog.debug;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.paging.PagedList;
import androidx.paging.RxPagedListBuilder;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.util_code.R;
import com.util_code.base.BaseActivity;
import com.util_code.base.mvp.MvpView;
import com.util_code.room.NetLogInfo;
import com.util_code.room.NetLogInfoDataSource;
import com.util_code.room.RoomDataBase;
import com.util_code.utils.AndroidUtils;
import com.util_code.utils.DateUtils;
import com.util_code.utils.RxUtils;
import com.util_code.utils.StatusBarHelper;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;

public class DebugInfoActivity extends BaseActivity implements View.OnClickListener, MvpView {

    protected View toolbar_back;
    protected TextView toolbar_title;
    protected TextView clear_data;
    protected RecyclerView rcv_list_id;

    // DebugInfoListAdapter mDebugInfoListAdapter;
    DebugInfoPageListAdapter mDebugInfoPageListAdapter;

    private static final int pageSize = 20;
    Flowable<PagedList<NetLogInfo>> mFlowablePagedList;

    ArrayList<String> mWhitelist = new ArrayList<>();

    @Override
    protected int getLayoutId() {
        return R.layout.activity_debuginfo_layout;
    }

    @Override
    protected void setupView() {
        StatusBarHelper.translateStatusBar(DebugInfoActivity.this);
        if (getIntent().getStringArrayListExtra("whitelist") != null) {
            mWhitelist.addAll(getIntent().getStringArrayListExtra("whitelist"));
        }

        toolbar_back = findViewById(R.id.toolbar_back);
        toolbar_title = findViewById(R.id.toolbar_title);
        clear_data = findViewById(R.id.clear_data);
        rcv_list_id = findViewById(R.id.rcv_list_id);

        toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar_title.setText("调试信息");

        clear_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NetLogInfoDataSource.getInstance(AndroidUtils.getContext())
                        .deleteAllNetLogInfo();
            }
        });

        PagedList.Config config = new PagedList.Config.Builder().setPageSize(pageSize)
                .setInitialLoadSizeHint(pageSize * 2)
                .setEnablePlaceholders(false)
                .build();

        mFlowablePagedList = new RxPagedListBuilder<>(RoomDataBase.getInstance(AndroidUtils
                .getContext())
                .netLogInfoDao()
                .loadDataSource(DateUtils.getCurrentDate(DateUtils.dateFormatYMD)), config)
                .buildFlowable(BackpressureStrategy.LATEST);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(DebugInfoActivity.this);
        rcv_list_id.setLayoutManager(linearLayoutManager);
        // mDebugInfoListAdapter = new DebugInfoListAdapter(DebugInfoActivity.this);
        // rcv_list_id.setAdapter(mDebugInfoListAdapter);
        mDebugInfoPageListAdapter = new DebugInfoPageListAdapter();
        rcv_list_id.setAdapter(mDebugInfoPageListAdapter);
    }

    @SuppressLint("CheckResult")
    @Override
    protected void setupData(Bundle savedInstanceState) {
        /*DebugInfoDataSource.getInstance(Global.getContext())
                .getDebugInfo()
                .toObservable()
                .compose(RxUtils.<List<DebugInfo>>applySchedulersLifeCycle((MvpView) this))
                .subscribe(new Observer<PagedList<DebugInfo>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(PagedList<DebugInfo> debugInfos) {
                        mDebugInfoListAdapter.resetData(debugInfos);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });*/

        mFlowablePagedList.toObservable()
                .compose(RxUtils.<PagedList<NetLogInfo>>applySchedulersLifeCycle((MvpView) this))
                .subscribe(new Observer<PagedList<NetLogInfo>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(PagedList<NetLogInfo> netLogInfos) {
                        mDebugInfoPageListAdapter.submitList(netLogInfos);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });

        RoomDataBase.getInstance(AndroidUtils.getContext())
                .netLogInfoDao()
                .loadAll()
                .toObservable()
                .flatMap(new Function<List<NetLogInfo>, Observable<NetLogInfo>>() {

                    @Override
                    public Observable<NetLogInfo> apply(List<NetLogInfo> netLogInfos) throws
                            Exception {
                        return Observable.fromArray(netLogInfos.toArray(new NetLogInfo[netLogInfos
                                .size()]));
                    }
                })
                .filter(new Predicate<NetLogInfo>() {
                    @Override
                    public boolean test(NetLogInfo netLogInfo) throws Exception {
                        if (mWhitelist.size() > 0) {
                            for (String whiteurl : mWhitelist) {
                                if (netLogInfo.getRequesturl().contains(whiteurl)) {
                                    RoomDataBase.getInstance(AndroidUtils.getContext())
                                            .netLogInfoDao()
                                            .delete(netLogInfo);
                                    return false;
                                }
                            }
                        }
                        return true;
                    }
                })
                .compose(RxUtils.<NetLogInfo>applySchedulersLifeCycle((MvpView) this))
                .subscribe(new Observer<NetLogInfo>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(NetLogInfo netLogInfo) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void onClick(View v) {

    }
}
