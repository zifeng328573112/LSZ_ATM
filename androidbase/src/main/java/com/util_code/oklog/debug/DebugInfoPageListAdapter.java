package com.util_code.oklog.debug;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.util_code.R;
import com.util_code.room.NetLogInfo;

public class DebugInfoPageListAdapter extends PagedListAdapter<NetLogInfo, DebugInfoPageListAdapter.DebugPageListHolder> {

    public DebugInfoPageListAdapter() {
        super(DIFF_CALLBACK);
    }

    @NonNull
    @Override
    public DebugPageListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DebugPageListHolder(View.inflate(parent.getContext(), R.layout.debug_list_item, null));
    }

    @Override
    public void onBindViewHolder(@NonNull DebugPageListHolder holder, int position) {
        ((DebugPageListHolder) holder).tv_debug_info_id.setText(getItem(position).getResponebody());
    }

    public class DebugPageListHolder extends RecyclerView.ViewHolder {

        TextView tv_debug_info_id;

        DebugPageListHolder(View itemView) {
            super(itemView);
            setupView();
        }

        private void setupView() {
            tv_debug_info_id = itemView.findViewById(R.id.tv_debug_info_id);
        }
    }

    public static final DiffUtil.ItemCallback<NetLogInfo> DIFF_CALLBACK = new DiffUtil.ItemCallback<NetLogInfo>() {

        @Override
        public boolean areItemsTheSame(NetLogInfo oldItem, NetLogInfo newItem) {
            return oldItem.getResponebody().equals(newItem.getResponebody());
        }

        @Override
        public boolean areContentsTheSame(NetLogInfo oldItem, NetLogInfo newItem) {
            return oldItem.getResponebody().equals(newItem.getResponebody());
        }
    };

}
