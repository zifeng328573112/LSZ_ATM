package com.util_code.oklog.debug;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.util_code.R;
import com.util_code.room.DebugInfo;
import com.util_code.widget.recycleadpter.BaseRecycleViewAdapter;

public class DebugInfoListAdapter extends BaseRecycleViewAdapter<DebugInfo> {

    Context mContext;

    public DebugInfoListAdapter(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, DebugInfo item) {
        ((DebugListHolder) holder).tv_debug_info_id.setText(item.getDebuginfo());
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return new DebugListHolder(View.inflate(mContext, R.layout.debug_list_item, null));
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    class DebugListHolder extends RecyclerView.ViewHolder {

        TextView tv_debug_info_id;

        DebugListHolder(View itemView) {
            super(itemView);
            setupView();
        }

        private void setupView() {
            tv_debug_info_id = itemView.findViewById(R.id.tv_debug_info_id);
        }
    }

}
