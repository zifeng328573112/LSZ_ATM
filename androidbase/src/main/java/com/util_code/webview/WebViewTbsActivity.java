package com.util_code.webview;

import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.util_code.BuildConfig;
import com.util_code.R;
import com.util_code.base.BaseActivity;
import com.util_code.utils.AndroidUtils;
import com.util_code.utils.DisplayUtils;
import com.tencent.smtt.export.external.interfaces.SslError;
import com.tencent.smtt.export.external.interfaces.SslErrorHandler;
import com.tencent.smtt.sdk.WebChromeClient;
import com.tencent.smtt.sdk.WebSettings;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;

import java.util.HashMap;

public class WebViewTbsActivity extends BaseActivity {
    public static final String WEBVIEW_URL = "webview_url";
    public static final String WEBVIEW_TITLE = "webview_title";

    TextView toolbar_title;
    View toolbar_back;
    protected WebView webview;
    protected TextView progress;

    protected String mUrl;
    protected HashMap<String, String> mWebViewHeader;
    protected String mInitTitle;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_webview_layout;
    }

    @Override
    protected void setupView() {
        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_back = findViewById(R.id.toolbar_back);
        webview = findViewById(R.id.webview);
        progress = findViewById(R.id.progress);

        mUrl = getIntent().getStringExtra(WEBVIEW_URL);
        mInitTitle = getIntent().getStringExtra(WEBVIEW_TITLE);
        toolbar_title.setText(R.string.webview_startload_title);
        toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (webview.canGoBack()) {
                    webview.goBack();
                } else {
                    finish();
                }
            }
        });

        WebSettings webSettings = webview.getSettings();
        //如果访问的页面中要与Javascript交互，则webview必须设置支持Javascript
        //若加载的html里有JS在执行动画等操作，会造成资源浪费（CPU、电量）在onStop和onResume
        //里分别把 setJavaScriptEnabled()给设置成false和true即可
        webSettings.setJavaScriptEnabled(true);
        // 设置允许JS弹窗
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        //设置自适应屏幕，两者合用
        webSettings.setUseWideViewPort(true); //将图片调整到适合webview的大小
        webSettings.setLoadWithOverviewMode(true); // 缩放至屏幕的大小
        //缩放操作
        //webSettings.setSupportZoom(true); //支持缩放，默认为true。是下面二个的前提。
        //webSettings.setBuiltInZoomControls(true); //设置内置的缩放控件。若为false，则该WebView不可缩放
        //webSettings.setDisplayZoomControls(false); //隐藏原生的缩放控件
        //设置图片加载
        webSettings.setBlockNetworkImage(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webSettings.setMixedContentMode(android.webkit.WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        webSettings.setDomStorageEnabled(true); // 开启 DOM storage API 功能
        webSettings.setDatabaseEnabled(true);   //开启 database storage API 功能
        //String cacheDirPath = getFilesDir().getAbsolutePath() + APP_CACAHE_DIRNAME;
        //webSettings.setAppCachePath(cacheDirPath); //设置  Application Caches 缓存目录
        webSettings.setAppCacheEnabled(false);//开启 Application Caches 功能
        //设置WebView是否使用其内置的变焦机制，该机制结合屏幕缩放控件使用，默认是false，不使用内置变焦机制。
        webSettings.setAllowContentAccess(false);
        //设置WebView是否保存表单数据，默认true，保存数据。
        webSettings.setSaveFormData(true);
        //缓存模式如下：
        //LOAD_CACHE_ONLY: 不使用网络，只读取本地缓存数据
        //LOAD_DEFAULT: （默认）根据cache-control决定是否从网络上取数据。
        //LOAD_NO_CACHE: 不使用缓存，只从网络获取数据.
        //LOAD_CACHE_ELSE_NETWORK，只要本地有，无论是否过期，或者no-cache，都使用缓存中的数据。
        if (AndroidUtils.isNetworkAvailable()) {
            webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        } else {
            webSettings.setCacheMode(WebSettings.LOAD_CACHE_ONLY);
        }
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);

        mWebViewHeader = new HashMap<>();
        mWebViewHeader.put("platform", "Android");
        mWebViewHeader.put("version", BuildConfig.VERSION_CODE + "");
        //mWebViewHeader.put("token", Global.getUserToken());
        //mWebViewHeader.put("channel", AppUtils.getChannel());
        mWebViewHeader.put("Custom-Agent", Build.MODEL + ";" + Build.VERSION.SDK_INT + ";" + BuildConfig.APPLICATION_ID + ";" + BuildConfig.VERSION_CODE + ";" + BuildConfig.VERSION_NAME);

        webview.setWebChromeClient(mWebChromeClient);
        webview.setWebViewClient(mWebViewClient);

        provideHeader();

        webview.loadUrl(mUrl, mWebViewHeader);
        //webview.loadData(htmlData, HtmlUtil.MIME_TYPE, HtmlUtil.ENCODING);
    }

    protected void provideHeader() {

    }

    @Override
    protected void setupData(Bundle savedInstanceState) {

    }

    //辅助WebView处理Javascript的对话框,网站图标,网站标题等
    private WebChromeClient mWebChromeClient = new WebChromeClient() {

        private String mTitle;

        //获取Web页中的标题
        @Override
        public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);
            this.mTitle = title;
            toolbar_title.setText(mTitle);
        }

        //获得网页的加载进度并显示
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
            if (!isFinishing()) {
                if (newProgress < 99) {
                    toolbar_title.setText(getResources().getString(R.string.webview_startload_title));
                } else {
                    toolbar_title.setText(mTitle);
                }

                if (newProgress == 100) {
                    progress.setVisibility(View.GONE);
                } else {
                    progress.setVisibility(View.VISIBLE);
                    ViewGroup.LayoutParams lp = progress.getLayoutParams();
                    lp.width = DisplayUtils.getWidth() * newProgress / 100;
                }
            }
        }
    };

    private WebViewClient mWebViewClient = new WebViewClient() {

        //复写shouldOverrideUrlLoading()方法，使得打开网页时不调用系统浏览器，
        //而是在本WebView中显示
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.startsWith("app://xxx")) {
                return true;
            } else if (url.startsWith("https://xxx")) {
                return true;
            }

            view.loadUrl(url, mWebViewHeader);
            return true;
        }

        //开始载入页面调用的
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        //在页面加载结束时调用
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }

        //在加载页面资源时会调用，每一个资源（比如图片）的加载都会调用一次。
        @Override
        public void onLoadResource(WebView view, String url) {
            super.onLoadResource(view, url);
        }

        @Override
        public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
            //handler.cancel(); 默认的处理方式，WebView变成空白页
            //接受证书
            sslErrorHandler.proceed();
            //handleMessage(Message msg); 其他处理
        }

        //加载页面的服务器出现错误时（如404）调用
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            webview.setVisibility(View.GONE);
            //mEmptyView.setVisibility(View.VISIBLE);
            toolbar_title.postDelayed(new Runnable() {
                @Override
                public void run() {
                    toolbar_title.setText(mInitTitle);
                }
            }, 200);
        }

    };

    @Override
    public void onBackPressed() {
        if (webview != null && webview.canGoBack()) {
            webview.goBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        if (webview != null) {
            webview.loadDataWithBaseURL(null, "", "text/html", "utf-8", null);

            //这个api仅仅清除自动完成填充的表单数据，并不会清除WebView存储到本地的数据
            webview.clearFormData();

            //清除当前webview访问的历史记录
            //只会webview访问历史记录里的所有记录除了当前访问记录
            webview.clearHistory();

            //清除网页访问留下的缓存
            //由于内核缓存是全局的因此这个方法不仅仅针对webview而是针对整个应用程序.
            webview.clearCache(true);

            ((ViewGroup) webview.getParent()).removeView(webview);
            webview.destroy();
            webview = null;
        }

        super.onDestroy();
    }

}

//    Office Document Show
//    mTbsReaderView = new TbsReaderView(this, this);
//    fl_pdf_id.addView(mTbsReaderView, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
//    Bundle bundle = new Bundle();
//    bundle.putString("filePath", getLocalFile().getPath());
//    bundle.putString("tempPath", Environment.getExternalStorageDirectory().getPath());
//    boolean result = mTbsReaderView.preOpen(parseFormat(mFileName), false);
//    if (result) {
//        mTbsReaderView.openFile(bundle);
//    }
//    mTbsReaderView.onStop();

