package com.util_code.room;

import android.content.Context;

import androidx.annotation.NonNull;

public class UserDataSource {

    private static volatile UserDataSource INSTANCE;

    private UserDao mUserDao;

    UserDataSource(UserDao userDao) {
        mUserDao = userDao;
    }

    public static UserDataSource getInstance(@NonNull Context context) {
        if (INSTANCE == null) {
            synchronized (UserDataSource.class) {
                if (INSTANCE == null) {
                    RoomDataBase database = RoomDataBase.getInstance(context);
                    INSTANCE = new UserDataSource(database.userDao());
                }
            }
        }
        return INSTANCE;
    }

    public User getUser() {
        return mUserDao.getUser();
    }

    public void insertOrUpdateUser(User user) {
        mUserDao.insertUser(user);
    }

    public void deleteAllUsers() {
        mUserDao.deleteAllUsers();
    }
}