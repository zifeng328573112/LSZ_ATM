package com.util_code.room;

import androidx.paging.DataSource;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface NetLogInfoDao {

    @Query("SELECT * FROM netloginfo")
    public Flowable<List<NetLogInfo>> loadAll();

    @Query("SELECT * from NetLogInfo where requestdate = :datetime")
    public List<NetLogInfo> synLoadNetLogInfoByDate(String datetime);

    @Query("SELECT * from NetLogInfo where requestdate = :datetime")
    public Flowable<List<NetLogInfo>> loadNetLogInfoByDate(String datetime);

    //Using Room DB,Need to Use DataSource.Factory Implement PositionalDataSource
    @Query("SELECT * FROM netloginfo where requestdate = :datetime")
    public DataSource.Factory<Integer, NetLogInfo> loadDataSource(String datetime);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertNetLogInfo(List<NetLogInfo> NetLogInfos);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertNetLogInfo(NetLogInfo NetLogInfo);

    @Delete
    void delete(NetLogInfo netLogInfo);

    @Query("DELETE FROM NetLogInfo")
    void deleteAllNetLogInfo();


}
