package com.util_code.room;

import androidx.annotation.Keep;
import androidx.annotation.NonNull;
import androidx.room.Entity;

@Keep
@Entity(tableName = "netloginfo", primaryKeys = {"requesturl", "requestmethod", "requestbody"})
public class NetLogInfo {

    @NonNull
    private String requesturl;

    @NonNull
    private String requestmethod;

    @NonNull
    private String requestbody;

    @NonNull
    private String requestdate;

    private String responebody;

    @NonNull
    public String getRequesturl() {
        return requesturl;
    }

    public void setRequesturl(@NonNull String requesturl) {
        this.requesturl = requesturl;
    }

    @NonNull
    public String getRequestmethod() {
        return requestmethod;
    }

    public void setRequestmethod(@NonNull String requestmethod) {
        this.requestmethod = requestmethod;
    }

    public String getRequestbody() {
        return requestbody;
    }

    public void setRequestbody(@NonNull String requestbody) {
        this.requestbody = requestbody;
    }

    @NonNull
    public String getRequestdate() {
        return requestdate;
    }

    public void setRequestdate(@NonNull String requestdate) {
        this.requestdate = requestdate;
    }

    public String getResponebody() {
        return responebody;
    }

    public void setResponebody(String responebody) {
        this.responebody = responebody;
    }
}
