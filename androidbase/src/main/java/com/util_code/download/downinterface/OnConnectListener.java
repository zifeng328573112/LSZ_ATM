package com.util_code.download.downinterface;

import com.util_code.download.DownloadException;

public interface OnConnectListener {

    void onConnecting();

    void onConnected(long time, long length, boolean isAcceptRanges);

    void onConnectCanceled();

    void onConnectFailed(DownloadException de);
}