package com.util_code.download.downinterface;

import com.util_code.download.DownloadException;

public interface OnDownloadListener {

    void onDownloadProgress(long finished, long length);

    void onDownloadPaused();

    void onDownloadCanceled();

    void onDownloadCompleted();

    void onDownloadFailed(DownloadException de);
}