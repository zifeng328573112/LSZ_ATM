package com.util_code.download.downinterface;

public interface DownloadStub {
    void start();

    void pause();

    void cancel();

    boolean isRunning();
}
