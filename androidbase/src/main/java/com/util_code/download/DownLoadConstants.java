package com.util_code.download;

public class DownLoadConstants {

    public static final class HTTP {
        public static final int CONNECT_TIME_OUT = 10 * 1000;
        public static final int READ_TIME_OUT = 10 * 1000;
        public static final String GET = "GET";
    }
}
