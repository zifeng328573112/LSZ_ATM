package com.util_code.download.downinterfaceimpl;

import com.util_code.download.downinterface.DownLoadCallBack;

import java.io.File;

public abstract class SimpleDownLoadCallBack implements DownLoadCallBack {

    @Override
    public void onConnected(long total, boolean isRangeSupport) {

    }

    @Override
    public void onPaused(File downloadfile) {

    }
}
