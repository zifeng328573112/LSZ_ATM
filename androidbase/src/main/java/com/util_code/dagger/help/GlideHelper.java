package com.util_code.dagger.help;

import android.content.Context;

import com.util_code.base.BaseApplication;
import com.util_code.dagger.component.AppComponent;
import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.Registry;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.load.engine.bitmap_recycle.LruBitmapPool;
import com.bumptech.glide.load.engine.cache.DiskCache;
import com.bumptech.glide.load.engine.cache.DiskLruCacheWrapper;
import com.bumptech.glide.load.engine.cache.LruResourceCache;
import com.bumptech.glide.load.engine.cache.MemorySizeCalculator;
import com.bumptech.glide.module.AppGlideModule;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
@GlideModule
public class GlideHelper extends AppGlideModule {

    public static final int IMAGE_DISK_CACHE_MAX_SIZE = 100 * 1024 * 1024;

    @Inject
    public GlideHelper() {
        /*=====*/
    }

    @Override
    public void applyOptions(final Context context, GlideBuilder builder) {
        //全局配置Glide
        builder.setDiskCache(new DiskCache.Factory() {
            @Override
            public DiskCache build() {
                // Careful: the external cache directory doesn't enforce permissions
                AppComponent appComponent = BaseApplication.getAppComponent();
                return DiskLruCacheWrapper.create(appComponent.getGlideCacheDir(), IMAGE_DISK_CACHE_MAX_SIZE);
            }
        });

        MemorySizeCalculator calculator = new MemorySizeCalculator.Builder(context).build();
        int defaultMemoryCacheSize = calculator.getMemoryCacheSize();
        int defaultBitmapPoolSize = calculator.getBitmapPoolSize();

        int customMemoryCacheSize = (int) (1.2 * defaultMemoryCacheSize);
        int customBitmapPoolSize = (int) (1.2 * defaultBitmapPoolSize);

        builder.setMemoryCache(new LruResourceCache(customMemoryCacheSize));
        builder.setBitmapPool(new LruBitmapPool(customBitmapPoolSize));
    }

    @Override
    public void registerComponents(Context context, Glide glide, Registry registry) {
        //注册自定义组件
        super.registerComponents(context, glide, registry);
    }

    @Override
    public boolean isManifestParsingEnabled() {
        // 避免二次加载
        return false;
    }

}

/*
Glide 4.0+使用姿势
1、资源文件
Glide.with(this).load(R.drawable.hsk1).into(imageView1);
2、本地图片
File file = new File("图片地址");
Glide.with(this).load(file).into(imageView1);
3、通过Uri方式
Uri uri = Uri.parse(url);
Glide.with(this).load(uri).into(imageView1);
4.占位符
GlideApp.with(this).asBitmap().load(url)
        .placeholder(R.drawable.image320)//加载的时候占位
        .error(new ColorDrawable(Color.BLUE))//请求资源失败的时候
        .fallback(new ColorDrawable(Color.CYAN))//当请求内容为null的时候显示
        .into(imageView2);
5.磁盘缓存策略
DiskCacheStrategy.ALL：缓存所有图片
DiskCacheStrategy.NONE：不缓存任何图片
DiskCacheStrategy.DATA：缓存原始数据
DiskCacheStrategy.RESOURCE：缓存转换后的数据
DiskCacheStrategy.AUTOMATIC：自动选择存储数据
*/



