package com.util_code.dagger.component;

import android.content.Context;

import com.util_code.dagger.help.OkhttpHelper;
import com.util_code.dagger.help.PreferencesHelper;
import com.util_code.dagger.help.RetrofitHelper;
import com.util_code.dagger.module.AppModule;
import com.util_code.dagger.qualifier.ApplicationContext;
import com.util_code.dagger.qualifier.GlideCache;
import com.util_code.dagger.qualifier.OkhttpCache;

import java.io.File;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {

    @ApplicationContext
    Context getContext();

    File getCacheDir();

    @OkhttpCache
    File getOkhttpCacheDir();

    @GlideCache
    File getGlideCacheDir();

    OkhttpHelper okhttpHelper();

    RetrofitHelper RetrofitHelper();

    PreferencesHelper preferencesHelper();

}
