package com.util_code.widget.tintimage;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.widget.ImageView;

import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;

import java.util.ArrayList;
import java.util.List;

public class TintUtils {

    // 支持使用ColorStateList着色,可以根据View的状态着色成不同的颜色.
    // getResources().getColorStateList(R.color.tint_colors)
    // res/color/tint_colors.xml
    // <?xml version="1.0" encoding="utf-8"?>
    // <selector xmlns:android="http://schemas.android.com/apk/res/android">
    // <item android:color="@color/red" android:state_pressed="true" />
    // <item android:color="@color/gray" />
    // </selector>

    // <com.baseandroid.widget.tintimage.TintImageView
    //    android:layout_width="wrap_content"
    //    android:layout_height="wrap_content"
    //    android:src="@drawable/animation_img"/>

    public static Drawable setDrawableTintColor(Drawable originDrawable, ColorStateList colorStateList, PorterDuff.Mode tintMode) {
        Drawable tintDrawable = DrawableCompat.wrap(originDrawable).mutate();
        if (tintMode != null) {
            DrawableCompat.setTintMode(tintDrawable, tintMode);
        }
        DrawableCompat.setTintList(tintDrawable, colorStateList);
        return tintDrawable;
    }

    public static void setImageTintColor(Context context, ImageView imageView, @DrawableRes int drawableId, @ColorInt Integer normalColor, @ColorInt Integer pressedColor, @ColorInt Integer disabledColor, @ColorInt Integer selectedColor, PorterDuff.Mode mode) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        List<int[]> stateList = new ArrayList<>();
        List<Integer> colorList = new ArrayList<>();

        // disabled
        if (disabledColor != null) {
            stateList.add(new int[]{-android.R.attr.state_enabled});
            colorList.add(disabledColor);
        }

        // pressed
        if (pressedColor != null) {
            stateList.add(new int[]{android.R.attr.state_pressed});
            colorList.add(pressedColor);
        }

        // selected
        if (selectedColor != null) {
            stateList.add(new int[]{android.R.attr.state_selected});
            colorList.add(selectedColor);
        }

        // normal state
        if (normalColor != null) {
            stateList.add(new int[]{});
            colorList.add(normalColor);
        }

        if (!colorList.isEmpty()) {
            int len = colorList.size();
            int[][] colorStates = new int[len][];
            stateList.toArray(colorStates);

            int[] colors = new int[len];
            for (int i = 0; i < len; i++) {
                colors[i] = colorList.get(i);
            }

            ColorStateList csl = new ColorStateList(colorStates, colors);

            StateListDrawable sld = new StateListDrawable();
            for (int i = 0; i < len; i++) {
                sld.addState(colorStates[i], drawable);
            }

            Drawable.ConstantState constantState = sld.getConstantState();
            drawable = DrawableCompat.wrap(constantState == null ? sld : constantState.newDrawable())
                    .mutate();

            DrawableCompat.setTintMode(drawable, mode);
            DrawableCompat.setTintList(drawable, csl);
        }
        imageView.setImageDrawable(drawable);
    }

}

