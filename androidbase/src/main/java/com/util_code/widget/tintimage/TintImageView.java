package com.util_code.widget.tintimage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.util_code.R;

@SuppressLint("AppCompatCustomView")
public class TintImageView extends ImageView {

    public static final int TINTMODE_MULTIPLY = 0;
    public static final int TINTMODE_SRCATOP = 1;

    private ColorStateList mTintColorStateList;
    private int mTintMode;

    public TintImageView(Context context) {
        this(context, null);
    }

    public TintImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TintImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initTint(context, attrs, defStyleAttr);
    }

    private void initTint(Context context, AttributeSet attrs, int defStyle) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TintImageView, defStyle, 0);
        mTintColorStateList = a.getColorStateList(R.styleable.TintImageView_tintRes);
        mTintMode = a.getInt(R.styleable.TintImageView_tintType, TINTMODE_MULTIPLY);
        a.recycle();

        if (mTintColorStateList == null) {
            //default
            mTintColorStateList = getResources().getColorStateList(R.color.tint_overlay_color);
        }

        Drawable drawable = getDrawable();
        if (drawable != null) {
            setImageDrawable(drawable);
        }
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        super.setImageBitmap(bm);
        Drawable drawable = getDrawable();
        if (drawable != null) {
            setImageDrawable(drawable);
        }
    }

    @Override
    public void setImageResource(int resId) {
        super.setImageResource(resId);
        Drawable drawable = getDrawable();
        if (drawable != null) {
            setImageDrawable(drawable);
        }
    }

    @Override
    public void setImageURI(Uri uri) {
        super.setImageURI(uri);
        Drawable drawable = getDrawable();
        if (drawable != null) {
            setImageDrawable(drawable);
        }
    }

    @Override
    public void setImageDrawable(Drawable drawable) {
        if (drawable != null) {
            drawable = tintDrawable(drawable);
        }

        super.setImageDrawable(drawable);
    }

    private Drawable tintDrawable(Drawable drawable) {
        if (mTintMode == TINTMODE_MULTIPLY) {
            return TintUtils.setDrawableTintColor(drawable, mTintColorStateList, PorterDuff.Mode.MULTIPLY);
        } else {
            return TintUtils.setDrawableTintColor(drawable, mTintColorStateList, PorterDuff.Mode.SRC_ATOP);
        }
    }

}

// MULTIPLY正片叠底中白色全部被屏蔽掉了，将两层底色混合，颜色相乘产生较暗的颜色，一般在制作阴影时常使用该模式。
// 它一般会把颜色的像素较浅显示的不明显，较深的保留，产生较暗的效果。取两图层交集部分叠加后颜色
// 其实“正片叠底”的计算公式是：A*B/255。（A、B是指的图层A和图层B）
// 透明度也是这样计算的：A层的透明度乘以B层的透明度然后除以255:AlphaA*AlphaB/255
// AB两个图层以正片叠底地方式混合没有先后顺序，谁在上层谁在下层都可以,因为混合后的目标图像的透明度是通过他们相乘的积得来的

// 像素灰阶值其实就可以直接用其对应RGB值来代替
//public int[] getRGB(int pixel) {
//    int r = (pixel >> 16) & 0xff;
//    int g = (pixel >> 8) & 0xff;
//    int b = pixel & 0xff;
//    return new int[]{r, g, b};
//}

//public void multiply(Tint imgA,Tint imgB){
// 先获取两个图层同一位置的像素值
//    int pixelA = imgA.getRGB(x, y);
//    int pixelB = imgB.getRGB(x, y);
//    // 获取两组RGB数组
//    int[] rgb1 = getRGB(pixelA);
//    int[] rgb2 = getRGB(pixelB);
// 根据公式分别计算新的RGB值
//    int r = rgb1[0] * rgb2[0] / 255;
//    int g = rgb1[1] * rgb2[1] / 255;
//    int b = rgb1[2] * rgb2[2] / 255;
// 获得正片叠底后的像素值
//    int pixelNew = new Color(r, g, b).getRGB();
//}