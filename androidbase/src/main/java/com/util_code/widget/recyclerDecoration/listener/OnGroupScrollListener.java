package com.util_code.widget.recyclerDecoration.listener;

public interface OnGroupScrollListener {

    void onScroll(String titleName, int position, int id);

}
