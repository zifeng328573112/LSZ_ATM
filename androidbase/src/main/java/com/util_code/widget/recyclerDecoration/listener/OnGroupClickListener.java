package com.util_code.widget.recyclerDecoration.listener;

/**
 * Created by Cyril
 * date 2019/10/17
 * description 头部点击事件
 */

public interface OnGroupClickListener {
    void onClick(int position, int id);
}
