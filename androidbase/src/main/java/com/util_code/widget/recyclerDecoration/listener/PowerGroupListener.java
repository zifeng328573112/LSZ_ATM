package com.util_code.widget.recyclerDecoration.listener;

import android.view.View;

/**
 * Created by Cyril
 * date 2019/10/17
 * 显示自定义View的Group监听
 */

public interface PowerGroupListener extends GroupListener {

    View getGroupView(int position);
}
