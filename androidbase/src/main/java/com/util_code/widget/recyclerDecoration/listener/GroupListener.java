package com.util_code.widget.recyclerDecoration.listener;

/**
 * Created by Cyril
 * date 2019/10/17
 */

public interface GroupListener {
    String getGroupName(int position);
}
