package com.util_code.widget.recycleadpter;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.util_code.R;
import com.util_code.utils.StringUtils;

public class SimpleLoadMoreView {

    private LoadMoreState mLoadMoreStatus = LoadMoreState.STATUS_DEFAULT;
    private boolean mLoadMoreEndGone = false;

    public void setLoadMoreStatus(LoadMoreState loadMoreStatus) {
        this.mLoadMoreStatus = loadMoreStatus;
    }

    public LoadMoreState getLoadMoreStatus() {
        return mLoadMoreStatus;
    }

    public void convert(ViewHolder holder) {

        switch (mLoadMoreStatus.getStatus()) {
            case DEFAULT:
                visibleLoading(holder, false, mLoadMoreStatus.getMessage());
                visibleLoadFail(holder, false, mLoadMoreStatus.getMessage());
                visibleLoadEnd(holder, false, mLoadMoreStatus.getMessage());
                break;

            case LOADING:
                visibleLoading(holder, true, mLoadMoreStatus.getMessage());
                visibleLoadFail(holder, false, mLoadMoreStatus.getMessage());
                visibleLoadEnd(holder, false, mLoadMoreStatus.getMessage());
                break;

            case FAIL:
                visibleLoading(holder, false, mLoadMoreStatus.getMessage());
                visibleLoadFail(holder, true, mLoadMoreStatus.getMessage());
                visibleLoadEnd(holder, false, mLoadMoreStatus.getMessage());
                break;

            case END:
                visibleLoading(holder, false, mLoadMoreStatus.getMessage());
                visibleLoadFail(holder, false, mLoadMoreStatus.getMessage());
                visibleLoadEnd(holder, true, mLoadMoreStatus.getMessage());
                break;
        }
    }

    private void visibleLoading(ViewHolder holder, boolean visible, String loadingContent) {
        holder.itemView.findViewById(getLoadingViewId())
                .setVisibility(visible ? View.VISIBLE : View.GONE);
        if (!StringUtils.isEmpty(loadingContent)) {
            TextView loadingText = (TextView) holder.itemView.findViewById(getLoadFailTextId());
            loadingText.setText(loadingContent);
        }
    }

    private void visibleLoadFail(ViewHolder holder, boolean visible, String failContent) {
        holder.itemView.findViewById(getLoadFailViewId())
                .setVisibility(visible ? View.VISIBLE : View.GONE);
        if (!StringUtils.isEmpty(failContent)) {
            TextView failText = (TextView) holder.itemView.findViewById(getLoadFailTextId());
            failText.setText(failContent);
        }
    }

    private void visibleLoadEnd(ViewHolder holder, boolean visible, String loadEndContent) {
        holder.itemView.findViewById(getLoadEndViewId())
                .setVisibility(visible ? View.VISIBLE : View.GONE);
        if (!StringUtils.isEmpty(loadEndContent)) {
            TextView endText = (TextView) holder.itemView.findViewById(getLoadEndTextId());
            endText.setText(loadEndContent);
        }
    }

    public final void setLoadMoreEndGone(boolean loadMoreEndGone) {
        this.mLoadMoreEndGone = loadMoreEndGone;
    }

    public final boolean isLoadEndMoreGone() {
        if (getLoadEndViewId() == 0) {
            return true;
        }
        return mLoadMoreEndGone;
    }

    public int getLayoutId() {
        return R.layout.simple_load_more;
    }

    protected int getLoadingViewId() {
        return R.id.load_more_loading_view;
    }

    protected int getLoadingTextId() {
        return R.id.load_more_loading_tv;
    }

    protected int getLoadFailViewId() {
        return R.id.load_more_load_fail_view;
    }

    protected int getLoadFailTextId() {
        return R.id.load_more_fail_tv;
    }

    protected int getLoadEndViewId() {
        return R.id.load_more_load_end_view;
    }

    protected int getLoadEndTextId() {
        return R.id.load_more_end_tv;
    }
}
