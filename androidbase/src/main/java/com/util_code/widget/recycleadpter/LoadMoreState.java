package com.util_code.widget.recycleadpter;

import com.util_code.utils.StringUtils;

public class LoadMoreState {

    public enum Status {
        DEFAULT,
        LOADING,
        FAIL,
        END
    }

    private Status status;

    private String message;

    private LoadMoreState(Status status, String message) {
        this.status = status;
        this.message = message;
    }

    private LoadMoreState(Status status) {
        this.status = status;
    }

    public Status getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public static LoadMoreState STATUS_DEFAULT = new LoadMoreState(Status.DEFAULT);

    public static LoadMoreState STATUS_LOADING = new LoadMoreState(Status.LOADING);

    public static LoadMoreState fail(String message) {
        return new LoadMoreState(Status.FAIL, StringUtils.isEmpty(message) ? "加载失败，请点我重试" : message);
    }

    public static LoadMoreState end(String message) {
        return new LoadMoreState(Status.END, StringUtils.isEmpty(message) ? "没有更多数据" : message);
    }

}