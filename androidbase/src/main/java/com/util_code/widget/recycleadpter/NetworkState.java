package com.util_code.widget.recycleadpter;

import com.util_code.utils.StringUtils;

public class NetworkState {

    public enum Status {
        RUNNING,
        SUCCESS,  /*加载成功,还有更多数据*/
        COMPLETE, /*加载成功,没有更多数据*/
        FAILED
    }

    private Status status;

    private String message;

    private NetworkState(Status status, String message) {
        this.status = status;
        this.message = message;
    }

    private NetworkState(Status status) {
        this.status = status;
    }

    public Status getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public static NetworkState LOADED = new NetworkState(Status.SUCCESS);

    public static NetworkState LOADING = new NetworkState(Status.RUNNING);

    public static NetworkState error(String message) {
        return new NetworkState(Status.FAILED, StringUtils.isEmpty(message) ? "加载失败，请点我重试" : message);
    }

    public static NetworkState complete(String message) {
        return new NetworkState(Status.COMPLETE, StringUtils.isEmpty(message) ? "没有更多数据" : message);
    }

}
