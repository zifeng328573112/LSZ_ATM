package com.util_code.widget.recycleadpter;


import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.util_code.R;
import com.util_code.utils.StringUtils;

public class PageLoadMoreView {

    public void convert(RecyclerView.ViewHolder holder, NetworkState networkState) {
        switch (networkState.getStatus()) {
            case RUNNING:
                visibleLoading(holder, true, networkState.getMessage());
                visibleLoadFail(holder, false, networkState.getMessage());
                visibleLoadEnd(holder, false, networkState.getMessage());
                break;

            case SUCCESS:
                visibleLoading(holder, false, networkState.getMessage());
                visibleLoadFail(holder, false, networkState.getMessage());
                visibleLoadEnd(holder, false, networkState.getMessage());
                break;

            case COMPLETE:
                visibleLoading(holder, false, networkState.getMessage());
                visibleLoadFail(holder, false, networkState.getMessage());
                visibleLoadEnd(holder, true, networkState.getMessage());
                break;

            case FAILED:
                visibleLoading(holder, false, networkState.getMessage());
                visibleLoadFail(holder, true, networkState.getMessage());
                visibleLoadEnd(holder, false, networkState.getMessage());
                break;
        }
    }

    private void visibleLoading(RecyclerView.ViewHolder holder, boolean visible, String loadingContent) {
        holder.itemView.findViewById(getLoadingViewId())
                .setVisibility(visible ? View.VISIBLE : View.GONE);
        if (!StringUtils.isEmpty(loadingContent)) {
            TextView loadingText = (TextView) holder.itemView.findViewById(getLoadFailTextId());
            loadingText.setText(loadingContent);
        }
    }

    private void visibleLoadFail(RecyclerView.ViewHolder holder, boolean visible, String failContent) {
        holder.itemView.findViewById(getLoadFailViewId())
                .setVisibility(visible ? View.VISIBLE : View.GONE);
        if (!StringUtils.isEmpty(failContent)) {
            TextView failText = (TextView) holder.itemView.findViewById(getLoadFailTextId());
            failText.setText(failContent);
        }
    }

    private void visibleLoadEnd(RecyclerView.ViewHolder holder, boolean visible, String loadEndContent) {
        holder.itemView.findViewById(getLoadEndViewId())
                .setVisibility(visible ? View.VISIBLE : View.GONE);
        if (!StringUtils.isEmpty(loadEndContent)) {
            TextView endText = (TextView) holder.itemView.findViewById(getLoadEndTextId());
            endText.setText(loadEndContent);
        }
    }

    public int getLayoutId() {
        return R.layout.simple_load_more;
    }

    protected int getLoadingViewId() {
        return R.id.load_more_loading_view;
    }

    protected int getLoadingTextId() {
        return R.id.load_more_loading_tv;
    }

    protected int getLoadFailViewId() {
        return R.id.load_more_load_fail_view;
    }

    protected int getLoadFailTextId() {
        return R.id.load_more_fail_tv;
    }

    protected int getLoadEndViewId() {
        return R.id.load_more_load_end_view;
    }

    protected int getLoadEndTextId() {
        return R.id.load_more_end_tv;
    }
}
