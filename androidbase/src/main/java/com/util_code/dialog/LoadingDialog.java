package com.util_code.dialog;

import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.DialogFragment;

import com.util_code.R;
import com.util_code.base.BaseDialog;

public class LoadingDialog extends BaseDialog {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.customDialogTheme);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_loading_layout;
    }

    @Override
    protected void setupView(View rootView) {
        getDialog().setCanceledOnTouchOutside(false);
        setCancelable(false);
    }

}
