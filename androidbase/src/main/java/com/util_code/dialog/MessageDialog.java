package com.util_code.dialog;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.util_code.R;
import com.util_code.base.BaseDialog;

public class MessageDialog extends BaseDialog {

    protected TextView titleView;
    protected LinearLayout textContainer;
    protected TextView contentView;
    protected Button confirmButton;
    protected Button cancelButton;
    protected View buttonDividerView;

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_message_layout;
    }

    @Override
    protected void setupView(View rootView) {
        titleView = rootView.findViewById(R.id.title);
        contentView = rootView.findViewById(R.id.content);
        confirmButton = rootView.findViewById(R.id.confirm);
        cancelButton = rootView.findViewById(R.id.cancel);
        buttonDividerView = rootView.findViewById(R.id.button_divider);
        textContainer = rootView.findViewById(R.id.text_container);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    public void setTitle(String title) {
        if (titleView != null) {
            titleView.setText(title);
        }
    }

    public void setContent(String content) {
        contentView.setText(content);
    }

    public void setConfirmText(String text) {
        if (confirmButton != null) {
            confirmButton.setText(text);
        }
    }

    public void setCancelText(String text) {
        if (cancelButton != null) {
            cancelButton.setText(text);
        }
    }

    public void setConfirmOnClickListener(View.OnClickListener confirmOnClickListener) {
        if (confirmButton != null && confirmOnClickListener != null) {
            confirmButton.setOnClickListener(confirmOnClickListener);
        }
    }

    public void setCancelOnClickListener(View.OnClickListener cancelOnClickListener) {
        if (cancelButton != null && cancelOnClickListener != null) {
            cancelButton.setOnClickListener(cancelOnClickListener);
        }
    }


    public void hideTitle() {
        titleView.setVisibility(View.GONE);
        textContainer.setBackgroundResource(R.drawable.dialog_topbg_shape);
    }

    public void hideCancelButton() {
        cancelButton.setVisibility(View.GONE);
        buttonDividerView.setVisibility(View.GONE);
        confirmButton.setBackgroundResource(R.drawable.dialog_button_select);
    }

    public void hideConfirmButton() {
        confirmButton.setVisibility(View.GONE);
        buttonDividerView.setVisibility(View.GONE);
        cancelButton.setBackgroundResource(R.drawable.dialog_button_select);
    }

    public void setConfirmTextColor(int color) {
        confirmButton.setTextColor(color);
    }

    public void setCancelTextColor(int color) {
        cancelButton.setTextColor(color);
    }

    public void setConfirmButtonBackground(int backgroundResource) {
        confirmButton.setBackgroundResource(backgroundResource);
    }

    public void setCancelButtonBackground(int backgroundResource) {
        cancelButton.setBackgroundResource(backgroundResource);
    }

    /*public static MessageDialog newInstance() {
        MessageDialog messageDialog = new MessageDialog();
        Bundle args = new Bundle();
        args.putString("param", "param");
        messageDialog.setArguments(args);
        return messageDialog;
    }*/

    public static void goSettingPermission(Activity activity, String content) {
        MessageDialog messageDialog = new MessageDialog();
        messageDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                messageDialog.hideTitle();
                messageDialog.setContent(content);
                messageDialog.setConfirmText("去设置");
                messageDialog.setCancelText("取消");
                messageDialog.setConfirmOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        messageDialog.dismiss();
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
                        intent.setData(uri);
                        activity.startActivity(intent);
                    }
                });
            }
        });
        messageDialog.show(((AppCompatActivity) activity).getSupportFragmentManager(), "messagedialog");
    }
}
