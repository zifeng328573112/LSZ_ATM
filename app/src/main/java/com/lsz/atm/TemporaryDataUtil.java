package com.lsz.atm;

import android.text.TextUtils;
import android.util.Base64;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.facebook.stetho.common.LogUtil;
import com.google.gson.Gson;
import com.lsz.atm.db.bean.CommodityBean;
import com.lsz.atm.db.bean.PackageBean;
import com.lsz.atm.db.dao_utils.DaoUtils;
import com.lsz.atm.json.OrderPayBean;
import com.lsz.atm.json.commodity.CommoditiesBean;
import com.lsz.atm.json.commodity.ProposalsBean;
import com.lsz.atm.viewutil.BigDecimalUtils;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class TemporaryDataUtil {

    public static CommodityBean getCommodityBean(ProposalsBean proposalsBean) {
        Gson gson = new Gson();
        String toJson = gson.toJson(proposalsBean);
        return gson.fromJson(toJson, CommodityBean.class);
    }

    public static ProposalsBean getProposalsBean(CommodityBean commodityBean) {
        Gson gson = new Gson();
        String toJson = gson.toJson(commodityBean);
        ProposalsBean proposalsBean = gson.fromJson(toJson, ProposalsBean.class);
        proposalsBean.isManySpe = false;
        return proposalsBean;
    }

    public static PackageBean getPackageBean(CommoditiesBean commoditiesBean, long randomId) {
        Gson gson = new Gson();
        String toJson = gson.toJson(commoditiesBean);
        PackageBean packageBean = gson.fromJson(toJson, PackageBean.class);
        packageBean.randomId = randomId;
        return packageBean;
    }

//    public static CommoditiesBean getCommoditiesBean(PackageBean packageBean) {
//        Gson gson = new Gson();
//        String toJson = gson.toJson(packageBean);
//        return gson.fromJson(toJson, CommoditiesBean.class);
//    }

    public static RequestBody setLogin(String storeNo, String phone, String password) {
        JSONObject object = new JSONObject();
        object.put("storeNo", storeNo);
        object.put("username", phone);
        String strBase64 = Base64.encodeToString(password.getBytes(), Base64.NO_WRAP);
        object.put("password", strBase64);
        return RequestBody.create(MediaType.parse("application/json; charset=utf-8"), object.toString());
    }

    public static RequestBody setCreateOrder(String storeId, String member, String packing, String totalAmount, List<CommodityBean> commodityList, String couponPrice) {
        JSONObject object = new JSONObject();
        List<String> option = new ArrayList<>();
        object.put("storeId", storeId);
        object.put("operator", "0");
        object.put("member", member);
        object.put("orderType", packing);
        object.put("channel", "5620305");
        object.put("remark", "");
        if (!TextUtils.isEmpty(totalAmount)) {
            object.put("totalAmount", totalAmount.replace(",", ""));
        }
        JSONArray hashMap = new JSONArray();
        for (CommodityBean commodity : commodityList) {
            if (commodity.cSelectNumber > 0) {
                JSONObject commodityObject = new JSONObject();
                commodityObject.put("id", commodity.id);
                commodityObject.put("count", commodity.cSelectNumber + "");
                commodityObject.put("remark", "");
                commodityObject.put("optionIds", option);
                JSONArray objects = new JSONArray();
                List<PackageBean> packageList = DaoUtils.getPackageInstance().loadPackageByRandomId(commodity.randomId);
                for (PackageBean packageBean : packageList) {
                    if (!TextUtils.isEmpty(packageBean.buyMin) && Integer.parseInt(packageBean.buyMin) > 0) {
                        JSONObject packageObject = new JSONObject();
                        packageObject.put("id", packageBean.proComId + "");
                        packageObject.put("count", packageBean.buyMin + "");
                        packageObject.put("optionIds", option);
                        objects.add(packageObject);
                    }
                }
                commodityObject.put("commodities", objects);
                hashMap.add(commodityObject);
            }
        }
        object.put("proposals", hashMap);
        object.put("couponCode", couponPrice);
        LogUtil.d(object.toString());
        return RequestBody.create(MediaType.parse("application/json; charset=utf-8"), object.toString());
    }

    public static JSONObject getOrderPayData(OrderPayBean orderPayBean) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("merchant_order_sn", orderPayBean.merchant_order_sn);
        jsonObject.put("platform_order_no", orderPayBean.platform_order_no);
        jsonObject.put("total_fee", orderPayBean.total_fee);
        jsonObject.put("order_price", orderPayBean.order_price);
        jsonObject.put("trade_no", orderPayBean.trade_no);
        jsonObject.put("pay_time", orderPayBean.pay_time);
        jsonObject.put("attach", orderPayBean.attach);
        jsonObject.put("store_id", orderPayBean.store_id);
        jsonObject.put("cashier_id", orderPayBean.cashier_id);
        jsonObject.put("device_no", orderPayBean.device_no);
        jsonObject.put("body", orderPayBean.body);
        jsonObject.put("user_id", orderPayBean.user_id);
        jsonObject.put("user_logon_id", orderPayBean.user_logon_id);
        jsonObject.put("fee", orderPayBean.fee);
        return jsonObject;
    }

    public static String getTotalAmount(String amount1, String amount2, long selectNumber) {
        String amount = amount1;
        for (int i = 0; i < selectNumber; i++) {
            if (i > 0) {
                amount = BigDecimalUtils.add(amount, amount2, 2);
            }
        }
        return amount;
    }
}
