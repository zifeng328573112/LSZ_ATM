package com.lsz.atm.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.lsz.atm.R;
import com.util_code.utils.DensityUtil;

public class BottomDialog extends Dialog {

    public BottomDialog(@NonNull Context context) {
        super(context);
    }

    public BottomDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected BottomDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public static class Builder {

        private Context context;
        private TextView signOut;
        private TextView exitSystem;

        private BottomClickListener clickListener;

        public Builder(Context context) {
            this.context = context;
        }

        public Builder setBottomClickListener(BottomClickListener clickListener) {
            this.clickListener = clickListener;
            return this;
        }

        public BottomDialog create() {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            BottomDialog bottomDialog = new BottomDialog(context, R.style.BottomDialog);
            View view = inflater.inflate(R.layout.dialog_content_circle, null);
            signOut = view.findViewById(R.id.tv_sign_out);
            exitSystem = view.findViewById(R.id.tv_exit_system);

            bottomDialog.setContentView(view);
            bottomDialog.getWindow().setGravity(Gravity.BOTTOM);
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            params.width = context.getResources().getDisplayMetrics().widthPixels - DensityUtil.dip2px(context, 16f);
            params.bottomMargin = DensityUtil.dip2px(context, 8f);
            view.setLayoutParams(params);

            signOut.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (clickListener != null) {
                        clickListener.viewClick(true);
                    }
                }
            });
            exitSystem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (clickListener != null) {
                        clickListener.viewClick(false);
                    }
                }
            });
            return bottomDialog;
        }
    }

    public interface BottomClickListener {

        void viewClick(boolean clickExit);

    }
}
