package com.lsz.atm.dialog;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.lsz.atm.R;

public class ConfirmDialog extends Dialog {
    public ConfirmDialog(@NonNull Context context) {
        super(context);
    }

    public ConfirmDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected ConfirmDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public static class Builder {
        private final Context context;

        private TextView mTvContent;
        private TextView tv_msg_content;
        private TextView mTvPositive;
        private TextView mTvNegative;
        private TextView tvOk;
        private LinearLayout llCancelOk;

        private static String mContentText;
        private static String mMsgText = "";
        private String mNegativeText;
        private String mPositiveText;
        private String mOkText;
        private int mContentGravity = Gravity.CENTER;
        private int mContentColor;
        private int mNegativeColor;
        private int mPositiveColor;
        private int mTvOkColor;
        private int mOkBackground;
        private boolean mCancelable;
        private boolean mCancelOk = false;

        private ConfirmClickListener mListener;

        public Builder(Context context) {
            this.context = context;
        }

        public Builder setContentText(String contentText) {
            mContentText = contentText;
            return this;
        }

        public Builder setmMsgText(String msgText) {
            mMsgText = msgText;
            return this;
        }

        public Builder setContentGravity(int mContentGravity){
            this.mContentGravity = mContentGravity;
            return this;
        }

        public Builder setContentText(String contentText, int contentColor) {
            mContentColor = contentColor;
            return this.setContentText(contentText);
        }

        public Builder setNegativeText(String negativeText) {
            mNegativeText = negativeText;
            return this;
        }

        public Builder setNegativeText(String negativeText, int negativeColor) {
            mNegativeColor = negativeColor;
            return this.setNegativeText(negativeText);
        }

        public Builder setPositiveText(String positiveText) {
            mPositiveText = positiveText;
            return this;
        }

        public Builder setPositiveText(String positiveText, int positiveColor) {
            mPositiveColor = positiveColor;
            return this.setPositiveText(positiveText);
        }

        public Builder setOkText(String okText) {
            mOkText = okText;
            return this;
        }

        public Builder setOkText(String okText, int okColor) {
            mTvOkColor = okColor;
            return this.setOkText(okText);
        }

        public Builder setOkBackground(int okBackground) {
            mOkBackground = okBackground;
            return this;
        }

        public Builder setCanceledOnTouchOutside(boolean b) {
            mCancelable = b;
            return this;
        }

        public Builder setOKVisibility(boolean o){
            mCancelOk = o;
            return this;
        }

        public ConfirmDialog create() {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final ConfirmDialog dialog = new ConfirmDialog(context, R.style.customDialogTheme);
            View layout = inflater.inflate(R.layout.dialog_confirm, null);
            dialog.addContentView(layout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            mTvContent = layout.findViewById(R.id.tv_content);
            mTvPositive = layout.findViewById(R.id.tv_positive);
            mTvNegative = layout.findViewById(R.id.tv_negative);
            tv_msg_content = layout.findViewById(R.id.tv_msg_content);
            llCancelOk = layout.findViewById(R.id.ll_cancel_ok);
            tvOk = layout.findViewById(R.id.tv_ok);
            mTvContent.setGravity(mContentGravity);
            if (!TextUtils.isEmpty(mContentText)) {
                mTvContent.setText(mContentText);
            }
            if (!TextUtils.isEmpty(mMsgText)) {
                tv_msg_content.setVisibility(View.VISIBLE);
                tv_msg_content.setText(mMsgText);
            } else {
                tv_msg_content.setVisibility(View.GONE);
            }
            if (0 != mContentColor) {
                mTvContent.setTextColor(ContextCompat.getColor(context, mContentColor));
            }
            /*********************************/
            if (!TextUtils.isEmpty(mPositiveText)) {
                mTvPositive.setText(mPositiveText);
            }
            if (0 != mPositiveColor) {
                mTvPositive.setTextColor(ContextCompat.getColor(context, mPositiveColor));
            }
            /*********************************/
            if (!TextUtils.isEmpty(mNegativeText)) {
                mTvNegative.setText(mNegativeText);
            }
            if (0 != mNegativeColor) {
                mTvNegative.setTextColor(ContextCompat.getColor(context, mNegativeColor));
            }
            /*********************************/
            if (0 != mTvOkColor) {
                tvOk.setTextColor(ContextCompat.getColor(context, mTvOkColor));
            }
            if (!TextUtils.isEmpty(mOkText)) {
                tvOk.setText(mOkText);
            }
            if(0 != mOkBackground){
                tvOk.setBackgroundColor(mOkBackground);
            }
            if(mCancelOk){
                llCancelOk.setVisibility(View.GONE);
                tvOk.setVisibility(View.VISIBLE);
            } else {
                llCancelOk.setVisibility(View.VISIBLE);
                tvOk.setVisibility(View.GONE);
            }
            /*********************************/
            tvOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener != null) {
                        mListener.click(true);
                    }
                }
            });
            mTvPositive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener != null) {
                        mListener.click(true);
                    }
                }
            });
            mTvNegative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener != null) {
                        mListener.click(false);
                    }
                }
            });
            dialog.setCanceledOnTouchOutside(mCancelable);
            return dialog;
        }

        public interface ConfirmClickListener {
            void click(boolean b);
        }

        public Builder setConfirmListener(ConfirmClickListener listener) {
            mListener = listener;
            return this;
        }
    }

    public void setMsgVisibility(int visibility) {

    }
}

