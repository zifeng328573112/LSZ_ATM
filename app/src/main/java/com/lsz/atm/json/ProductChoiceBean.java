package com.lsz.atm.json;

import androidx.annotation.Keep;

import java.util.Arrays;
import java.util.List;

@Keep
public class ProductChoiceBean {

    public Long productId;  //所属产品id
    public Long choiceId; //该产品属性id
    public String choiceName; //产品属性名
    public boolean skuClass; //是否SKU级:单选，必选，不灰显 【当前属性是否为主属性】
    public boolean required; //是否必选
    public boolean multiple; //是否多选
    public Long status; //选项状态
    public int ranking; //顺序
    public List<ProductOptionBean> productOptions; //哪些属性值--属于当前产品属性

    public ProductChoiceBean(Long productId, Long choiceId, String choiceName, boolean skuClass, boolean required, boolean multiple, Long status, int ranking, List<ProductOptionBean> productOptions) {
        this.productId = productId;
        this.choiceId = choiceId;
        this.choiceName = choiceName;
        this.skuClass = skuClass;
        this.required = required;
        this.multiple = multiple;
        this.status = status;
        this.ranking = ranking;
        this.productOptions = productOptions;
    }

    public static class ProductOptionBean{
        public Long productId;  //所属产品id
        public Long choiceId;   //所属产品属性的id
        public Long optionId; // 当前属性Id
        public String optionName; //该产品属性值
        public int price;  // 单价
        public boolean showPrice; //是否显示价格
        public boolean showThis;  //是否显示该选项
        public boolean selected;  //是否默认
        public int ranking; //顺序
        public long[] awayOptions; //当前的属性和哪些属性互斥

        public ProductOptionBean(Long productId, Long choiceId, Long optionId, String optionName, int price, boolean showPrice, boolean showThis, boolean selected, int ranking, long[] awayOptions) {
            this.productId = productId;
            this.choiceId = choiceId;
            this.optionId = optionId;
            this.optionName = optionName;
            this.price = price;
            this.showPrice = showPrice;
            this.showThis = showThis;
            this.selected = selected;
            this.ranking = ranking;
            this.awayOptions = awayOptions;
        }

        @Override
        public String toString() {
            return "ProductOptionBean{" +
                    "productId=" + productId +
                    ", choiceId=" + choiceId +
                    ", optionId=" + optionId +
                    ", optionName='" + optionName + '\'' +
                    ", price=" + price +
                    ", showPrice=" + showPrice +
                    ", showThis=" + showThis +
                    ", selected=" + selected +
                    ", ranking=" + ranking +
                    ", awayOptions=" + Arrays.toString(awayOptions) +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "ProductChoiceBean{" +
                "productId=" + productId +
                ", choiceId=" + choiceId +
                ", choiceName='" + choiceName + '\'' +
                ", skuClass=" + skuClass +
                ", required=" + required +
                ", multiple=" + multiple +
                ", status=" + status +
                ", ranking=" + ranking +
                ", productOptions=" + productOptions +
                '}';
    }
}
