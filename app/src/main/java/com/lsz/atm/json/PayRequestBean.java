package com.lsz.atm.json;

import androidx.annotation.Keep;

import java.util.List;

@Keep
public class PayRequestBean<T> {

    /**
     * app_id : 20161219113441561987
     * method : openapi.payment.order.swipe
     * format : json
     * sign_method : md5
     * sign : 5F0EA68A1278C0F7A524073D2B498C43
     * nonce : pp2kiwi
     * version : 1.0
     * biz_content : {"attach":"订单: 123456","store_id":25249,"cashier_id":151156,"merchant_order_sn":"201704191492587335309","total_fee":100,"type":1,"auth_code":"130784921069830971","discount_switch":"1"}
     */

    public String app_id;//付呗平台分配的API接口ID（商户开放平台ID）
    public String method;//接口名称
    public String format;//接口格式,默认json
    public String sign_method;//签名算法,默认md5
    public String sign;//签名
    public String nonce;//请求端随机生成数
    public String version;//接口版本,默认1.0
    public T biz_content;//请求参数的集合,最大长度不限,除公共参数外所有请求参数都必须放在这个参数中传递

    @Keep
    public static class BizContentBean {
        /**
         * attach : 订单: 123456
         * store_id : 25249
         * cashier_id : 151156
         * merchant_order_sn : 201704191492587335309
         * total_fee : 100
         * type : 1
         * auth_code : 130784921069830971
         * discount_switch : 1
         */

        public String attach;//附加字段
        //        public int store_id;//门店ID,当存在多个门店时,此字段必填
        //        public int cashier_id;//收银员ID
        public String merchant_order_sn;//第三方商户订单号，确保唯一，前后不允许带空格
        public float total_fee;//订单金额(元)，精确到2位小数
        public int type;//支付方式[微信1/支付宝2/银联5]
        public String auth_code;//用户支付授权码
        public int discount_switch;//是否享受商家折扣【 1 享受 0不享受】
        public String call_back_url;//支付成功后回调链接
        public String timeout_express;//订单失效时间格式为yyyyMMddHHmmss，如2009年12月27日9点10分10秒表示为20091227091010。
        public String goods_tag;//订单优惠标记，代金券或立减优惠功能的参数【若为单品券则必填】
        public Detail detail;//商品详细描述，Json格式，对于使用单品优惠的商户，该字段必须按照规范上传

        @Override
        public String toString() {
            return "BizContentBean{" +
                    "attach='" + attach + '\'' +
                    ", merchant_order_sn='" + merchant_order_sn + '\'' +
                    ", total_fee=" + total_fee +
                    ", type=" + type +
                    ", auth_code='" + auth_code + '\'' +
                    ", discount_switch=" + discount_switch +
                    ", call_back_url='" + call_back_url + '\'' +
                    ", timeout_express='" + timeout_express + '\'' +
                    ", goods_tag='" + goods_tag + '\'' +
                    ", detail='" + detail + '\'' +
                    '}';
        }
    }

    public static class Detail {

        /**
         * 1.商户侧一张小票订单可能被分多次支付，订单原价用于记录整张小票的交易金额。
         * 2.当订单原价与支付金额不相等，则不享受优惠。
         * 3.该字段主要用于防止同一张小票分多次支付，以享受多次优惠的情况，正常支付订单不必上传此参数。
         */
        public int cost_price;//
        public String receipt_id; //商家小票ID
        public List<GoodsDetail> goods_detail; //单品信息，使用Json数组格式提交

        public static class GoodsDetail {
            public String goods_id;//商品编码
            public String goods_name;//商品名称
            public int quantity;//商品数量
            public int price;//商品单价，单位为分

            @Override
            public String toString() {
                return "GoodsDetail{" +
                        "goods_id='" + goods_id + '\'' +
                        ", goods_name='" + goods_name + '\'' +
                        ", quantity=" + quantity +
                        ", price=" + price +
                        '}';
            }
        }

        @Override
        public String toString() {
            return "Detail{" +
                    "cost_price=" + cost_price +
                    ", receipt_id='" + receipt_id + '\'' +
                    ", goods_detail=" + goods_detail +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "PayRequestBean{" +
                "app_id='" + app_id + '\'' +
                ", method='" + method + '\'' +
                ", format='" + format + '\'' +
                ", sign_method='" + sign_method + '\'' +
                ", sign='" + sign + '\'' +
                ", nonce='" + nonce + '\'' +
                ", version='" + version + '\'' +
                ", biz_content=" + biz_content +
                '}';
    }
}
