package com.lsz.atm.json;

import androidx.annotation.Keep;

@Keep
public class CheckUpdate {
    private String version_desc;
    private String latest_version;// 最新版本号V0.0.1
    private String force_update_version;// 强制更新版本号，该版本(含)及以下版本需强制更新为最新版本V0.0.0
    private String size;// 安装包大小（MB）
    private String update_log;// 更新说明
    private String apk_url;

    private boolean mandatoryUpdate;

    public String getVersion_desc() {
        return version_desc;
    }

    public void setVersion_desc(String version_desc) {
        this.version_desc = version_desc;
    }

    public String getLatest_version() {
        return latest_version;
    }

    public void setLatest_version(String latest_version) {
        this.latest_version = latest_version;
    }

    public String getForce_update_version() {
        return force_update_version;
    }

    public void setForce_update_version(String force_update_version) {
        this.force_update_version = force_update_version;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getUpdate_log() {
        return update_log;
    }

    public void setUpdate_log(String update_log) {
        this.update_log = update_log;
    }

    public String getApk_url() {
        return apk_url;
    }

    public void setApk_url(String apk_url) {
        this.apk_url = apk_url;
    }

    public boolean isMandatoryUpdate() {
        return mandatoryUpdate;
    }

    public void setMandatoryUpdate(boolean mandatoryUpdate) {
        this.mandatoryUpdate = mandatoryUpdate;
    }

    @Override
    public String toString() {
        return "CheckUpdate{" +
                "version_desc='" + version_desc + '\'' +
                ", latest_version='" + latest_version + '\'' +
                ", force_update_version='" + force_update_version + '\'' +
                ", size='" + size + '\'' +
                ", update_log='" + update_log + '\'' +
                ", apk_url='" + apk_url + '\'' +
                ", mandatoryUpdate=" + mandatoryUpdate +
                '}';
    }
}