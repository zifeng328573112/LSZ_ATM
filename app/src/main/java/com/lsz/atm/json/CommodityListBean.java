package com.lsz.atm.json;

import androidx.annotation.Keep;

import com.lsz.atm.db.bean.CommodityBean;

import java.util.List;

@Keep
public class CommodityListBean implements McTemporary {

    public String menuId;
    public String cTitle;
    public List<CommodityBean> proposals;

    @Override
    public int getmType() {
        return 1;
    }

    @Override
    public String toString() {
        return "CommodityListBean{" +
                "menuId='" + menuId + '\'' +
                ", cTitle='" + cTitle + '\'' +
                ", proposals=" + proposals +
                '}';
    }
}
