package com.lsz.atm.json;

import androidx.annotation.Keep;

@Keep
public class VIPMemberBean {


    /**
     * id : 11000
     * name :
     * gender : 1010101
     * birth : 2019-11-25
     * memberCard : 201911251000
     * phone : nullnull
     * avatar :
     * country :
     * language :
     * timezone : Asia/Shanghai
     */

    public String id;
    public String name;
    public String gender;
    public String birth;
    public String memberCard;
    public String phone;
    public String avatar;
    public String country;
    public String language;
    public String timezone;

    @Override
    public String toString() {
        return "VIPMemberBean{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", birth='" + birth + '\'' +
                ", memberCard='" + memberCard + '\'' +
                ", phone='" + phone + '\'' +
                ", avatar='" + avatar + '\'' +
                ", country='" + country + '\'' +
                ", language='" + language + '\'' +
                ", timezone='" + timezone + '\'' +
                '}';
    }
}
