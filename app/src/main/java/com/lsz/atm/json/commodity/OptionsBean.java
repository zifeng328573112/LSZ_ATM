package com.lsz.atm.json.commodity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Keep;

import java.util.List;

@Keep
public class OptionsBean implements Parcelable {

    public String choiceId;//所属的属性id
    public String id; //该属性值id
    public String name;//属性值名
    public String priceList; //显示单价/原价
    public String priceSell; //销售单价/现价
    public boolean showPrice; //是否显示价格
    public boolean showThis; //是否显示该选项
    public boolean selected;//是否默认
    public String ranking; //顺序
    public List<String> awayOptions;//互斥规格选项/ctr_product_option.id:本尊出现，列表内消失

    protected OptionsBean(Parcel in) {
        choiceId = in.readString();
        id = in.readString();
        name = in.readString();
        priceList = in.readString();
        priceSell = in.readString();
        showPrice = in.readByte() != 0;
        showThis = in.readByte() != 0;
        selected = in.readByte() != 0;
        ranking = in.readString();
        awayOptions = in.createStringArrayList();
    }

    public static final Creator<OptionsBean> CREATOR = new Creator<OptionsBean>() {
        @Override
        public OptionsBean createFromParcel(Parcel in) {
            return new OptionsBean(in);
        }

        @Override
        public OptionsBean[] newArray(int size) {
            return new OptionsBean[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(choiceId);
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(priceList);
        parcel.writeString(priceSell);
        parcel.writeByte((byte) (showPrice ? 1 : 0));
        parcel.writeByte((byte) (showThis ? 1 : 0));
        parcel.writeByte((byte) (selected ? 1 : 0));
        parcel.writeString(ranking);
        parcel.writeStringList(awayOptions);
    }

    @Override
    public String toString() {
        return "OptionsBean{" +
                "choiceId='" + choiceId + '\'' +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", priceList='" + priceList + '\'' +
                ", priceSell='" + priceSell + '\'' +
                ", showPrice=" + showPrice +
                ", showThis=" + showThis +
                ", selected=" + selected +
                ", ranking='" + ranking + '\'' +
                ", awayOptions=" + awayOptions +
                '}';
    }
}
