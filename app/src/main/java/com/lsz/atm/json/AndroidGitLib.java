package com.lsz.atm.json;

import androidx.annotation.Keep;

import java.util.List;

@Keep
public class AndroidGitLib {

    private int total_count;
    private boolean incomplete_results;
    private List<AndroidLib> items;

    public int getTotal_count() {
        return total_count;
    }

    public void setTotal_count(int total_count) {
        this.total_count = total_count;
    }

    public boolean isIncomplete_results() {
        return incomplete_results;
    }

    public void setIncomplete_results(boolean incomplete_results) {
        this.incomplete_results = incomplete_results;
    }

    public List<AndroidLib> getItems() {
        return items;
    }

    public void setItems(List<AndroidLib> items) {
        this.items = items;
    }
}
