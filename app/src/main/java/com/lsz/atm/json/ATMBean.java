package com.lsz.atm.json;


import androidx.annotation.Keep;

@Keep
public class ATMBean {

    public static String DINE_IN = "5610301";//堂吃
    public static String PICK_UP = "5610302";//自提

    public String storeId = "2800001";
    public String storeName;
    public String orderType;

    @Override
    public String toString() {
        return "ATMBean{" +
                "storeId='" + storeId + '\'' +
                ", storeName='" + storeName + '\'' +
                ", orderType='" + orderType + '\'' +
                '}';
    }
}
