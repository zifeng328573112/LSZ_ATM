package com.lsz.atm.json;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Keep;

@Keep
public class OrderBean implements Parcelable {
    /**
     * id : 1200
     * createDt : 2019-11-07 16:10:04
     * modifyDt : 1000-01-01 00:00:00
     * deleteDt : 1000-01-01 00:00:00
     * commitId : 1200
     * clientType : 5520103
     * clientId : 0
     * storeId : 2800001
     * regionId : 1000900
     * orderSeq : SS1200
     * currency : CNY
     * amountSubtotal : 0.00
     * amountShipping : 0
     * amountDiscount : 0
     * amountCoupon : 0
     * amountPoint : 0
     * amountTaxes : 0
     * amountOther : 0
     * amountTotal : 0.00
     * channelInfo :
     * buyerRemark :
     * storeRemark :
     * buyerReceipt :
     * storeReceipt :
     * packing : true
     * addressId : 0
     * sendoutInfo :
     * sendouterId : 0
     * outermanId : 0
     * timezone : 10100
     * orderNewLdt : 2019-11-07 16:10:04
     * orderPayLdt : 1000-01-01 00:00:00
     * storeGetLdt : 1000-01-01 00:00:00
     * storeEndLdt : 1000-01-01 00:00:00
     * outerGetLdt : 1000-01-01 00:00:00
     * guestGetLdt : 1000-01-01 00:00:00
     * canceledLdt : 1000-01-01 00:00:00
     * status : 5610201
     */

    public String id = "";// '主键',
    public String createDt = "";//'创建日时（系统）',
    public String modifyDt = "";// '1000-01-01' ON UPDATE CURRENT_TIMESTAMP COMMENT '修改日时（系统）',
    public String deleteDt = "";//'1000-01-01' COMMENT '标记删除',
    public String commitId = "";// '提交id',
    public String clientType = "";//'购买者类型:',
    public String clientId = "";//'购买者id:ctr_buyer.id|ctr_clerk.id',
    public String storeId = "";//'售卖店id:ctr_store.id',
    public String regionId = "";// '订单区域:ctr_standard_region.id',
    public String orderSeq = "";// '订单流水号|取餐码:可以利用id特性',
    public String orderFrom = "";// '订单来源/56101##',
    public String orderNum = "";//取餐码
    public String currency = "";//'结算货币:ctr_standard_currency',
    public String amountSubtotal = "";//'商品金额（小计），正数',
    public String amountShipping = "";// '运费金额，正',
    public String amountDiscount = "";// '折扣金额，负',
    public String amountCoupon = "";// '券卡抵值，负',
    public String amountPoint = "";//'积分抵值，负',
    public String amountTaxes = "";// '税金总额，正',
    public String amountOther = "";// '其他调整，正/负，要注明原因',
    public String amountTotal = "";// '总计金额，以上综合',
    public String amountActual = "";// '实付金额:抹零',
    public String channelInfo = "";//'付通道类型/56203##:逗号分割',
    public String buyerRemark = "";// '购买者备注',
    public String storeRemark = "";//'售卖店备注',
    public String buyerReceipt = "";// '客人小票:金额构成等，尤其其他调整',
    public String storeReceipt = "";// '门店小票:金额构成等，尤其其他调整',
    public String orderType = ATMBean.DINE_IN;// '是否打包',
    public String addressId = "";// '外送地址id:ctr_buyer_address.id, 0表示非外送',
    public String sendoutInfo = "";// '地址/电话/姓名',
    public String sendouterId = "";// '外送公司:ctr_sendouter.id, 0表示非外送',
    public String outermanId = "";// '外送小哥:ctr_outerman.id, 0表示非外送',
    public String timezone = "";// '本地时区',
    public String orderNewLdt = "";// '订单创建日时(系统)',
    public String orderPayLdt = "";// '付款完成日时(系统)',
    public String storeGetLdt = "";// '标签打印日时(系统)',
    public String storeEndLdt = "";// '制作完成日时(系统)',
    public String outerGetLdt = "";// '外送取件日时(系统)',
    public String guestGetLdt = "";// '客人签收日时(系统)',
    public String canceledLdt = "";// '取消日时(系统)',
    public String status = "";// '订单状态/56102##',
    public String noticeUrl = "";//


    protected OrderBean(Parcel in) {
        id = in.readString();
        createDt = in.readString();
        modifyDt = in.readString();
        deleteDt = in.readString();
        commitId = in.readString();
        clientType = in.readString();
        clientId = in.readString();
        storeId = in.readString();
        regionId = in.readString();
        orderSeq = in.readString();
        orderFrom = in.readString();
        currency = in.readString();
        amountSubtotal = in.readString();
        amountShipping = in.readString();
        amountDiscount = in.readString();
        amountCoupon = in.readString();
        amountPoint = in.readString();
        amountTaxes = in.readString();
        amountOther = in.readString();
        amountTotal = in.readString();
        amountActual = in.readString();
        channelInfo = in.readString();
        buyerRemark = in.readString();
        storeRemark = in.readString();
        buyerReceipt = in.readString();
        storeReceipt = in.readString();
        orderType = in.readString();
        addressId = in.readString();
        sendoutInfo = in.readString();
        sendouterId = in.readString();
        outermanId = in.readString();
        timezone = in.readString();
        orderNewLdt = in.readString();
        orderPayLdt = in.readString();
        storeGetLdt = in.readString();
        storeEndLdt = in.readString();
        outerGetLdt = in.readString();
        guestGetLdt = in.readString();
        canceledLdt = in.readString();
        status = in.readString();
        noticeUrl = in.readString();
        orderNum = in.readString();
    }

    public static final Creator<OrderBean> CREATOR = new Creator<OrderBean>() {
        @Override
        public OrderBean createFromParcel(Parcel in) {
            return new OrderBean(in);
        }

        @Override
        public OrderBean[] newArray(int size) {
            return new OrderBean[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(createDt);
        parcel.writeString(modifyDt);
        parcel.writeString(deleteDt);
        parcel.writeString(commitId);
        parcel.writeString(clientType);
        parcel.writeString(clientId);
        parcel.writeString(storeId);
        parcel.writeString(regionId);
        parcel.writeString(orderSeq);
        parcel.writeString(orderFrom);
        parcel.writeString(currency);
        parcel.writeString(amountSubtotal);
        parcel.writeString(amountShipping);
        parcel.writeString(amountDiscount);
        parcel.writeString(amountCoupon);
        parcel.writeString(amountPoint);
        parcel.writeString(amountTaxes);
        parcel.writeString(amountOther);
        parcel.writeString(amountTotal);
        parcel.writeString(amountActual);
        parcel.writeString(channelInfo);
        parcel.writeString(buyerRemark);
        parcel.writeString(storeRemark);
        parcel.writeString(buyerReceipt);
        parcel.writeString(storeReceipt);
        parcel.writeString(orderType);
        parcel.writeString(addressId);
        parcel.writeString(sendoutInfo);
        parcel.writeString(sendouterId);
        parcel.writeString(outermanId);
        parcel.writeString(timezone);
        parcel.writeString(orderNewLdt);
        parcel.writeString(orderPayLdt);
        parcel.writeString(storeGetLdt);
        parcel.writeString(storeEndLdt);
        parcel.writeString(outerGetLdt);
        parcel.writeString(guestGetLdt);
        parcel.writeString(canceledLdt);
        parcel.writeString(status);
        parcel.writeString(noticeUrl);
        parcel.writeString(orderNum);
    }
}
