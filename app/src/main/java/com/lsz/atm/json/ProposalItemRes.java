package com.lsz.atm.json;


import androidx.annotation.Keep;

import java.util.List;

@Keep
public class ProposalItemRes {
    public List<Choice> choices; //该建议包含的属性
    public List<Bundle> bundles; //当前建议为套餐时,才有值

    // ///单品///
    @Keep
    public class Choice {
        public String id;            //该选择id
        public String name;        //选择名字
        public String skuClass;   //是否SKU级:单选，必选，不灰显
        public String required;   //是否必选
        public String multiple;   //是否多选
        public String ranking;        //顺序
        public List<Option> options; //该属性所包含的属性值
    }

    @Keep
    public class Option {
        public String choiceId;          //所属的属性id
        public String id;                //该属性值id
        public String name;            //属性值名
        public String priceList;   //显示单价/原价
        public String priceSell;   //销售单价/现价
        public String showPrice;      //是否显示价格
        public String showThis;       //是否显示该选项
        public String selected;       //是否默认
        public String ranking;            //顺序
        public List<Long> awayOptions; //互斥规格选项/ctr_product_option.id:本尊出现，列表内消失
    }

    // ///套餐///
    @Keep
    public class Bundle {
        public String id;
        public String name;                //显示名字
        public String buyNum;                 //限定份数(含)
        public String ranking;                //顺序
        public List<Commodity> commodities;//所包含商品

    }

    @Keep
    public class Commodity {
        public String id;                //商品id
        public String bundleId;          //所属id
        public String name;            //商品名
        public String priceList;   //显示单价/原价
        public String priceSell;   //销售单价/现价
        public String buyMin;             //至少份数(含)
        public String buyMax;             //最多份数(含)
        public String imgIcon;         //图标URL:/x/icon.jpg->/x/icon-32x32.jpg
        public String imgMini;         //小图URL
        public String showPrice;      //是否显示价格
        public String selected;       //是否默认
        public String ranking;            //顺序
    }
}