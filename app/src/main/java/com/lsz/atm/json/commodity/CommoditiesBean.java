package com.lsz.atm.json.commodity;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import androidx.annotation.Keep;

@Keep
public class CommoditiesBean implements BundlesTemporary, Parcelable {

    public String proComId;
    public String bundleId;
    public String name;
    public String priceList;
    public String priceSell;
    public String buyMin = "0";
    public String buyMax = "0";
    public String imgIcon;
    public String imgMini;
    public boolean showPrice;
    public boolean selected;
    public String ranking;

    public boolean addSelect = true;
    public boolean isRequired = false;
    public String commodiTotalPrice = "0";

    public CommoditiesBean() {
        super();
    }

    protected CommoditiesBean(Parcel in) {
        proComId = in.readString();
        bundleId = in.readString();
        name = in.readString();
        priceList = in.readString();
        priceSell = in.readString();
        buyMin = in.readString();
        buyMax = in.readString();
        imgIcon = in.readString();
        imgMini = in.readString();
        showPrice = in.readByte() != 0;
        selected = in.readByte() != 0;
        ranking = in.readString();
        addSelect = in.readByte() != 0;
        commodiTotalPrice = in.readString();
    }

    public static final Creator<CommoditiesBean> CREATOR = new Creator<CommoditiesBean>() {
        @Override
        public CommoditiesBean createFromParcel(Parcel in) {
            return new CommoditiesBean(in);
        }

        @Override
        public CommoditiesBean[] newArray(int size) {
            return new CommoditiesBean[size];
        }
    };

    public String getProComId() {
        return proComId;
    }

    public void setProComId(String proComId) {
        this.proComId = proComId;
    }

    public String getBundleId() {
        return bundleId;
    }

    public void setBundleId(String bundleId) {
        this.bundleId = bundleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPriceList() {
        return priceList;
    }

    public void setPriceList(String priceList) {
        this.priceList = priceList;
    }

    public String getPriceSell() {
        return priceSell;
    }

    public void setPriceSell(String priceSell) {
        this.priceSell = priceSell;
    }

    public String getBuyMin() {
        return buyMin;
    }

    public void setBuyMin(String buyMin) {
        this.buyMin = buyMin;
    }

    public String getBuyMax() {
        return buyMax;
    }

    public void setBuyMax(String buyMax) {
        this.buyMax = buyMax;
    }

    public String getImgIcon() {
        return imgIcon;
    }

    public void setImgIcon(String imgIcon) {
        this.imgIcon = imgIcon;
    }

    public String getImgMini() {
        return imgMini;
    }

    public void setImgMini(String imgMini) {
        this.imgMini = imgMini;
    }

    public boolean isShowPrice() {
        return showPrice;
    }

    public void setShowPrice(boolean showPrice) {
        this.showPrice = showPrice;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getRanking() {
        return ranking;
    }

    public void setRanking(String ranking) {
        this.ranking = ranking;
    }

    @Override
    public int getmType() {
        if (isRequired) {
            return 1;
        } else {
            return 2;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(proComId);
        parcel.writeString(bundleId);
        parcel.writeString(name);
        parcel.writeString(priceList);
        parcel.writeString(priceSell);
        parcel.writeString(buyMin);
        parcel.writeString(buyMax);
        parcel.writeString(imgIcon);
        parcel.writeString(imgMini);
        parcel.writeByte((byte) (showPrice ? 1 : 0));
        parcel.writeByte((byte) (selected ? 1 : 0));
        parcel.writeString(ranking);
        parcel.writeByte((byte) (addSelect ? 0 : 1));
        parcel.writeString(commodiTotalPrice);
    }

    @Override
    public String toString() {
        return "CommoditiesBean{" +
                "proComId='" + proComId + '\'' +
                ", bundleId='" + bundleId + '\'' +
                ", name='" + name + '\'' +
                ", priceList='" + priceList + '\'' +
                ", priceSell='" + priceSell + '\'' +
                ", buyMin='" + buyMin + '\'' +
                ", buyMax='" + buyMax + '\'' +
                ", imgIcon='" + imgIcon + '\'' +
                ", imgMini='" + imgMini + '\'' +
                ", showPrice=" + showPrice +
                ", selected=" + selected +
                ", ranking='" + ranking + '\'' +
                ", addSelect=" + addSelect +
                ", commodiTotalPrice=" + commodiTotalPrice +
                '}';
    }
}