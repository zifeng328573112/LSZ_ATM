package com.lsz.atm.json;

import androidx.annotation.Keep;

import java.util.List;

@Keep
public class LoginEntity {


    /**
     * accessToken : {"access_token":"WG-VH27VH-FXT22-GAXHH4X7U423RW","token_type":"bearer","refresh_token":"5b7dcf2e-4ad0-475c-aec5-a015a32319ad","expires_in":"2588441","scope":"any"}
     * storeInfo : {"id":"2800001","name":"虹桥天街店","storeNum":"2800001"}
     * timings : [{"timingType":"1200201","startDt":"10:00","closeDt":"15:00"},{"timingType":"1200202","startDt":"10:00","closeDt":"15:00"},{"timingType":"1200203","startDt":"10:00","closeDt":"15:00"}]
     */

    public AccessTokenBean accessToken;
    public StoreInfoBean storeInfo;
    public List<TimingsBean> timings;

    @Keep
    public class AccessTokenBean {
        /**
         * access_token : WG-VH27VH-FXT22-GAXHH4X7U423RW
         * token_type : bearer
         * refresh_token : 5b7dcf2e-4ad0-475c-aec5-a015a32319ad
         * expires_in : 2588441
         * scope : any
         */

        public String access_token;
        public String token_type;
        public String refresh_token;
        public String expires_in;
        public String scope;
    }

    @Keep
    public class StoreInfoBean {
        /**
         * id : 2800001
         * name : 虹桥天街店
         * storeNum : 2800001
         */

        public String id;
        public String name;
        public String storeNum;
    }

    @Keep
    public class TimingsBean {
        /**
         * timingType : 1200201
         * startDt : 10:00
         * closeDt : 15:00
         */

        public String timingType;
        public String startDt;
        public String closeDt;

    }
}
