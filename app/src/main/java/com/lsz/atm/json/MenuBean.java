package com.lsz.atm.json;

import androidx.annotation.Keep;

@Keep
public class MenuBean {

    public String id;//方案id
    public String imgIcon;//图片
    public String name;//菜单名字
    public boolean menuSelect = false;//指针指向item

    @Override
    public String toString() {
        return "MenuBean{" +
                "id=" + id +
                ", imgIcon='" + imgIcon + '\'' +
                ", name='" + name + '\'' +
                ", menuSelect=" + menuSelect +
                '}';
    }
}
