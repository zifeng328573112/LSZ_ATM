package com.lsz.atm.json.commodity;

import androidx.annotation.Keep;

import com.lsz.atm.json.McTemporary;

import java.util.List;

@Keep
public class CommodityDataBean implements McTemporary {

    public String menuId;
    public String cTitle;
    public List<ProposalsBean> proposals;

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public List<ProposalsBean> getProposals() {
        return proposals;
    }

    public void setProposals(List<ProposalsBean> proposals) {
        this.proposals = proposals;
    }

    public String getcTitle() {
        return cTitle;
    }

    public void setcTitle(String cTitle) {
        this.cTitle = cTitle;
    }

    @Override
    public int getmType() {
        return 1;
    }

    @Override
    public String toString() {
        return "CommodityDataBean{" +
                "menuId='" + menuId + '\'' +
                ", cTitle='" + cTitle + '\'' +
                ", proposals=" + proposals +
                '}';
    }
}
