package com.lsz.atm.json;

import androidx.annotation.Keep;

@Keep
public class LabelBean {

    public LabelBean(long labelId, String labelName) {
        this.labelId = labelId;
        this.labelName = labelName;
    }

    public long labelId;
    public String labelName;
}