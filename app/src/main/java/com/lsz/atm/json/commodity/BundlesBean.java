package com.lsz.atm.json.commodity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Keep;

import java.util.List;

@Keep
public class BundlesBean implements BundlesTemporary, Parcelable {

    public String id = "0";
    public String name;
    public String buyNum;
    public String ranking;
    public List<CommoditiesBean> commodities;

    public int indexOf;

    public BundlesBean() {
        super();
    }



    public static final Creator<BundlesBean> CREATOR = new Creator<BundlesBean>() {
        @Override
        public BundlesBean createFromParcel(Parcel in) {
            return new BundlesBean(in);
        }

        @Override
        public BundlesBean[] newArray(int size) {
            return new BundlesBean[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBuyNum() {
        return buyNum;
    }

    public void setBuyNum(String buyNum) {
        this.buyNum = buyNum;
    }

    public String getRanking() {
        return ranking;
    }

    public void setRanking(String ranking) {
        this.ranking = ranking;
    }

    public List<CommoditiesBean> getCommodities() {
        return commodities;
    }

    public void setCommodities(List<CommoditiesBean> commodities) {
        this.commodities = commodities;
    }


    @Override
    public int getmType() {
        return 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(buyNum);
        parcel.writeString(ranking);
        parcel.writeTypedList(commodities);
    }

    public BundlesBean(Parcel in) {
        id = in.readString();
        name = in.readString();
        buyNum = in.readString();
        ranking = in.readString();
        commodities = in.createTypedArrayList(CommoditiesBean.CREATOR);
    }

    @Override
    public String toString() {
        return "BundlesBean{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", buyNum='" + buyNum + '\'' +
                ", ranking='" + ranking + '\'' +
                ", commodities=" + commodities +
                '}';
    }
}