package com.lsz.atm.json.commodity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Keep;

import com.lsz.atm.json.McTemporary;

import java.util.List;

@Keep
public class ProposalsBean implements McTemporary, Parcelable {
    public String menuId;//
    public String id;//
    public String name;//
    public String alias;//
    public String desc;//
    public String imgMini;//
    public String imgIcon;//
    public String imgFace;//
    public String priceList;//
    public String priceSell;//
    public String ranking;//
    public String today_start; //
    public String today_end;//
    public boolean pauseSell;//是否停售
    public List<ChoicesBean> choices;
    public List<BundlesBean> bundles;

    public long cSelectNumber;// 产品选择数量
    public String totalPriceList;//总金额
    public boolean isManySpe = false;//多规格弹窗

    public static final Creator<ProposalsBean> CREATOR = new Creator<ProposalsBean>() {
        @Override
        public ProposalsBean createFromParcel(Parcel in) {
            return new ProposalsBean(in);
        }

        @Override
        public ProposalsBean[] newArray(int size) {
            return new ProposalsBean[size];
        }
    };

    @Override
    public int getmType() {
        return 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(menuId);
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(alias);
        parcel.writeString(desc);
        parcel.writeString(imgMini);
        parcel.writeString(imgIcon);
        parcel.writeString(imgFace);
        parcel.writeString(priceList);
        parcel.writeString(priceSell);
        parcel.writeString(ranking);
        parcel.writeLong(cSelectNumber);
        parcel.writeString(totalPriceList);
        parcel.writeByte((byte) (pauseSell ? 1 : 0));
        parcel.writeByte((byte) (isManySpe ? 1 : 0));
        parcel.writeTypedList(choices);
        parcel.writeTypedList(bundles);
    }

    private ProposalsBean(Parcel in) {
        menuId = in.readString();
        id = in.readString();
        name = in.readString();
        alias = in.readString();
        desc = in.readString();
        imgMini = in.readString();
        imgIcon = in.readString();
        imgFace = in.readString();
        priceList = in.readString();
        priceSell = in.readString();
        ranking = in.readString();
        cSelectNumber = in.readLong();
        totalPriceList = in.readString();
        pauseSell = in.readByte() != 0;
        isManySpe = in.readByte() != 0;
        choices = in.createTypedArrayList(ChoicesBean.CREATOR);
        bundles = in.createTypedArrayList(BundlesBean.CREATOR);
    }

    @Override
    public String toString() {
        return "ProposalsBean{" +
                "menuId='" + menuId + '\'' +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", alias='" + alias + '\'' +
                ", desc='" + desc + '\'' +
                ", imgMini='" + imgMini + '\'' +
                ", imgIcon='" + imgIcon + '\'' +
                ", imgFace='" + imgFace + '\'' +
                ", priceList='" + priceList + '\'' +
                ", priceSell='" + priceSell + '\'' +
                ", ranking='" + ranking + '\'' +
                ", today_start='" + today_start + '\'' +
                ", today_end='" + today_end + '\'' +
                ", pauseSell=" + pauseSell +
                ", choices=" + choices +
                ", bundles=" + bundles +
                ", cSelectNumber=" + cSelectNumber +
                ", totalPriceList='" + totalPriceList + '\'' +
                ", isManySpe=" + isManySpe +
                '}';
    }
}