package com.lsz.atm.json;

import androidx.annotation.Keep;

@Keep
public class OrderNumberBean {

    public String orderTotalNumber = "0";

    public OrderNumberBean() {
        super();
    }

    public OrderNumberBean(String orderTotalNumber) {
        this.orderTotalNumber = orderTotalNumber;
    }
}
