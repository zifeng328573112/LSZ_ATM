package com.lsz.atm.json;

import androidx.annotation.Keep;

import java.io.Serializable;

@Keep
public interface McTemporary extends Serializable {

    int getmType();

}