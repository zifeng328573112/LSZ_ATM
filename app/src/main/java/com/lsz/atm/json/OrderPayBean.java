package com.lsz.atm.json;

import androidx.annotation.Keep;

@Keep
public class OrderPayBean {


    /**
     * merchant_order_sn : A1902
     * order_sn : 20191111171151393429
     * trade_state : SUCCESS
     * platform_order_no : 4200000449201911113313726281
     * total_fee : 0.01
     * order_price : 0.01
     * trade_no : 2019111117115101497819526526N
     * pay_time : 1573463513
     * type : 1
     * attach : 订单: 1902
     * store_id : 898782
     * cashier_id : 0
     * device_no : 0
     * body : 上海粮全其美餐饮管理有限公司
     * user_id : ogRDxv1IT8c--K1flq_eDfCSmiEU
     * user_logon_id : null
     * fee : 0
     * discount_money : 0
     * buyer_pay_amount : 0.01
     */

    public String merchant_order_sn;// 是 32 第三方商户的订单号
    public String platform_order_no;// 是 32 平台方订单号
    public float total_fee;// 是 10 实收金额(元)
    public float order_price;// 是 10 订单金额
    public String trade_no;// 是 32 商户单号
    public int pay_time;// int 是 10 交易成功的时间(单位:秒)
    public String attach;// 否 127 附加数据
    public int store_id;// 是 10 付呗系统的门店id
    public int cashier_id;// 否 16 付呗系统的收银员id
    public String device_no;// 否 32 设备终端号
    public String body;// 否 128 对商品或交易的描述
    public String user_id;// 否 32 微信顾客支付授权的“open_id”或者支付宝顾客的“buyer_user_id”
    public String user_logon_id;// 否 32 支付宝顾客的账号
    public float fee;// 是 10 手续费(元)
    public int type;
    public String order_sn;
    public String trade_state;
    public float discount_money;
    public float buyer_pay_amount;
}
