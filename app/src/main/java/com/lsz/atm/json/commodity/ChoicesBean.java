package com.lsz.atm.json.commodity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Keep;

import java.util.List;

@Keep
public class ChoicesBean implements Parcelable {

    public String id;  //该选择id
    public String name; //选择名字
    public boolean skuClass; //是否SKU级:单选，必选，不灰显
    public boolean required;//是否必选
    public boolean multiple; //是否多选
    public String ranking;//顺序
    public List<OptionsBean> options;//该属性所包含的属性值

    public static final Creator<ChoicesBean> CREATOR = new Creator<ChoicesBean>() {
        @Override
        public ChoicesBean createFromParcel(Parcel in) {
            return new ChoicesBean(in);
        }

        @Override
        public ChoicesBean[] newArray(int size) {
            return new ChoicesBean[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeByte((byte) (skuClass ? 1 : 0));
        parcel.writeByte((byte) (required ? 1 : 0));
        parcel.writeByte((byte) (multiple ? 1 : 0));
        parcel.writeString(ranking);
        parcel.writeTypedList(options);
    }

    protected ChoicesBean(Parcel in) {
        id = in.readString();
        name = in.readString();
        skuClass = in.readByte() != 0;
        required = in.readByte() != 0;
        multiple = in.readByte() != 0;
        ranking = in.readString();
        options = in.createTypedArrayList(OptionsBean.CREATOR);
    }

    @Override
    public String toString() {
        return "ChoicesBean{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", skuClass=" + skuClass +
                ", required=" + required +
                ", multiple=" + multiple +
                ", ranking='" + ranking + '\'' +
                ", options=" + options +
                '}';
    }
}