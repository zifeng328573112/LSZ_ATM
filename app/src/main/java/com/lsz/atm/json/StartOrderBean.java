package com.lsz.atm.json;


import androidx.annotation.Keep;

import java.util.List;

@Keep
public class StartOrderBean {


    /**
     * storeName : 虹桥天街店
     * advertising : ["http://static.lianglife.com/homePage.png"]
     */

    public String storeName;
    public List<String> advertising;

    @Override
    public String toString() {
        return "StartOrderBean{" +
                "storeName='" + storeName + '\'' +
                ", advertising=" + advertising +
                '}';
    }
}
