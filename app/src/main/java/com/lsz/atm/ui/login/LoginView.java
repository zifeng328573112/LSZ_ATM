package com.lsz.atm.ui.login;

import com.lsz.atm.json.StartOrderBean;
import com.util_code.base.mvp.MvpView;

public interface LoginView extends MvpView {

    void onHomeBanner(StartOrderBean startOrderBean);

    <T> void onSuccess(T t);

    void onFail();

}
