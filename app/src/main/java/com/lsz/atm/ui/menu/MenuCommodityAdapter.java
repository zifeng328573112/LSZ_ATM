package com.lsz.atm.ui.menu;

import android.content.Context;
import android.graphics.Paint;
import android.net.Uri;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.lsz.atm.R;
import com.lsz.atm.json.McTemporary;
import com.lsz.atm.json.commodity.CommodityDataBean;
import com.lsz.atm.json.commodity.ProposalsBean;
import com.lsz.atm.viewutil.BigDecimalUtils;
import com.lsz.atm.viewutil.SpannableUtil;
import com.util_code.utils.DateUtils;
import com.util_code.widget.recycleadpter.BaseRecycleViewAdapter;

import org.jetbrains.annotations.NotNull;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MenuCommodityAdapter extends BaseRecycleViewAdapter<McTemporary> {

    private CommodityClicklistener commodityClicklistener;

    public MenuCommodityAdapter(Context context, CommodityClicklistener commodityClicklistener) {
        super(context);
        this.commodityClicklistener = commodityClicklistener;
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, McTemporary item) {
        if (holder instanceof CommodityHolder) {
            CommodityHolder commodityHolder = (CommodityHolder) holder;
            ProposalsBean commodityBean = (ProposalsBean) item;
            RequestOptions requestOptions = new RequestOptions();
            requestOptions = requestOptions.centerCrop();
            requestOptions =  requestOptions.placeholder(mContext.getResources().getDrawable(R.drawable.bg_menu_food));
            Glide.with(mContext).load(commodityBean.imgMini).apply(requestOptions).downloadOnly(new SimpleTarget<File>() {
                @Override
                public void onResourceReady(@NotNull File resource, Transition<? super File> transition) {
                    Uri uri = Uri.fromFile(resource);
                    commodityHolder.ivCommodityImg.setImageURI(uri);
                }
            });
            commodityHolder.tvCommodityName.setText(commodityBean.name);
            commodityHolder.tvCommodityEnglishName.setText(commodityBean.alias);
            String price = BigDecimalUtils.round(commodityBean.priceList, 2);
            String vip = BigDecimalUtils.round(commodityBean.priceSell, 2);
            if (TextUtils.equals(price, vip)) {
                commodityHolder.tvCommoditySell.setVisibility(View.INVISIBLE);
            } else {
                commodityHolder.tvCommoditySell.setVisibility(View.VISIBLE);
                String orderSell = mContext.getResources().getString(R.string.order_sell) + price;
                commodityHolder.tvCommoditySell.setText(orderSell);
                commodityHolder.tvCommoditySell.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
            }
            String amount = mContext.getResources().getString(R.string.currency) + vip;
            SpannableString textSpannable = SpannableUtil.setTextSpannable(amount, 1, amount.length());
            commodityHolder.tvCommodityPrice.setText(textSpannable);
            if (commodityBean.cSelectNumber == 0) {
                commodityHolder.tvCommodityNumber.setVisibility(View.INVISIBLE);
                commodityHolder.ivCommodityDelete.setVisibility(View.INVISIBLE);
            } else {
                commodityHolder.tvCommodityNumber.setVisibility(View.VISIBLE);
                commodityHolder.ivCommodityDelete.setVisibility(View.VISIBLE);
                String sNumber = commodityBean.cSelectNumber + "";
                commodityHolder.tvCommodityNumber.setText(sNumber);
            }
            if (commodityBean.isManySpe) {
                commodityHolder.tvCommodityManySpe.setVisibility(View.VISIBLE);
            } else {
                commodityHolder.tvCommodityManySpe.setVisibility(View.INVISIBLE);
            }

            if (getMenuIsClick(commodityBean, commodityHolder.ivMealStop, commodityHolder.tvMealTime)) {
                commodityHolder.flMeal.setVisibility(View.INVISIBLE);
                commodityHolder.tvCommodityName.setTextColor(mContext.getResources().getColor(R.color.c_1B1B1B));
                commodityHolder.tvCommodityPrice.setTextColor(mContext.getResources().getColor(R.color.c_1B1B1B));
                commodityHolder.tvCommodityEnglishName.setTextColor(mContext.getResources().getColor(R.color.c_A0A0A0));
                commodityHolder.ivCommodityAdd.setImageResource(R.drawable.icon_yellow_add);
            } else {
                commodityHolder.flMeal.setVisibility(View.VISIBLE);
                commodityHolder.tvCommodityName.setTextColor(mContext.getResources().getColor(R.color.c_C9C9C9));
                commodityHolder.tvCommodityPrice.setTextColor(mContext.getResources().getColor(R.color.c_C9C9C9));
                commodityHolder.tvCommodityEnglishName.setTextColor(mContext.getResources().getColor(R.color.c_E3E3E3));
                commodityHolder.ivCommodityAdd.setImageResource(R.drawable.icon_gray_max_add);
            }
            commodityHolder.ivCommodityDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (commodityClicklistener != null && getMenuIsClick(commodityBean, commodityHolder.ivMealStop, commodityHolder.tvMealTime)) {
                        commodityClicklistener.onCommodityDelete(commodityBean, holder.getLayoutPosition() - getHeaderLayoutCount());
                    }
                }
            });
            commodityHolder.ivCommodityAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (commodityClicklistener != null && getMenuIsClick(commodityBean, commodityHolder.ivMealStop, commodityHolder.tvMealTime)) {
                        commodityClicklistener.onCommodityAdd(commodityBean, holder.getLayoutPosition() - getHeaderLayoutCount());
                    }
                }
            });
            commodityHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (commodityClicklistener != null && getMenuIsClick(commodityBean, commodityHolder.ivMealStop, commodityHolder.tvMealTime)) {
                        commodityClicklistener.onItemClick(commodityBean, holder.getLayoutPosition() - getHeaderLayoutCount());
                    }
                }
            });
        } else if (holder instanceof CommodityTitleHolder) {
            CommodityTitleHolder titleHolder = (CommodityTitleHolder) holder;
            CommodityDataBean titleBean = (CommodityDataBean) item;
            String title = "— " + titleBean.cTitle + " —";
            titleHolder.tvCommodityTitle.setText(title);
        }
    }

    private boolean getMenuIsClick(ProposalsBean commodityBean, ImageView ivMealStop, TextView tvMealTime) {
        String todayStart = commodityBean.today_start;
        String todayEnd = commodityBean.today_end;
        if (commodityBean.pauseSell) {
            ivMealStop.setVisibility(View.VISIBLE);
            tvMealTime.setVisibility(View.INVISIBLE);
            return false;
        } else {
            if ((TextUtils.isEmpty(todayStart) && TextUtils.isEmpty(todayEnd)) || (DateUtils.compare(todayStart) && !DateUtils.compare(todayEnd))) {
                return true;
            } else {
                ivMealStop.setVisibility(View.INVISIBLE);
                tvMealTime.setVisibility(View.VISIBLE);
                String timeContent = "餐品售卖时间\n" + todayStart.substring(0, todayStart.length() - 3) + " - " + todayEnd.substring(0, todayEnd.length() - 3);
                tvMealTime.setText(timeContent);
                return false;
            }
        }
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 0:
                return new CommodityHolder(View.inflate(mContext, R.layout.item_commodity_view, null));
            case 1:
                return new CommodityTitleHolder(View.inflate(mContext, R.layout.item_commodity_title, null));
            default:
                return new CommodityHolder(View.inflate(mContext, R.layout.item_commodity_view, null));

        }
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return getData().get(position).getmType();
    }

    class CommodityHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_commodity_img)
        ImageView ivCommodityImg;
        @BindView(R.id.tv_commodity_name)
        TextView tvCommodityName;
        @BindView(R.id.tv_commodity_english_name)
        TextView tvCommodityEnglishName;
        @BindView(R.id.tv_commodity_price)
        TextView tvCommodityPrice;
        @BindView(R.id.iv_commodity_delete)
        ImageView ivCommodityDelete;
        @BindView(R.id.tv_commodity_number)
        TextView tvCommodityNumber;
        @BindView(R.id.iv_commodity_add)
        ImageView ivCommodityAdd;
        @BindView(R.id.tv_commodity_sell)
        TextView tvCommoditySell;
        @BindView(R.id.tv_commodity_many_spe)
        TextView tvCommodityManySpe;
        @BindView(R.id.fl_meal)
        FrameLayout flMeal;
        @BindView(R.id.tv_meal_time)
        TextView tvMealTime;
        @BindView(R.id.iv_meal_stop)
        ImageView ivMealStop;

        CommodityHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class CommodityTitleHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_commodity_title)
        TextView tvCommodityTitle;

        CommodityTitleHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface CommodityClicklistener {

        void onCommodityDelete(ProposalsBean commodityBean, int position);

        void onCommodityAdd(ProposalsBean commodityBean, int position);

        void onItemClick(ProposalsBean commodityBean, int position);

    }

}
