package com.lsz.atm.ui.commodity.module_view;

import android.content.Context;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.lsz.atm.R;
import com.lsz.atm.json.commodity.BundlesBean;
import com.lsz.atm.json.commodity.BundlesTemporary;
import com.lsz.atm.json.commodity.CommoditiesBean;
import com.lsz.atm.viewutil.BigDecimalUtils;
import com.lsz.atm.viewutil.SpannableUtil;
import com.util_code.dagger.help.GlideApp;
import com.util_code.widget.recycleadpter.BaseRecycleViewAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PackageAdapter extends BaseRecycleViewAdapter<BundlesTemporary> {

    private OnPackageItemListener onPackageItemListener;

    PackageAdapter(Context context, OnPackageItemListener onPackageItemListener) {
        super(context);
        this.onPackageItemListener = onPackageItemListener;
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, BundlesTemporary item) {
        if (holder instanceof TitleHolder) {
            TitleHolder titleHolder = (TitleHolder) holder;
            BundlesBean bundles = (BundlesBean) item;
            if (bundles.indexOf == -1) {
                titleHolder.tvPackageTitle.setText(bundles.name);
            } else {
                int color = mContext.getResources().getColor(R.color.c_FABE00);
                SpannableString textSpannable = SpannableUtil.setTextSpannable(bundles.name, bundles.indexOf, bundles.name.length() - 1, color);
                titleHolder.tvPackageTitle.setText(textSpannable);
            }
        } else if (holder instanceof LineHolder) {
            LineHolder lineHolder = (LineHolder) holder;
            CommoditiesBean commoditiesBean = (CommoditiesBean) item;
            GlideApp.with(mContext).load(commoditiesBean.imgIcon).into(lineHolder.ivPackageImg);
            lineHolder.tvPackageName.setText(commoditiesBean.name);
            String buyNumber = commoditiesBean.buyMin + " 份";
            lineHolder.tvPackageNumber.setText(buyNumber);
        } else if (holder instanceof GirdHolder) {
            GirdHolder girdHolder = (GirdHolder) holder;
            CommoditiesBean commoditiesBean = (CommoditiesBean) item;
            GlideApp.with(mContext).load(commoditiesBean.imgMini).into(girdHolder.ivPackageImg);
            girdHolder.tvPackageName.setText(commoditiesBean.name);
            String round = BigDecimalUtils.round(commoditiesBean.priceSell, 2);
            if (!TextUtils.equals("0", round)) {
                String sRound = "加 ¥ " + round;
                girdHolder.tvPackageAttachAmount.setText(sRound);
                girdHolder.tvPackageAttachAmount.setVisibility(View.VISIBLE);
            } else {
                girdHolder.tvPackageAttachAmount.setVisibility(View.INVISIBLE);
            }
            if (!TextUtils.equals("0", commoditiesBean.buyMin)) {
                girdHolder.ivPackageDelete.setVisibility(View.VISIBLE);
                girdHolder.tvPackageNumber.setVisibility(View.VISIBLE);
                girdHolder.tvPackageNumber.setText(commoditiesBean.buyMin);
                girdHolder.rlPackageBg.setBackgroundResource(R.drawable.bg_package_yellow);
            } else {
                girdHolder.ivPackageDelete.setVisibility(View.GONE);
                girdHolder.tvPackageNumber.setVisibility(View.GONE);
                girdHolder.rlPackageBg.setBackgroundResource(R.drawable.bg_package_gray);
            }
            if (commoditiesBean.addSelect) {
                girdHolder.ivPackageAdd.setImageResource(R.drawable.icon_yellow_add);
            } else {
                girdHolder.ivPackageAdd.setImageResource(R.drawable.icon_gray_add);
            }
            girdHolder.ivPackageAdd.setEnabled(commoditiesBean.addSelect);
            girdHolder.ivPackageAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onPackageItemListener.onItemAdd(commoditiesBean);
                }
            });
            girdHolder.ivPackageDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onPackageItemListener.onItemDelete(commoditiesBean);
                }
            });
        }
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 0:
                View titleView = LayoutInflater.from(mContext).inflate(R.layout.item_package_title, parent, false);
                return new TitleHolder(titleView);
            case 1:
                View lineView = LayoutInflater.from(mContext).inflate(R.layout.item_package_line, parent, false);
                return new LineHolder(lineView);
            case 2:
                View girdView = LayoutInflater.from(mContext).inflate(R.layout.item_package_gird, parent, false);
                return new GirdHolder(girdView);
            default:
                View inflate = LayoutInflater.from(mContext).inflate(R.layout.item_package_title, parent, false);
                return new TitleHolder(inflate);
        }
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return mData.get(position).getmType();
    }

    class TitleHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_package_title)
        TextView tvPackageTitle;

        TitleHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class LineHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_package_img)
        ImageView ivPackageImg;
        @BindView(R.id.tv_package_name)
        TextView tvPackageName;
        @BindView(R.id.tv_package_number)
        TextView tvPackageNumber;

        LineHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class GirdHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.rl_package_bg)
        RelativeLayout rlPackageBg;
        @BindView(R.id.iv_package_img)
        ImageView ivPackageImg;
        @BindView(R.id.tv_package_name)
        TextView tvPackageName;
        @BindView(R.id.tv_package_attach_amount)
        TextView tvPackageAttachAmount;
        @BindView(R.id.iv_package_delete)
        ImageView ivPackageDelete;
        @BindView(R.id.tv_package_number)
        TextView tvPackageNumber;
        @BindView(R.id.iv_package_add)
        ImageView ivPackageAdd;

        GirdHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnPackageItemListener {

        void onItemAdd(CommoditiesBean commoditiesBean);

        void onItemDelete(CommoditiesBean commoditiesBean);

    }
}
