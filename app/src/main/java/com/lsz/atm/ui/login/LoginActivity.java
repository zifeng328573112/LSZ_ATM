package com.lsz.atm.ui.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.common.config.Global;
import com.common.mvp.BaseMvpActivity;
import com.common.widght.EditTextWithDel;
import com.lsz.atm.R;
import com.lsz.atm.json.LoginEntity;
import com.lsz.atm.json.StartOrderBean;
import com.lsz.atm.ui.menu.MenuListActivity;
import com.util_code.dialog.LoadingDialog;
import com.util_code.utils.ToastUtils;

import java.util.Objects;

import javax.microedition.khronos.opengles.GL;

import butterknife.BindView;

public class LoginActivity extends BaseMvpActivity<LoginView, LoginPresent> implements LoginView, View.OnClickListener {

    @BindView(R.id.et_store_no)
    EditTextWithDel etStoreNo;
    @BindView(R.id.et_login_user)
    EditTextWithDel etLoginUser;
    @BindView(R.id.et_login_password)
    EditTextWithDel etLoginPassword;
    @BindView(R.id.tv_yellow_button)
    TextView tvYellowButton;

    private LoadingDialog loadingDialog;

    public static void startActivity(Activity activity) {
        Intent intent = new Intent(activity, LoginActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected void setTranslateStatusBar(boolean barColor) {
        barColor = false;
        super.setTranslateStatusBar(barColor);
    }

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void setupView() {
        tvYellowButton.setOnClickListener(this);

        if (loadingDialog == null) {
            loadingDialog = new LoadingDialog();
        }

        etStoreNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.i("TAG",s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        LoginEntity loginBean = Global.getLoginBean();
        if (loginBean != null && loginBean.accessToken != null && !TextUtils.isEmpty(loginBean.accessToken.access_token)) {
            StartOrderActivity.startActivity(this);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_yellow_button:
                String storeNo = Objects.requireNonNull(etStoreNo.getText()).toString();
                String loginUser = Objects.requireNonNull(etLoginUser.getText()).toString();
                String password = Objects.requireNonNull(etLoginPassword.getText()).toString();
                if (TextUtils.isEmpty(storeNo)) {
                    ToastUtils.showShortToast("请输入店号");
                    return;
                } else if (TextUtils.isEmpty(loginUser)) {
                    ToastUtils.showShortToast("请输入用户名");
                    return;
                } else if (TextUtils.isEmpty(password)) {
                    ToastUtils.showShortToast("请输入密码");
                    return;
                } else {
                    if (!loadingDialog.isVisible()) {
                        loadingDialog.show(getSupportFragmentManager(), "loadingdialog");
                    }
                    getPresenter().postLogin(storeNo, loginUser, password);
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void onHomeBanner(StartOrderBean startOrderBean) {
        loadingDialog.dismissAllowingStateLoss();
    }

    @Override
    public <T> void onSuccess(T t) {
        loadingDialog.dismissAllowingStateLoss();
        if (t instanceof LoginEntity) {
            StartOrderActivity.startActivity(this);
        }
    }

    @Override
    public void onFail() {
        loadingDialog.dismissAllowingStateLoss();
    }
}
