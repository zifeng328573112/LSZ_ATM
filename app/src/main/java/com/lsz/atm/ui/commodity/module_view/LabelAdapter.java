package com.lsz.atm.ui.commodity.module_view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.lsz.atm.R;
import com.lsz.atm.json.ProductChoiceBean;
import com.util_code.widget.recycleadpter.BaseRecycleViewAdapter;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LabelAdapter extends BaseRecycleViewAdapter<ProductChoiceBean.ProductOptionBean> {

    private LabelItemClickListener labelItemClickListener;

    public LabelAdapter(Context context, LabelItemClickListener labelItemClickListener) {
        super(context);
        this.labelItemClickListener = labelItemClickListener;
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, ProductChoiceBean.ProductOptionBean item) {
        if (holder instanceof LabelHolder) {
            LabelHolder labelHolder = (LabelHolder) holder;
            if (item.showPrice) {
                String currency = mContext.getResources().getString(R.string.currency);
                String s = item.optionName + " " + currency + item.price;
                labelHolder.btnLabelSelect.setText(s);
            } else {
                labelHolder.btnLabelSelect.setText(item.optionName);
            }

            if (!getAwayOption(item.choiceId, item.awayOptions)) {
                labelHolder.btnLabelSelect.setEnabled(true);
                if (item.selected) {
                    labelHolder.btnLabelSelect.setBackgroundResource(R.drawable.btn_label_yellow);
                } else {
                    labelHolder.btnLabelSelect.setBackgroundResource(R.drawable.btn_label_white);
                }
            } else {
                labelHolder.btnLabelSelect.setBackgroundResource(R.drawable.btn_label_gray);
                labelHolder.btnLabelSelect.setEnabled(false);
            }
            labelHolder.btnLabelSelect.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    if (labelItemClickListener != null) {
                        labelItemClickListener.onItemClick(item);
                    }
                }

            });
        }
    }

    private boolean getAwayOption(long choiceId, long[] awayOptions) {
        if (choiceId == 0 || awayOptions.length == 0) {
            return false;
        }
        Arrays.sort(awayOptions);
        int binarySearch = Arrays.binarySearch(awayOptions, choiceId);
        return binarySearch > 0;
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_label_btn, parent, false);
        return new LabelHolder(view);
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    class LabelHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.btn_label_select)
        TextView btnLabelSelect;

        LabelHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface LabelItemClickListener {

        void onItemClick(ProductChoiceBean.ProductOptionBean item);

    }

}
