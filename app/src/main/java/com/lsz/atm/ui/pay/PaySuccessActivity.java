package com.lsz.atm.ui.pay;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.common.CountDownUtlis;
import com.common.config.Api;
import com.common.config.Global;
import com.common.mvp.BaseMvpActivity;
import com.common.thirdparty.PrintBuilder;
import com.common.thirdparty.ThirdParty;
import com.common.thirdparty.aiyin.AiYinParty;
import com.lsz.atm.R;
import com.lsz.atm.db.bean.CommodityBean;
import com.lsz.atm.db.bean.PackageBean;
import com.lsz.atm.db.dao_help.GreenDaoDbHelp;
import com.lsz.atm.db.dao_utils.DaoUtils;
import com.lsz.atm.event.StartOrderEvent;
import com.lsz.atm.json.OrderBean;
import com.lsz.atm.json.OrderPayBean;
import com.lsz.atm.viewutil.BigDecimalUtils;

import org.greenrobot.eventbus.EventBus;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class PaySuccessActivity extends BaseMvpActivity<PayView, PayPresent> implements PayView, View.OnClickListener {

    @BindView(R.id.tv_take_meals_number)
    TextView tvTakeMealsNumber;
    @BindView(R.id.btn_continue_restaurant)
    Button btnContinueRestaurant;

    private CountDownUtlis downUtlis;
    private String continueRestaurant;
    private OrderBean orderBean;


    public static void startActivity(Activity activity, OrderBean orderBean, float total_fee) {
        Intent intent = new Intent(activity, PaySuccessActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("OrderBean", orderBean);
        bundle.putFloat("total_fee", total_fee);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_pay_success;
    }

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void setupView() {
        downUtlis = new CountDownUtlis(getMvpView());

        btnContinueRestaurant.setOnClickListener(this);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        List<CommodityBean> commodityList = DaoUtils.getCommodityInstance().loadAll();
        continueRestaurant = getResources().getString(R.string.pay_continue_restaurant);

        Intent intent = getIntent();
        orderBean = intent.getParcelableExtra("OrderBean");
        float total_fee = intent.getFloatExtra("total_fee", 0.0f);
        DecimalFormat decimalFormat = new DecimalFormat("0.00");//构造方法的字符格式这里如果小数不足2位,会以0补足.
        String totalFee = decimalFormat.format(total_fee);//format 返回的是字符串
        if (orderBean == null) {
            finish();
            return;
        }

        downUtlis.countDown(5, countDownListener);
        tvTakeMealsNumber.setText(orderBean.orderNum);
        Drawable drawable = getResources().getDrawable(R.drawable.img_print_logo);
        List<String> arrayList = new ArrayList<>();
        for (CommodityBean commodity : commodityList) {
            if (commodity.cSelectNumber > 0) {
                List<PackageBean> packageList = DaoUtils.getPackageInstance().loadPackageByRandomId(commodity.randomId);
                String totalA = "0";
                for (PackageBean packageBean : packageList) {
//                    String tAmount = TemporaryDataUtil.getTotalAmount(packageBean.commodiTotalPrice, packageBean.commodiTotalPrice, commodity.cSelectNumber);
                    totalA = BigDecimalUtils.add(totalA, packageBean.commodiTotalPrice, 2);
                }
                totalA = BigDecimalUtils.add(totalA, commodity.priceSell, 2);
                arrayList.add(PrintBuilder.getTextStyle(commodity.name, commodity.cSelectNumber + "", "￥" + totalA, commodity.alias));
            }
        }
        String qrCode = Api.QR_CODE_DATA + "?" + orderBean.orderSeq + "_1";
        List<PrintBuilder> printList = PrintBuilder.getPrintList(ThirdParty.PRINT_TYPE, orderBean.orderNum, Global.getAtmBean().orderType, orderBean.orderSeq, orderBean.createDt, qrCode, orderBean.amountTotal, totalFee, drawable, arrayList);
        ThirdParty.onStartPrint(printList);
    }

    private CountDownUtlis.CountDownListener countDownListener = new CountDownUtlis.CountDownListener() {

        @Override
        public void onCountStart() {
            String format = String.format(continueRestaurant, 5);
            btnContinueRestaurant.setText(format);
        }

        @Override
        public void onCountProgress(long progress) {
            String format = String.format(continueRestaurant, progress);
            btnContinueRestaurant.setText(format);
        }

        @Override
        public void onCountEnd() {
            EventBus.getDefault().post(new StartOrderEvent());
            finish();
        }

        @Override
        public void onCountError() {
            EventBus.getDefault().post(new StartOrderEvent());
            finish();
        }

    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_continue_restaurant:
                DaoUtils.getCommodityInstance().deleteAll();
                DaoUtils.getPackageInstance().deleteAll();
                EventBus.getDefault().post(new StartOrderEvent());
                finish();
                break;
        }
    }

    @Override
    public void onSuccess(OrderPayBean orderPayBean) {

    }

    @Override
    public void onStatusSuccess(boolean isTrue) {

    }

    @Override
    public void onPayError(String sub_code) {

    }


    @Override
    public void onFail() {

    }
}
