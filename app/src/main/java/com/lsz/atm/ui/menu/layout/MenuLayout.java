package com.lsz.atm.ui.menu.layout;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.OrientationHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.lsz.atm.json.McTemporary;
import com.lsz.atm.json.MenuBean;
import com.lsz.atm.json.commodity.CommodityDataBean;
import com.lsz.atm.json.commodity.ProposalsBean;
import com.lsz.atm.ui.menu.MenuAdapter;
import com.lsz.atm.ui.menu.MenuCommodityAdapter;
import com.util_code.widget.recycleadpter.BaseRecycleViewAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.Iterator;
import java.util.List;

public class MenuLayout<T extends Activity> {

    private Activity activity;
    private RecyclerView rcOneMenu;
    private RecyclerView rcCommodityList;
    private LinearLayoutManager linearLayoutManager;
    private MenuAdapter menuAdapter;
    private MenuCommodityAdapter commodityAdapter;
    private MenuLayoutListener menuLayoutListener;

    private List<MenuBean> menuBeanList;
    private List<McTemporary> commodityList;

    private boolean moveToTop = false;
    private int index;
    private boolean fromClick = false;

    public MenuLayout(T activity) {
        this.activity = activity;
    }

    public void initRecyclerManager(RecyclerView rcOneMenu, RecyclerView rcCommodityList, MenuCommodityAdapter.CommodityClicklistener commodityClicklistene) {
        this.rcOneMenu = rcOneMenu;
        this.rcCommodityList = rcCommodityList;

        linearLayoutManager = new LinearLayoutManager(activity);
        rcOneMenu.setLayoutManager(linearLayoutManager);
        menuAdapter = new MenuAdapter(activity);
        rcOneMenu.setAdapter(menuAdapter);
        menuAdapter.setOnItemClickListener(itemClickListener);

        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(activity);
        rcCommodityList.setLayoutManager(linearLayoutManager1);
        commodityAdapter = new MenuCommodityAdapter(activity, commodityClicklistene);
        rcCommodityList.setAdapter(commodityAdapter);
        rcCommodityList.addOnScrollListener(scrollListener);
        rcCommodityList.setOnTouchListener(onTouchListener);
    }

    public void setMenuLayoutListener(MenuLayoutListener menuLayoutListener) {
        this.menuLayoutListener = menuLayoutListener;
    }

    public void setCommodityList(List<McTemporary> commodityList) {
        this.commodityList = commodityList;
        commodityAdapter.resetData(commodityList);
    }

    public void setCommodityList(List<McTemporary> commodityList, McTemporary mcTemporary, int position) {
        this.commodityList = commodityList;
        commodityAdapter.notifyItemChanged(position, mcTemporary);
    }

    public void setMenuBeanList(List<MenuBean> menuBeanList) {
        this.menuBeanList = menuBeanList;
        menuAdapter.resetData(menuBeanList);
    }

    public void notifiAdapter(ProposalsBean proposalsBean) {
        for (int i = 0; i < commodityAdapter.getData().size(); i++) {
            if (commodityAdapter.getData().get(i) instanceof ProposalsBean) {
                ProposalsBean proposals = (ProposalsBean) commodityAdapter.getData().get(i);
                if (TextUtils.equals(proposals.id, proposalsBean.id)) {
                    commodityAdapter.getData().set(i, proposalsBean);
                    commodityAdapter.notifyItemChanged(i, commodityAdapter.getData().get(i));
                }
            }
        }
    }

    public void notifiAdapter(int position, ProposalsBean proposalsBean) {
        proposalsBean.isManySpe = true;
        commodityAdapter.getData().set(position, proposalsBean);
        commodityAdapter.notifyItemChanged(position, commodityAdapter.getData().get(position));
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                    Message message = new Message();
                    message.obj = proposalsBean;
                    message.what = position;
                    handler.sendMessage(message);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            ProposalsBean proposalsBean = (ProposalsBean) message.obj;
            int position = message.what;
            if (proposalsBean != null && position != -1) {
                proposalsBean.isManySpe = false;
                commodityAdapter.getData().set(position, proposalsBean);
                commodityAdapter.notifyItemChanged(position, commodityAdapter.getData().get(position));
            }
            return false;
        }
    });

    public int[] getRecyclerViewLastPosition() {
        int[] pos = new int[2];
        pos[0] = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
        OrientationHelper orientationHelper = OrientationHelper.createOrientationHelper(linearLayoutManager, OrientationHelper.VERTICAL);
        int fromIndex = 0;
        int toIndex = menuBeanList.size();
        final int start = orientationHelper.getStartAfterPadding();
        final int end = orientationHelper.getEndAfterPadding();
        final int next = toIndex > fromIndex ? 1 : -1;
        for (int i = fromIndex; i != toIndex; i += next) {
            final View child = rcOneMenu.getChildAt(i);
            final int childStart = orientationHelper.getDecoratedStart(child);
            final int childEnd = orientationHelper.getDecoratedEnd(child);
            if (childStart < end && childEnd > start) {
                if (childStart >= start && childEnd <= end) {
                    pos[1] = childStart;
                    return pos;
                }
            }
        }
        return pos;
    }

    /**
     * 滑动到指定位置
     *
     * @param mRecyclerView
     * @param position
     */
    public void smoothMoveToPosition(RecyclerView mRecyclerView, int position) {
        LinearLayoutManager layoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        int f = layoutManager.findFirstVisibleItemPosition();
        int l = layoutManager.findLastVisibleItemPosition();
        if (position <= f) {
            layoutManager.scrollToPosition(position);
        } else if (position <= l) {
            int i = position - f;
            if (0 <= i && i <= layoutManager.getChildCount()) {
                int top = layoutManager.getChildAt(i).getTop();
                mRecyclerView.smoothScrollBy(0, top);
            }
        } else {
            moveToTop = true;
            layoutManager.scrollToPosition(position);
        }
    }

    private BaseRecycleViewAdapter.OnItemClickListener itemClickListener = new BaseRecycleViewAdapter.OnItemClickListener() {
        @Override
        public <T> void onItemClick(View view, T t) {
            if (rcCommodityList.getScrollState() != RecyclerView.SCROLL_STATE_IDLE) return;
            MenuBean item = (MenuBean) t;
            setMenuList(item.id);
            fromClick = true;
            if (commodityList == null) {
                return;
            }
            for (int i = 0; i < commodityList.size(); i++) {
                if (commodityList.get(i) instanceof CommodityDataBean) {
                    CommodityDataBean commodityListBean = (CommodityDataBean) commodityList.get(i);
                    if (TextUtils.equals(item.id, commodityListBean.menuId)) {
                        smoothMoveToPosition(rcCommodityList, i);
                        index = i;
                        break;
                    }
                }
            }
        }
    };

    private RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {

        @Override
        public void onScrolled(@NotNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            LinearLayoutManager layoutManager = (LinearLayoutManager) rcCommodityList.getLayoutManager();
            if (moveToTop) {
                moveToTop = false;
                int a = index - layoutManager.findFirstVisibleItemPosition();
                if (a >= 0 && a <= layoutManager.getChildCount()) {
                    int top = layoutManager.getChildAt(a).getTop();
                    rcCommodityList.smoothScrollBy(0, top);
                }
            } else {
                int itemPosition = layoutManager.findFirstVisibleItemPosition();
                McTemporary mcTemporary = commodityList.get(itemPosition);
                String menuName = getMenuName(mcTemporary);
                menuLayoutListener.setCommodityTitle(menuName);
            }
        }
    };

    private View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            fromClick = false;
            return false;
        }
    };

    private String getMenuName(McTemporary mcTemporary) {
        String menuName = "";
        if (mcTemporary instanceof CommodityDataBean) {
            CommodityDataBean listBean = (CommodityDataBean) mcTemporary;
            menuName = "— " + listBean.cTitle + " —";
            if (!fromClick) {
                int i = setMenuList(listBean.menuId);
                moveTopPosition(rcOneMenu, i);
            }
        } else if (mcTemporary instanceof ProposalsBean) {
            ProposalsBean commodityBean = (ProposalsBean) mcTemporary;
            Iterator<MenuBean> iterator = menuBeanList.iterator();
            while (iterator.hasNext()) {
                MenuBean menuBean = iterator.next();
                if (TextUtils.equals(menuBean.id, commodityBean.menuId)) {
                    menuName = "— " + menuBean.name + " —";
                    if (!fromClick) {
                        int i = setMenuList(commodityBean.menuId);
                        moveTopPosition(rcOneMenu, i);
                    }
                }
            }
        }
        return menuName;
    }

    private int setMenuList(String menuId) {
        int index = 0;
        for (int i = 0; i < menuBeanList.size(); i++) {
            MenuBean menuBean = menuBeanList.get(i);
            if (TextUtils.equals(menuId, menuBean.id)) {
                menuBean.menuSelect = true;
                index = i;
            } else {
                menuBean.menuSelect = false;
            }
        }
        menuAdapter.notifyDataSetChanged();
        return index;
    }

    private void moveTopPosition(RecyclerView recyclerView, int i) {
        LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        int f = layoutManager.findFirstVisibleItemPosition();
        int l = layoutManager.findLastVisibleItemPosition();
        if (i <= f || i >= l) {
            layoutManager.scrollToPosition(i);
        }
    }

    public interface MenuLayoutListener {

        void setCommodityTitle(String menuName);

    }
}
