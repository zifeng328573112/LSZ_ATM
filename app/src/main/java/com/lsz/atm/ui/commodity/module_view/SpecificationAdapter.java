package com.lsz.atm.ui.commodity.module_view;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.common.labels.LabelsView;
import com.common.labels.SelectType;
import com.common.labels.inteface.LabelTextProvider;
import com.common.labels.inteface.OnLabelClickListener;
import com.common.labels.inteface.OnLabelSelectChangeListener;
import com.lsz.atm.R;
import com.lsz.atm.json.commodity.ChoicesBean;
import com.lsz.atm.json.commodity.OptionsBean;
import com.lsz.atm.viewutil.BigDecimalUtils;
import com.util_code.widget.recycleadpter.BaseRecycleViewAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SpecificationAdapter extends BaseRecycleViewAdapter<ChoicesBean> {

    private SpecificationListener listener;
    private List<OptionsBean> optionsList;

    SpecificationAdapter(Context context) {
        super(context);
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, ChoicesBean item) {
        if (holder instanceof SpecificationHolder) {
            SpecificationHolder specificationHolder = (SpecificationHolder) holder;
            specificationHolder.tvItemOption.setText(item.name);
            setSpecificationData(specificationHolder.lvItemLabel, item);
        }
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_specifications_view, parent, false);
        return new SpecificationHolder(view);
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    void setSpecificationListener(SpecificationListener listener) {
        this.listener = listener;
    }

    private void setSpecificationData(LabelsView labelsView, ChoicesBean choiceBean) {
        labelsView.setOnLabelClickListener(null);
        labelsView.clearCompulsorys();
        if (choiceBean.required) {
            labelsView.setMaxSelect(0);
            labelsView.setMinSelect(1);
        } else {
            labelsView.setMaxSelect(0);
            labelsView.setMinSelect(0);
        }
        if (choiceBean.multiple) {
            labelsView.setSelectType(SelectType.MULTI);
        } else {
            labelsView.setSelectType(SelectType.SINGLE_IRREVOCABLY);
        }
        labelsView.setOnLabelSelectChangeListener(onLabelSelectChangeListener);
        labelsView.setOnLabelClickListener(onLabelClickListener);
        labelsView.setLabels(choiceBean.options, labelTextProvider);
        optionsList = labelsView.getLabels();
    }

    private OnLabelSelectChangeListener onLabelSelectChangeListener = new OnLabelSelectChangeListener() {
        @Override
        public void onLabelSelectChange(LabelsView labelsView, TextView label, Object data, boolean isSelect, int position) {
            OptionsBean item = (OptionsBean) data;
//            boolean isSelected = false;
//            for (ChoicesBean choicesBean : getData()) {
//                for (OptionsBean optionsBean : choicesBean.options) {
//                    if (TextUtils.equals(optionsBean.id, item.id)) {
//                        isSelected = optionsBean.selected;
//                        Log.d("ABC", optionsBean.selected + "---optionsBean.selected");
//                    }
//                }
//            }
//            Log.d("ABC", item.toString() + "---item");
            listener.labelSelect(item, false);
//            Log.d("ABC", isSelected + "---isSelected");
//            if (isSelected) {
//                labelsView.setLabelBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.btn_label_yellow), position);
//            } else {
//                labelsView.setLabelBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.btn_label_white), position);
//            }
        }
    };

    private OnLabelClickListener onLabelClickListener = new OnLabelClickListener() {
        @Override
        public void onLabelClick(TextView label, Object data, int position) {
            OptionsBean item = (OptionsBean) data;
            isLabelSelect(item, label);
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    notifyDataSetChanged();
                }
            });
        }
    };

    private LabelTextProvider labelTextProvider = new LabelTextProvider<OptionsBean>() {
        @Override
        public CharSequence getLabelText(TextView label, int position, OptionsBean item) {
            return setLabelOption(item);
        }

        @Override
        public int setLabelSelect(int position, OptionsBean item) {
            if (item.selected) {
                return position;
            } else {
                return -1;
            }
        }

        @Override
        public boolean labelCreateClick(LabelsView labelsView, TextView label, int position, OptionsBean item) {
            boolean labelSelect = getLabelSelect(item);
            if (labelSelect) {
                if (item.selected) {
                    labelsView.setLabelBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.btn_label_yellow), position);
                } else {
                    labelsView.setLabelBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.btn_label_white), position);
                }
            } else {
                labelsView.setLabelBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.btn_label_gray), position);
            }
            return labelSelect;
        }
    };

    private synchronized String setLabelOption(OptionsBean item) {
        String round = BigDecimalUtils.round(item.priceSell, 2);
        String currency = mContext.getResources().getString(R.string.currency);
        return item.showThis && !TextUtils.equals(round, "0") ? item.name + " " + currency + round : item.name;
    }

    private synchronized boolean getLabelSelect(OptionsBean item) {
        boolean isClick = true;
        for (ChoicesBean choices : getData()) {
            for (OptionsBean options : choices.options) {
                if (options.selected && options.awayOptions != null && options.awayOptions.size() > 0) {
                    for (String optionsId : options.awayOptions) {
                        if (TextUtils.equals(item.id, optionsId)) {
                            isClick = false;
                            break;
                        }
                    }
                }
            }
        }
        return isClick;
    }

    private void isLabelSelect(OptionsBean item, TextView label) {
        for (ChoicesBean choices : getData()) {
            for (OptionsBean options : choices.options) {
                if (choices.multiple) {
                    if (TextUtils.equals(item.id, options.id)) {
                        if (item.selected) {
                            item.selected = false;
                        } else {
                            item.selected = true;
                        }
                        options.selected = item.selected;
                        label.setClickable(options.selected);
                    }
                } else if (TextUtils.equals(item.choiceId, options.choiceId)) {
                    options.selected = TextUtils.equals(item.id, options.id);
                }
            }
        }
    }

    class SpecificationHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_item_option)
        TextView tvItemOption;
        @BindView(R.id.lv_item_label)
        LabelsView lvItemLabel;

        SpecificationHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface SpecificationListener {

        void labelSelect(OptionsBean item, boolean isSelect);

    }

}
