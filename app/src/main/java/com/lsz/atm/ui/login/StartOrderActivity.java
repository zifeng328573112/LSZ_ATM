package com.lsz.atm.ui.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.common.config.Global;
import com.common.download.CheckVersionUpdate;
import com.common.download.VersionUpdatePresent;
import com.common.mvp.BaseMvpActivity;
import com.common.thirdparty.ThirdParty;
import com.common.thirdparty.aiyin.AiYinParty;
import com.lsz.atm.R;
import com.lsz.atm.db.dao_utils.DaoUtils;
import com.lsz.atm.event.StartOrderEvent;
import com.lsz.atm.json.ATMBean;
import com.lsz.atm.json.StartOrderBean;
import com.lsz.atm.ui.login.module_view.SelectMealsView;
import com.lsz.atm.ui.login.module_view.StartOrderView;
import com.lsz.atm.ui.menu.MenuListActivity;
import com.util_code.utils.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;

public class StartOrderActivity extends BaseMvpActivity<LoginView, LoginPresent> implements LoginView, View.OnClickListener {

    @BindView(R.id.ll_start_order_view)
    RelativeLayout llStartOrderView;

    private StartOrderView<StartOrderActivity> activityStartOrderView;
    private SelectMealsView<StartOrderActivity> selectMealsView;

    private StartOrderBean startOrderBean;
    private CheckVersionUpdate checkVersionUpdate;

    public static void startActivity(Activity activity) {
        Intent intent = new Intent(activity, StartOrderActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_start_order;
    }

    @Override
    protected void setTranslateStatusBar(boolean barColor) {
        super.setTranslateStatusBar(false);
    }

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void setupView() {
        activityStartOrderView = new StartOrderView<>(this, startOrderClickListner);
        llStartOrderView.addView(activityStartOrderView.getView());

        selectMealsView = new SelectMealsView<>(this, mealsClickListener);

        checkVersionUpdate = new CheckVersionUpdate(this, checkVersionResult);
        checkVersionUpdate.onCheckUpdate();
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        getPresenter().getHomePage();
    }

    private VersionUpdatePresent.CheckVersionResult checkVersionResult = new VersionUpdatePresent.CheckVersionResult() {
        @Override
        public void versionCheckError() {

        }

        @Override
        public void versionNoNeedDowload() {

        }

        @Override
        public void versionDowloadError() {

        }

        @Override
        public void versionDowloadSuccess() {

        }
    };

    private StartOrderView.StartOrderClickListner startOrderClickListner = new StartOrderView.StartOrderClickListner() {
        @Override
        public void onStartOrderCLick() {
            llStartOrderView.addView(selectMealsView.getView());
        }
    };

    private SelectMealsView.SelectMealsClickListener mealsClickListener = new SelectMealsView.SelectMealsClickListener() {

        @Override
        public void onTakeAwayClick() {
            MenuListActivity.startActivity(StartOrderActivity.this);
        }

        @Override
        public void onDineInClick() {
            MenuListActivity.startActivity(StartOrderActivity.this);
        }
    };

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(StartOrderEvent event) {
        llStartOrderView.removeAllViews();
        llStartOrderView.addView(activityStartOrderView.getView());
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (activityStartOrderView != null && startOrderBean != null && startOrderBean.advertising != null && startOrderBean.advertising.size() > 0) {
            activityStartOrderView.setImageResources(startOrderBean.advertising.get(0));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DaoUtils.getCommodityInstance().closeDB();
        DaoUtils.getPackageInstance().closeDB();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onHomeBanner(StartOrderBean startOrderBean) {
        if (startOrderBean == null) {
            return;
        }
        this.startOrderBean = startOrderBean;
        ATMBean atmBean = new ATMBean();
        atmBean.storeName = startOrderBean.storeName;
        Global.setAtmBean(atmBean);
        if (activityStartOrderView != null && startOrderBean.advertising != null && startOrderBean.advertising.size() > 0) {
            activityStartOrderView.setImageResources(startOrderBean.advertising.get(0));
        }
    }

    @Override
    public <T> void onSuccess(T t) {

    }

    @Override
    public void onFail() {

    }
}
