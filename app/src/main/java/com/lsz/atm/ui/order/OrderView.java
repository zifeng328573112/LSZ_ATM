package com.lsz.atm.ui.order;

import com.lsz.atm.json.OrderBean;
import com.lsz.atm.json.VIPMemberBean;
import com.util_code.base.mvp.MvpView;

public interface OrderView extends MvpView {

    void onOrderSuccess(OrderBean orderBean);

    void onVIPMember(VIPMemberBean vipMemberBean);

    void onDisountStart();

    void onDiscountAmount(String discountAmount, String couponPrice);

    void onFail();

}
