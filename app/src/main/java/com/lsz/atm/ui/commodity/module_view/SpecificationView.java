package com.lsz.atm.ui.commodity.module_view;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lsz.atm.R;
import com.lsz.atm.json.commodity.ChoicesBean;
import com.lsz.atm.json.commodity.OptionsBean;

import java.util.ArrayList;
import java.util.List;

public class SpecificationView<T extends Activity> {

    private Activity activity;
    private RecyclerView rvSpecification;
    private TextView tvSelectAlready;
    private TextView tvSelectSpecification;
    private SpecificationAdapter specificationAdapter;
    private List<OptionsBean> optionsList = new ArrayList<>();

    public SpecificationView(T activity) {
        this.activity = activity;
    }

    public View getView() {
        View view = View.inflate(activity, R.layout.layout_specifications, null);
        tvSelectSpecification = view.findViewById(R.id.tv_select_specification);
        tvSelectAlready = view.findViewById(R.id.tv_select_already);
        rvSpecification = view.findViewById(R.id.rv_specification);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
        rvSpecification.setLayoutManager(linearLayoutManager);
        specificationAdapter = new SpecificationAdapter(activity);
        rvSpecification.setAdapter(specificationAdapter);
        return view;
    }

    public void setData(List<ChoicesBean> choicesList) {
        String orderSelectOk = activity.getResources().getString(R.string.order_select_ok);
        specificationAdapter.resetData(choicesList);
        specificationAdapter.setSpecificationListener(new SpecificationAdapter.SpecificationListener() {
            @Override
            public void labelSelect(OptionsBean item, boolean isSelect) {
                StringBuffer stringBuffer = new StringBuffer();
                for (ChoicesBean choices : specificationAdapter.getData()) {
                    for (OptionsBean options : choices.options) {
                        if (options.selected) {
                            if (TextUtils.isEmpty(stringBuffer)) {
                                stringBuffer.append(options.name);
                            } else {
                                stringBuffer.append("/" + options.name);
                            }
                        }
                    }
                }
                tvSelectAlready.setText(orderSelectOk + stringBuffer);
            }
        });
    }
}
