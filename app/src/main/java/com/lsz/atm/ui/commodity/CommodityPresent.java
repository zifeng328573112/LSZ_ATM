package com.lsz.atm.ui.commodity;

import com.common.config.MyApi;
import com.lsz.atm.TemporaryDataUtil;
import com.lsz.atm.json.commodity.BundlesBean;
import com.lsz.atm.json.commodity.CommoditiesBean;
import com.lsz.atm.json.commodity.ProposalsBean;
import com.lsz.atm.viewutil.BigDecimalUtils;
import com.util_code.base.mvp.RxMvpPresenter;

import java.util.List;

import javax.inject.Inject;

public class CommodityPresent extends RxMvpPresenter<CommodityView> {

    protected MyApi mMyApi;

    @Inject
    public CommodityPresent(MyApi myApi) {
        this.mMyApi = myApi;
    }

    /**
     * 商品总价+套餐单品总价
     *
     * @return
     */
    public String totalPrice(ProposalsBean proposalsBean, List<BundlesBean> bundlesList) {
        String commodiTotalPrice = "0";
        if (bundlesList == null || bundlesList.size() == 0) {
            return BigDecimalUtils.round(proposalsBean.totalPriceList, 2);
        }
        for (BundlesBean bundles : bundlesList) {
            for (CommoditiesBean commodities : bundles.getCommodities()) {
                commodiTotalPrice = TemporaryDataUtil.getTotalAmount(commodities.commodiTotalPrice, commodities.commodiTotalPrice, proposalsBean.cSelectNumber);
            }
        }
        commodiTotalPrice = BigDecimalUtils.add(proposalsBean.totalPriceList, commodiTotalPrice, 2);
        return commodiTotalPrice;
    }

}
