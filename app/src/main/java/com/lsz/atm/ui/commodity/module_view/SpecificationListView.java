package com.lsz.atm.ui.commodity.module_view;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.lsz.atm.R;

public class SpecificationListView<T extends Activity> {

    private Activity activity;
    private TextView tvItemOption;

    public SpecificationListView(T activity) {
        this.activity = activity;
    }

    public View getView(){
        View view = View.inflate(activity, R.layout.item_specifications_view, null);
        tvItemOption = view.findViewById(R.id.tv_item_option);
        return view;
    }



}
