package com.lsz.atm.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.util_code.base.BaseApplication;
import com.util_code.dialog.LoadingDialog;
import com.util_code.download.DownloadException;
import com.util_code.download.DownloadManager;
import com.util_code.download.downinterface.DownLoadCallBack;
import com.util_code.oklog.debug.DebugInfoActivity;
import com.util_code.utils.DecimalUtils;
import com.util_code.utils.ToastUtils;
import com.util_code.widget.tintimage.TintImageView;
import com.lsz.atm.R;
import com.lsz.atm.json.CheckUpdate;
import com.common.mvp.BaseMvpActivity;

import java.io.File;

import butterknife.BindView;
import timber.log.Timber;

public class MainActivity extends BaseMvpActivity<MainView, MainPresent> implements MainView, View.OnClickListener {

    @BindView(R.id.tv_net_test_id)
    TextView tv_net_test_id;
    @BindView(R.id.tv_db_test_id)
    TextView tv_db_test_id;
    @BindView(R.id.tv_mc_test_id)
    TextView tv_mc_test_id;
    @BindView(R.id.iv_tint_test_id)
    TintImageView iv_tint_test_id;
    @BindView(R.id.iv_tint_testpng_id)
    TintImageView iv_tint_testpng_id;
    @BindView(R.id.tv_debug_test_id)
    TextView tv_debug_test_id;
    @BindView(R.id.tv_work_test_id)
    TextView tv_work_test_id;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initInject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected void setupView() {
        tv_net_test_id.setOnClickListener(this);
        tv_db_test_id.setOnClickListener(this);
        tv_mc_test_id.setOnClickListener(this);
        iv_tint_test_id.setOnClickListener(this);
        iv_tint_testpng_id.setOnClickListener(this);
        tv_debug_test_id.setOnClickListener(this);
        tv_work_test_id.setOnClickListener(this);
    }

    @Override
    protected void setupData(Bundle savedInstanceState) {

//        GlideApp.with(this)
//                .asBitmap()
//                .load("http://f.hiphotos.baidu.com/image/pic/item/08f790529822720eb1faa1be77cb0a46f21fabba.jpg")
//                .into(iv_tint_test_id);

    }

    @Override
    public void OnVersionUpdate(CheckUpdate result) {
        loadingDialog.dismissAllowingStateLoss();
    }

    @Override
    public void OnVersionUpdateError() {
        loadingDialog.dismissAllowingStateLoss();
    }

    LoadingDialog loadingDialog;

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tv_net_test_id:
                loadingDialog = new LoadingDialog();
                loadingDialog.show(getSupportFragmentManager(), "loadingdialog");
                getPresenter().requestVersion();
                break;

            case R.id.tv_db_test_id:
                DownloadManager.getInstance()
                        .download("http://dl.m.cc.youku.com/android/phone/Youku_Phone_youkuweb.apk", BaseApplication
                                .getAppComponent()
                                .getCacheDir(), "Youku_Phone_youkuweb.apk", false, new DownLoadCallBack() {

                            @Override
                            public void onConnected(long total, boolean isRangeSupport) {

                            }

                            @Override
                            public void onProgress(long finished, long total, int progress) {

                            }

                            @Override
                            public void onCompleted(File downloadfile) {

                            }

                            @Override
                            public void onFailed(DownloadException e) {

                            }

                            @Override
                            public void onPaused(File downloadfile) {

                            }
                        });

                break;

            case R.id.tv_mc_test_id:
                Timber.e("111 = " + DecimalUtils.formatDecimalWithZero(123456.789, 2));
                Timber.e("111 = " + DecimalUtils.formatDecimal(123456.789, 2));
                Timber.e("111 = " + DecimalUtils.formatThousandthDecimal(123456.789, 2));

                Timber.e("222 = " + DecimalUtils.formatDecimalWithZero(0.123, 2));
                Timber.e("222 = " + DecimalUtils.formatDecimal(0.123, 2));
                Timber.e("222 = " + DecimalUtils.formatThousandthDecimal(1254354125.123, 2));
                BaseApplication.restartApp(MainActivity.this);
                break;

            case R.id.iv_tint_test_id:
            case R.id.iv_tint_testpng_id:
                ToastUtils.showShortToast("tint image test");
                break;

            case R.id.tv_debug_test_id:
                startActivity(new Intent(MainActivity.this, DebugInfoActivity.class));
                break;

            case R.id.tv_work_test_id:
                break;

            default:
                break;
        }
    }
}
