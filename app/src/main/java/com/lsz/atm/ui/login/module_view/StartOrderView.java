package com.lsz.atm.ui.login.module_view;

import android.app.Activity;
import android.app.ActivityManager;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.request.RequestOptions;
import com.common.UIApplication;
import com.common.config.Global;
import com.common.util.LongClickUtils;
import com.lsz.atm.R;
import com.lsz.atm.dialog.BottomDialog;
import com.lsz.atm.ui.login.LoginActivity;
import com.lsz.atm.viewutil.TVUtil;
import com.util_code.dagger.help.GlideApp;
import com.util_code.utils.ToastUtils;

public class StartOrderView<T extends Activity> implements View.OnClickListener {

    ImageView ivStartOrderBg;
    RelativeLayout rlStartOrder;
    TextView tvBtnText;
    TextView tvBtnEnglishText;
    ImageView ivQrCode;
    ImageView ivLogo;
    TextView tvHintMessage;

    private BottomDialog bottomDialog;

    Activity activity;
    StartOrderClickListner startOrderClickListner;

    public StartOrderView(T activity, StartOrderClickListner startOrderClickListner) {
        this.activity = activity;
        this.startOrderClickListner = startOrderClickListner;
    }

    public View getView() {
        View view = View.inflate(activity, R.layout.layout_start_order, null);
        ivStartOrderBg = view.findViewById(R.id.iv_start_order_bg);
        rlStartOrder = view.findViewById(R.id.rl_start_order);
        tvBtnText = view.findViewById(R.id.tv_btn_text);
        tvBtnEnglishText = view.findViewById(R.id.tv_btn_english_text);
        ivQrCode = view.findViewById(R.id.iv_qr_code);
        ivLogo = view.findViewById(R.id.iv_logo);
        tvHintMessage = view.findViewById(R.id.tv_hint_message);
        rlStartOrder.setOnClickListener(this);
        setTextColor();
        LongClickUtils.setLongClick(new Handler(), ivStartOrderBg, 5000, new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View view) {
                BottomDialog.Builder builder = new BottomDialog.Builder(activity);
                bottomDialog = builder.setBottomClickListener(new BottomDialog.BottomClickListener() {
                    @Override
                    public void viewClick(boolean clickExit) {
                        bottomDialog.dismiss();
                        if (clickExit) {
                            LoginActivity.startActivity(activity);
                            Global.loginOutclear();
                        } else {
                            Global.loginOutclear();
                            UIApplication.getAppManager().finishAllActivity();
                        }
                    }
                }).create();
                bottomDialog.show();
                return true;
            }
        });
        return view;
    }

    public void setTextColor() {
        String string = activity.getResources().getString(R.string.lsz_hint_message);
        int color = activity.getResources().getColor(R.color.c_FABE00);
        TVUtil.setTVColor(string, 9, 15, color, tvHintMessage);
        setImageResources(null);
    }

    public void setImageResources(String img) {
        Drawable drawable = activity.getResources().getDrawable(R.drawable.ad_banner);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(drawable);
        requestOptions.error(drawable);
        if (!TextUtils.isEmpty(img)) {
            GlideApp.with(activity).load(img).apply(requestOptions).into(ivStartOrderBg);
        } else {
            GlideApp.with(activity).load(drawable).apply(requestOptions).into(ivStartOrderBg);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_start_order:
                startOrderClickListner.onStartOrderCLick();
                break;

            default:
                break;
        }
    }

    public interface StartOrderClickListner {

        void onStartOrderCLick();

    }
}
