package com.lsz.atm.ui.menu;

import androidx.annotation.NonNull;

import com.common.config.Global;
import com.common.config.MyApi;
import com.common.retrofit.RxObserver;
import com.common.retrofit.json.Data;
import com.facebook.stetho.common.LogUtil;
import com.lsz.atm.json.MenuBean;
import com.lsz.atm.json.commodity.CommodityDataBean;
import com.util_code.base.mvp.RxMvpPresenter;
import com.util_code.utils.RxUtils;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

import static com.common.retrofit.RxObserver.checkJsonCode;

public class MenuPresent extends RxMvpPresenter<MenuView> {

    private MyApi mMyApi;

    @Inject
    public MenuPresent(MyApi myApi) {
        this.mMyApi = myApi;
    }

    void getMenuDataList(boolean isShowLoading) {
        mMyApi.getMenuList()
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(Disposable disposable) throws Exception {
                        if (isShowLoading) {
                            getView().onShowLoading();
                        }
                    }
                })
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(new Consumer<Data<List<MenuBean>>>() {
                    @Override
                    public void accept(Data<List<MenuBean>> listData) throws Exception {
                        if (checkJsonCode(listData, false) && listData.data != null) {
                            Global.setMenuList(listData.data);
                            getView().getMenuListSuccess(listData.data);
                        }
                    }
                })
                .observeOn(Schedulers.newThread())
                .flatMap(new Function<Data<List<MenuBean>>, ObservableSource<Data<List<CommodityDataBean>>>>() {
                    @Override
                    public ObservableSource<Data<List<CommodityDataBean>>> apply(Data<List<MenuBean>> listData) throws Exception {
                        return mMyApi.getCommodityList();
                    }
                })
                .compose(RxUtils.<Data<List<CommodityDataBean>>>applySchedulersLifeCycle(getView()))
                .doFinally(new Action() {
                    @Override
                    public void run() throws Exception {
                        if (isShowLoading) {
                            getView().onCloseLoading();
                        }
                    }
                })
                .subscribe(new RxObserver<Data<List<CommodityDataBean>>>() {

                    @Override
                    public void onNext(@NonNull Data<List<CommodityDataBean>> data) {
                        if (checkJsonCode(data, false) && data.data != null) {
                            Global.setCommodityList(data.data);
                            getView().getCommodityListSuccess(data.data);
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        super.onError(e);
                        if (isShowLoading) {
                            getView().onFail(1);
                        } else {
                            getView().onFail(2);
                        }
                    }
                });

    }

    public void getMenuList() {
        mMyApi.getMenuList()
                .compose(RxUtils.<Data<List<MenuBean>>>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data<List<MenuBean>>>() {

                    @Override
                    public void onNext(@NonNull Data<List<MenuBean>> data) {
                        LogUtil.i(data.toString());
                        if (checkJsonCode(data, false)) {
                            getView().getMenuListSuccess(data.data);
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail(1);
                    }
                });
    }

    public void getCommodityList() {
        mMyApi.getCommodityList()
                .compose(RxUtils.<Data<List<CommodityDataBean>>>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data<List<CommodityDataBean>>>() {

                    @Override
                    public void onNext(@NonNull Data<List<CommodityDataBean>> data) {
                        LogUtil.i(data.toString());
                        if (checkJsonCode(data, false) && checkJsonCode(data.data, false)) {
                            getView().getCommodityListSuccess(data.data);
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail(1);
                    }
                });
    }

//    public void getProposal() {
//        mMyApi.getProposal(0)
//                .compose(RxUtils.<Data<ProposalItemRes>>applySchedulersLifeCycle(getView()))
//                .subscribe(new RxObserver<Data<ProposalItemRes>>() {
//
//                    @Override
//                    public void onNext(@NonNull Data<ProposalItemRes> data) {
//                        LogUtil.i(data.toString());
////                        if (checkJsonCode(data, false)) {
////                            getView().getCommodityListSuccess(data.data);
////                        }
//                    }
//
//                    @Override
//                    public void onError(@NonNull Throwable e) {
////                        getView().onFail(1);
//                    }
//                });
//    }

}
