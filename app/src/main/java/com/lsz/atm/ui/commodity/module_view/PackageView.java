package com.lsz.atm.ui.commodity.module_view;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lsz.atm.R;
import com.lsz.atm.json.commodity.BundlesBean;
import com.lsz.atm.json.commodity.BundlesTemporary;
import com.lsz.atm.json.commodity.CommoditiesBean;
import com.lsz.atm.ui.commodity.CommodityView;
import com.lsz.atm.viewutil.BigDecimalUtils;
import com.lsz.atm.viewutil.SpannableUtil;

import java.util.ArrayList;
import java.util.List;

public class PackageView<T extends Activity> {

    private Activity activity;
    private GridLayoutManager gridLayoutManager;
    private PackageAdapter packageAdapter;

    private List<BundlesTemporary> bundlesTemporaryList;
    private CommodityView.PackageListener packageListener;

    public PackageView(T activity) {
        this.activity = activity;
    }

    public View getView() {
        View view = View.inflate(activity, R.layout.layout_list_package, null);
        RecyclerView rvPackageList = view.findViewById(R.id.rv_package_list);

        gridLayoutManager = new GridLayoutManager(activity, 3);
        rvPackageList.setLayoutManager(gridLayoutManager);
        packageAdapter = new PackageAdapter(activity, onPackageItemListener);
        rvPackageList.setAdapter(packageAdapter);
        rvPackageList.setHasFixedSize(true);
        rvPackageList.setNestedScrollingEnabled(false);
        return view;
    }

    public void setOnPackageItemListener(CommodityView.PackageListener packageListener) {
        this.packageListener = packageListener;
    }

    public void setRvPackageList(List<BundlesBean> bundlesBeanList) {
        if (bundlesBeanList == null || bundlesBeanList.size() == 0) return;
        bundlesTemporaryList = getBundlesTemporaryList(bundlesBeanList);
        packageAdapter.resetData(bundlesTemporaryList);
        gridLayoutManager.setSpanSizeLookup(spanSizeLookup);
    }

    private PackageAdapter.OnPackageItemListener onPackageItemListener = new PackageAdapter.OnPackageItemListener() {
        @Override
        public void onItemAdd(CommoditiesBean commoditiesBean) {
            addAndDelete(commoditiesBean, true);
        }

        @Override
        public void onItemDelete(CommoditiesBean commoditiesBean) {
            addAndDelete(commoditiesBean, false);
        }
    };

    private void addAndDelete(CommoditiesBean commoditiesBean, boolean isAdd) {
        int buyNum = 0;
        int buymin = 0;
        for (int i = 0; i < bundlesTemporaryList.size(); i++) {
            BundlesTemporary temporary = bundlesTemporaryList.get(i);
            if (temporary instanceof CommoditiesBean) {
                CommoditiesBean bean = (CommoditiesBean) temporary;
                if (TextUtils.equals(commoditiesBean.proComId, bean.proComId)) {
                    if (isAdd) {
                        bean.buyMin = (Integer.parseInt(bean.buyMin) + 1) + "";
                        if (Integer.parseInt(bean.buyMin) == 1) {
                            bean.commodiTotalPrice = BigDecimalUtils.round(bean.priceSell, 2);
                        } else {
                            bean.commodiTotalPrice = BigDecimalUtils.add(bean.commodiTotalPrice, bean.priceSell, 2);
                        }
                        packageListener.onCommoditiesAdd(bean);
                    } else {
                        bean.buyMin = (Integer.parseInt(bean.buyMin) - 1) + "";
                        if (Integer.parseInt(bean.buyMin) == 0) {
                            bean.commodiTotalPrice = "0";
                        } else {
                            bean.commodiTotalPrice = BigDecimalUtils.sub(bean.commodiTotalPrice, bean.priceSell, 2);
                        }
                        packageListener.onCommoditiesDelete(bean);
                    }
                    packageAdapter.notifyItemChanged(i, bean);
                }
                if (TextUtils.equals(commoditiesBean.bundleId, bean.bundleId)) {
                    buymin = buymin + Integer.parseInt(bean.buyMin);
                }
            }
            if (temporary instanceof BundlesBean) {
                BundlesBean bundlesBean = (BundlesBean) temporary;
                if (TextUtils.equals(commoditiesBean.bundleId, bundlesBean.id)) {
                    buyNum = Integer.parseInt(bundlesBean.buyNum);
                }
            }
        }
        for (int j = 0; j < bundlesTemporaryList.size(); j++) {
            BundlesTemporary bundlesTemporary = bundlesTemporaryList.get(j);
            if (bundlesTemporary instanceof CommoditiesBean) {
                CommoditiesBean commodities = (CommoditiesBean) bundlesTemporary;
                if (TextUtils.equals(commodities.bundleId, commoditiesBean.bundleId)) {
                    commodities.addSelect = (Integer.parseInt(commodities.buyMin) == Integer.parseInt(commodities.buyMax)) ? false : buyNum == buymin ? false : true;
                    packageAdapter.notifyItemChanged(j, commodities);
                }
            }
            if (bundlesTemporary instanceof BundlesBean) {
                BundlesBean bundlesBean = (BundlesBean) bundlesTemporary;
                if (TextUtils.equals(commoditiesBean.bundleId, bundlesBean.id)) {
                    buyNum = Integer.parseInt(bundlesBean.buyNum);
                    if (bundlesBean.indexOf != -1 && buymin > 0) {
                        String strName = bundlesBean.name;
                        int indexOf = bundlesBean.name.indexOf("/");
                        if (indexOf != -1) {
                            String substring = bundlesBean.name.substring(bundlesBean.indexOf, indexOf + 1);
                            strName = bundlesBean.name.replace(substring, "");
                        }
                        bundlesBean.name = SpannableUtil.addSpace(strName, buymin + "/", bundlesBean.indexOf - 1);
                    } else {
                        String strName = bundlesBean.name;
                        int indexOf = bundlesBean.name.indexOf("/");
                        if (indexOf != -1) {
                            String substring = bundlesBean.name.substring(bundlesBean.indexOf, indexOf + 1);
                            strName = bundlesBean.name.replace(substring, "");
                        }
                        bundlesBean.name = strName;
                    }
                    packageAdapter.notifyItemChanged(j, bundlesBean);
                }
            }
        }
    }

    private List<BundlesTemporary> getBundlesTemporaryList(List<BundlesBean> bundlesBeanList) {
        List<BundlesTemporary> arrayList = new ArrayList<>();
        for (BundlesBean bundlesBean : bundlesBeanList) {
            BundlesBean bean = new BundlesBean();
            bean.id = bundlesBean.id;
            bean.buyNum = bundlesBean.buyNum;
            if (Integer.parseInt(bundlesBean.buyNum) > 0) {
                String hintText = "请选择" + bundlesBean.name + "&" + bundlesBean.buyNum + "份";
                bean.indexOf = hintText.indexOf("&");
                bean.name = hintText.replace("&", "");
            } else {
                bean.name = bundlesBean.name;
                bean.indexOf = -1;
            }
            bean.buyNum = bundlesBean.buyNum;
            bean.ranking = bundlesBean.ranking;
            arrayList.add(bean);
            int buymin = 0;
            for (CommoditiesBean commoditiesBean : bundlesBean.commodities) {
                buymin = buymin + Integer.parseInt(commoditiesBean.buyMin);
            }
            for (CommoditiesBean commoditiesBean : bundlesBean.commodities) {
                commoditiesBean.isRequired = TextUtils.equals("0", bean.id);
                commoditiesBean.addSelect = Integer.parseInt(bean.buyNum) != buymin;
                arrayList.add(commoditiesBean);
            }
        }
        return arrayList;
    }

    private GridLayoutManager.SpanSizeLookup spanSizeLookup = new GridLayoutManager.SpanSizeLookup() {
        @Override
        public int getSpanSize(int position) {
            return setSpanSize(position, bundlesTemporaryList);
        }
    };

    private int setSpanSize(int position, List<BundlesTemporary> listEntities) {
        int getmType = listEntities.get(position).getmType();
        int count;
        if (getmType == 0 || getmType == 1) {
            count = 3;
        } else {
            count = 1;
        }

        return count;
    }

}
