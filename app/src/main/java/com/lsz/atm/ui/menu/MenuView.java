package com.lsz.atm.ui.menu;

import com.lsz.atm.json.MenuBean;
import com.lsz.atm.json.commodity.CommodityDataBean;
import com.util_code.base.mvp.MvpView;

import java.util.List;

public interface MenuView extends MvpView {

    void getMenuListSuccess(List<MenuBean> menuBeanList);

    void getCommodityListSuccess(List<CommodityDataBean> commodityBeanList);

    void onShowLoading();

    void onCloseLoading();

    void onFail(int number);
}
