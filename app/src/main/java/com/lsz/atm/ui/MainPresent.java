package com.lsz.atm.ui;

import androidx.annotation.NonNull;

import com.common.UIApplication;
import com.common.config.Constant;
import com.common.config.MyApi;
import com.common.retrofit.RxObserver;
import com.common.retrofit.json.Data;
import com.lsz.atm.json.CheckUpdate;
import com.util_code.base.mvp.RxMvpPresenter;
import com.util_code.utils.RxUtils;

import java.util.HashMap;

import javax.inject.Inject;

import timber.log.Timber;

public class MainPresent extends RxMvpPresenter<MainView> {

    MyApi mMyApi;

    @Inject
    public MainPresent(MyApi myApi) {
        mMyApi = myApi;
        Timber.e("===========================================================");
        Timber.e("okhttpHelper = " + UIApplication.getAppComponent().okhttpHelper());
        Timber.e("cacheDir = " + UIApplication.getAppComponent().getCacheDir());
        Timber.e("glideCacheDir = " + UIApplication.getAppComponent().getGlideCacheDir());
        Timber.e("okhttpCacheDir = " + UIApplication.getAppComponent()
                .getOkhttpCacheDir());
    }

    public void requestVersion() {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("classcode", Constant.ANDROIDVERSION);

        mMyApi.checkUpdate(hashMap)
                .compose(RxUtils.<Data<CheckUpdate>>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data<CheckUpdate>>() {

                    @Override
                    public void onNext(@NonNull Data<CheckUpdate> checkUpdateData) {
                        if (checkJsonCode(checkUpdateData, false)) {
                            getView().OnVersionUpdate(checkUpdateData.data);
                        } else {
                            getView().OnVersionUpdateError();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().OnVersionUpdateError();
                    }

                });
    }

}
