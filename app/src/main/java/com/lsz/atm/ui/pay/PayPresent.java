package com.lsz.atm.ui.pay;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.alibaba.fastjson.JSONObject;
import com.common.config.Constant;
import com.common.config.MyApi;
import com.common.retrofit.RxObserver;
import com.common.retrofit.json.Data;
import com.common.util.ASCIIUtil;
import com.common.util.ObjectUtil;
import com.facebook.stetho.common.LogUtil;
import com.lsz.atm.TemporaryDataUtil;
import com.lsz.atm.json.OrderPayBean;
import com.lsz.atm.json.PayRequestBean;
import com.util_code.base.mvp.RxMvpPresenter;
import com.util_code.utils.RxUtils;

import java.util.Map;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class PayPresent extends RxMvpPresenter<PayView> {

    protected MyApi myApi;

    @Inject
    public PayPresent(MyApi myApi) {
        this.myApi = myApi;
    }

    void payOrderSwipe(PayRequestBean payRequest) {
        myApi.payOrderSwipe(payRequest)
                .compose(RxUtils.<Data<OrderPayBean>>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data<OrderPayBean>>() {

                    @Override
                    public void onNext(@NonNull Data<OrderPayBean> data) {
                        LogUtil.d(data.toString());
                        if (!TextUtils.isEmpty(data.sub_code) && TextUtils.equals(data.sub_code, "ORDER_PAY_ERROR")) {
                            getView().onPayError(data.result_message);
                        } else if (checkJsonCode(data, true)) {
                            getView().onSuccess(data.data);
                        } else {
                            getView().onFail();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        LogUtil.d(e.toString());
                        getView().onFail();
                    }
                });
    }

    void payOrderQuery(PayRequestBean payRequest) {
        myApi.payOrderQuery(payRequest)
                .compose(RxUtils.<Data<OrderPayBean>>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data<OrderPayBean>>() {

                    @Override
                    public void onNext(@NonNull Data<OrderPayBean> data) {
                        LogUtil.d(data.toString());
                        if (checkJsonCode(data, true)) {
                            getView().onSuccess(data.data);
                        } else {
                            getView().onFail();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        LogUtil.d(e.toString());
                        getView().onFail();
                    }
                });
    }

    public void payOrderNotice(OrderPayBean orderPayBean) {
        Map<String, Object> objectToMap = ObjectUtil.objectToMap(orderPayBean);
        String sign = ASCIIUtil.getSign(objectToMap, Constant.PAY_APP_SECRET);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("result_code", 200);
        jsonObject.put("result_message", "success");
        JSONObject orderPayData = TemporaryDataUtil.getOrderPayData(orderPayBean);
        jsonObject.put("data", orderPayData);
        jsonObject.put("sign", sign);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
        myApi.payOrderNotice(requestBody)
                .compose(RxUtils.<Data<String>>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data<String>>() {

                    @Override
                    public void onNext(@NonNull Data<String> data) {
                        LogUtil.d(data.toString());
                        if (checkJsonCode(data, true)) {
                            getView().onStatusSuccess(data.success);
                        } else {
                            getView().onFail();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        LogUtil.d(e.toString());
                        getView().onFail();
                    }
                });
    }

    void getOrderStatus(String orderSeq) {
        myApi.getOrderStatus(orderSeq)
                .compose(RxUtils.<Data<String>>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data<String>>() {

                    @Override
                    public void onNext(@NonNull Data<String> data) {
                        if (checkJsonCode(data, true)) {
                            getView().onStatusSuccess(data.success);
                        } else {
                            getView().onFail();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail();
                    }
                });
    }
}
