package com.lsz.atm.ui.commodity;

import com.lsz.atm.json.commodity.CommoditiesBean;
import com.util_code.base.mvp.MvpView;

public interface CommodityView extends MvpView {

    interface PackageListener {

        void onCommoditiesAdd(CommoditiesBean commoditiesBean);

        void onCommoditiesDelete(CommoditiesBean commoditiesBean);

    }

}
