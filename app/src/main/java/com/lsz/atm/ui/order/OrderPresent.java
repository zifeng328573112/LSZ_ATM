package com.lsz.atm.ui.order;

import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

import androidx.annotation.NonNull;

import com.common.config.MyApi;
import com.common.retrofit.RxObserver;
import com.common.retrofit.json.Data;
import com.common.thirdparty.sdkUtil.QRCodeTypeUtils;
import com.facebook.stetho.common.LogUtil;
import com.lsz.atm.json.OrderBean;
import com.lsz.atm.json.PayRequestBean;
import com.lsz.atm.json.VIPMemberBean;
import com.lsz.atm.viewutil.BigDecimalUtils;
import com.util_code.base.mvp.RxMvpPresenter;
import com.util_code.utils.RxUtils;
import com.util_code.utils.ToastUtils;

import javax.inject.Inject;

import okhttp3.RequestBody;

public class OrderPresent extends RxMvpPresenter<OrderView> {

    protected MyApi mMyApi;

    @Inject
    public OrderPresent(MyApi myApi) {
        this.mMyApi = myApi;
    }

    public void getOrderCreate(RequestBody requestBody) {
        mMyApi.postOrderCreate(requestBody)
                .compose(RxUtils.<Data<OrderBean>>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data<OrderBean>>() {

                    @Override
                    public void onNext(@NonNull Data<OrderBean> data) {
                        if (checkJsonCode(data, true)) {
                            getView().onOrderSuccess(data.data);
                        } else {
                            ToastUtils.showLongToast("订单生成失败");
                            getView().onFail();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        ToastUtils.showLongToast("订单生成失败");
                        getView().onFail();
                    }
                });
    }

    public void getMember(String id) {
        mMyApi.getMember(id)
                .compose(RxUtils.<Data<VIPMemberBean>>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data<VIPMemberBean>>() {

                    @Override
                    public void onNext(@NonNull Data<VIPMemberBean> data) {
                        if (checkJsonCode(data, true)) {
                            getView().onVIPMember(data.data);
                        } else {
                            getView().onFail();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail();
                    }
                });
    }

    private Handler mHandler = new Handler();
    private StringBuilder sb = new StringBuilder();

    public boolean handleKeyEvent(KeyEvent event) {
        int action = event.getAction();
        switch (action) {
            case KeyEvent.ACTION_DOWN:
                if (event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_UP) {
                    return false;
                }
                if (event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_DOWN) {
                    return false;
                }
                if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                    return false;
                }
                if (event.getKeyCode() == KeyEvent.KEYCODE_MENU) {
                    return false;
                }
                if (event.getKeyCode() == KeyEvent.KEYCODE_HOME) {
                    return false;
                }
                if (event.getKeyCode() == KeyEvent.KEYCODE_POWER) {
                    return false;
                }
                int unicodeChar = event.getUnicodeChar();
                if (unicodeChar > 0 && unicodeChar < 128) {
                    sb.append((char) unicodeChar);
                    mHandler.postDelayed(this::spliceQrCode, 500);
                } else {
                    getView().onFail();
                }
                return false;
            default:
                break;
        }
        return false;
    }

    /**
     * 拼接付款码
     */
    private void spliceQrCode() {
        if (sb.length() > 0) {
            String qrCode = sb.toString().trim();
            getCouponGet(qrCode);
            Log.d("ABC", sb.toString());
            sb.setLength(0);
        }
    }

    public void getCouponGet(String couponCode) {
        getView().onDisountStart();
        mMyApi.getCouponGet(couponCode)
                .compose(RxUtils.<Data<String>>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data<String>>() {

                    @Override
                    public void onNext(@NonNull Data<String> data) {
                        if (checkJsonCode(data, true)) {
                            ToastUtils.showLongToast("订单已优惠 ¥" + BigDecimalUtils.round(data.data, 2));
                            getView().onDiscountAmount(data.data, couponCode);
                        } else {
                            ToastUtils.showLongToast("该二维码无效，请重新出示二维码");
                            getView().onFail();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        ToastUtils.showLongToast("该二维码无效，请重新出示二维码");
                        getView().onFail();
                    }
                });
    }
}
