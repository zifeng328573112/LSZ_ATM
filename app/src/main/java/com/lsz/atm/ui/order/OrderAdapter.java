package com.lsz.atm.ui.order;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.request.RequestOptions;
import com.lsz.atm.R;
import com.lsz.atm.TemporaryDataUtil;
import com.lsz.atm.db.bean.CommodityBean;
import com.lsz.atm.db.bean.PackageBean;
import com.lsz.atm.db.dao_utils.DaoUtils;
import com.lsz.atm.viewutil.BigDecimalUtils;
import com.lsz.atm.viewutil.SpannableUtil;
import com.util_code.dagger.help.GlideApp;
import com.util_code.widget.recycleadpter.BaseRecycleViewAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderAdapter extends BaseRecycleViewAdapter<CommodityBean> {

    private OrderItemClicklistener orderItemClicklistener;

    private String currency = mContext.getResources().getString(R.string.currency);

    public OrderAdapter(Context context) {
        super(context);
    }

    public void setItemOnClicklistener(OrderItemClicklistener orderItemClicklistener) {
        this.orderItemClicklistener = orderItemClicklistener;
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, CommodityBean item) {
        if (holder instanceof OrderHolder) {
            OrderHolder orderHolder = (OrderHolder) holder;
            Drawable drawable = mContext.getResources().getDrawable(R.drawable.bg_menu_food);
            RequestOptions options = new RequestOptions();
            options.placeholder(drawable);
            GlideApp.with(mContext).load(item.imgIcon).apply(options).dontAnimate().into(orderHolder.ivItemImg);
            orderHolder.tvItemName.setText(item.name);

            String totalA = BigDecimalUtils.round(item.totalPriceList, 2);
            List<PackageBean> packageList = DaoUtils.getPackageInstance().loadPackageByRandomId(item.randomId);
            for (PackageBean packageBean : packageList) {
                String tAmount = TemporaryDataUtil.getTotalAmount(packageBean.commodiTotalPrice, packageBean.commodiTotalPrice, item.cSelectNumber);
                totalA = BigDecimalUtils.add(totalA, tAmount, 2);
            }
            String amount = currency + " " + totalA;
            SpannableString textSpannable = SpannableUtil.setTextSpannable(amount, 2, amount.length());
            orderHolder.tvItemAmount.setText(textSpannable);
            orderHolder.tvItemNumber.setText(item.cSelectNumber + "");
            orderHolder.ivItemDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (orderItemClicklistener != null) {
                        orderItemClicklistener.onItemDelete(item);
                    }
                }
            });
            orderHolder.ivItemAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (orderItemClicklistener != null) {
                        orderItemClicklistener.onItemAdd(item);
                    }
                }
            });
            orderHolder.tvItemLabel.setVisibility(View.GONE);
            if (item.isPackage) {
                orderHolder.tvItemPackage.setVisibility(View.VISIBLE);
                orderHolder.tvItemPackage.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        if (item.selectPackageDetail) {
                            item.selectPackageDetail = false;
                        } else {
                            item.selectPackageDetail = true;
                        }
                        notifyItemChanged(holder.getLayoutPosition() - getHeaderLayoutCount());
                    }
                });
                if (item.selectPackageDetail) {
                    orderHolder.tvItemPackage.setText(getString(R.string.order_package_collapse));
                    orderHolder.llPackageView.setVisibility(View.VISIBLE);
                    setPackageData(item.randomId, orderHolder.tvPackageContent);
                } else {
                    orderHolder.tvItemPackage.setText(getString(R.string.order_package_detail));
                    orderHolder.llPackageView.setVisibility(View.GONE);
                }
            } else {
                orderHolder.tvItemPackage.setVisibility(View.GONE);
                orderHolder.llPackageView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_order_view, parent, false);
        return new OrderHolder(view);
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    private String getString(int stringId) {
        return mContext.getResources().getString(stringId);
    }

    private void setPackageData(long randomId, TextView tvPackageContent) {
        StringBuffer packageName = new StringBuffer();
        List<PackageBean> packageList = DaoUtils.getPackageInstance().loadPackageByRandomId(randomId);
        for (PackageBean packageB : packageList) {
            if (!TextUtils.isEmpty(packageB.buyMin) && Integer.parseInt(packageB.buyMin) > 0) {
                packageName.append(packageB.name + packageB.buyMin + "份，");
            }
        }
        if (TextUtils.isEmpty(packageName)) return;
        packageName.deleteCharAt(packageName.length() - 1);
        tvPackageContent.setText(packageName);
    }

    class OrderHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_item_img)
        ImageView ivItemImg;
        @BindView(R.id.tv_item_name)
        TextView tvItemName;
        @BindView(R.id.tv_item_label)
        TextView tvItemLabel;
        @BindView(R.id.tv_item_amount)
        TextView tvItemAmount;
        @BindView(R.id.iv_item_delete)
        ImageView ivItemDelete;
        @BindView(R.id.tv_item_snumber)
        TextView tvItemNumber;
        @BindView(R.id.iv_item_add)
        ImageView ivItemAdd;
        @BindView(R.id.tv_item_package)
        TextView tvItemPackage;
        @BindView(R.id.ll_package_view)
        LinearLayout llPackageView;
        @BindView(R.id.tv_package_content)
        TextView tvPackageContent;

        OrderHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OrderItemClicklistener {

        void onItemDelete(CommodityBean item);

        void onItemAdd(CommodityBean item);

    }
}
