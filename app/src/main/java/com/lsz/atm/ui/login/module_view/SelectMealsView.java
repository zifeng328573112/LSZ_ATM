package com.lsz.atm.ui.login.module_view;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.common.config.Global;
import com.lsz.atm.R;
import com.lsz.atm.json.ATMBean;

public class SelectMealsView<T extends Activity> implements View.OnClickListener {

    Activity activity;
    private TextView tvSelectMealsType;
    private LinearLayout llTakeAway;
    private ImageView ivPackImg;
    private TextView tvPackTitle;
    private LinearLayout llDineIn;
    private ImageView ivMealsImg;
    private TextView tvMealsTItle;

    SelectMealsClickListener mealsClickListener;

    public SelectMealsView(T activity, SelectMealsClickListener mealsClickListener) {
        this.activity = activity;
        this.mealsClickListener = mealsClickListener;
    }

    public View getView() {
        View view = View.inflate(activity, R.layout.layout_select_meals, null);
        tvSelectMealsType = view.findViewById(R.id.tv_select_meals_type);
        llTakeAway = view.findViewById(R.id.ll_take_away);
        ivPackImg = view.findViewById(R.id.iv_pack_img);
        tvPackTitle = view.findViewById(R.id.tv_pack_title);
        llDineIn = view.findViewById(R.id.ll_dine_in);
        ivMealsImg = view.findViewById(R.id.iv_meals_img);
        tvMealsTItle = view.findViewById(R.id.tv_meals_title);

        llTakeAway.setOnClickListener(this);
        llDineIn.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_take_away:
                setTakeDine(ATMBean.PICK_UP);
                mealsClickListener.onTakeAwayClick();
                break;
            case R.id.ll_dine_in:
                setTakeDine(ATMBean.DINE_IN);
                mealsClickListener.onDineInClick();
                break;
            default:
                break;
        }
    }

    public void setTakeDine(String takeDine) {
        ATMBean atmBean = Global.getAtmBean();
        if (atmBean != null) {
            atmBean.orderType = takeDine;
        }
        Global.setAtmBean(atmBean);
    }

    public interface SelectMealsClickListener {

        void onTakeAwayClick();

        void onDineInClick();

    }

}
