package com.lsz.atm.ui.login;

import android.util.Base64;

import androidx.annotation.NonNull;

import com.common.config.Global;
import com.common.config.MyApi;
import com.common.retrofit.RxObserver;
import com.common.retrofit.json.Data;
import com.lsz.atm.TemporaryDataUtil;
import com.lsz.atm.json.LoginEntity;
import com.lsz.atm.json.OrderBean;
import com.lsz.atm.json.StartOrderBean;
import com.util_code.base.mvp.RxMvpPresenter;
import com.util_code.utils.RxUtils;
import com.util_code.utils.ToastUtils;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import okhttp3.RequestBody;

public class LoginPresent extends RxMvpPresenter<LoginView> {

    protected MyApi mMyApi;

    @Inject
    public LoginPresent(MyApi myApi) {
        this.mMyApi = myApi;
    }

    public void getHomePage() {
        mMyApi.getHomePage()
                .compose(RxUtils.<Data<StartOrderBean>>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data<StartOrderBean>>() {

                    @Override
                    public void onNext(@NonNull Data<StartOrderBean> data) {
                        if (checkJsonCode(data, false) && data.data != null) {
                            getView().onHomeBanner(data.data);
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getView().onFail();
                    }
                });
    }


    public void postLogin(String storeNo, String phone, String password) {
//        {
//            "password": "MTIzNDU2",
//                "storeNo": "2800001",
//                "username": "001"
//        }
        RequestBody requestBody = TemporaryDataUtil.setLogin(storeNo, phone, password);
        mMyApi.postLogin(requestBody)
                .compose(RxUtils.<Data<LoginEntity>>applySchedulersLifeCycle(getView()))
                .subscribe(new RxObserver<Data<LoginEntity>>() {

                    @Override
                    public void onNext(@NonNull Data<LoginEntity> data) {
                        if (checkJsonCode(data, true)) {
                            Global.setLoginBean(data.data);
                            getView().onSuccess(data.data);
                        } else {
                            ToastUtils.showLongToast("登录失败");
                            getView().onFail();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        ToastUtils.showLongToast("登录失败");
                        getView().onFail();
                    }
                });
    }

}
