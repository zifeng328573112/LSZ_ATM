package com.lsz.atm.ui.menu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.request.RequestOptions;
import com.lsz.atm.R;
import com.lsz.atm.json.MenuBean;
import com.util_code.dagger.help.GlideApp;
import com.util_code.widget.recycleadpter.BaseRecycleViewAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MenuAdapter extends BaseRecycleViewAdapter<MenuBean> {

    private OnItemClickListener itemClickListener;

    public MenuAdapter(Context context) {
        super(context);
    }

    @Override
    protected void onBindBaseViewHolder(RecyclerView.ViewHolder holder, MenuBean item) {
        if (holder instanceof MenuHolder) {
            MenuHolder menuHolder = (MenuHolder) holder;
            RequestOptions requestOptions = new RequestOptions();
            GlideApp.with(mContext).load(item.imgIcon).apply(requestOptions).into(menuHolder.ivMenuImg);

            menuHolder.tvMenuName.setText(item.name);

            if (item.menuSelect) {
                menuHolder.ivMenuPointer.setVisibility(View.VISIBLE);
                menuHolder.rlMenuPointer.setBackgroundColor(mContext.getResources().getColor(R.color.app_white));
            } else {
                menuHolder.ivMenuPointer.setVisibility(View.INVISIBLE);
                menuHolder.rlMenuPointer.setBackgroundColor(mContext.getResources().getColor(R.color.c_f9faf9));
            }

            menuHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (itemClickListener != null) {
                        itemClickListener.onItemClick(view, item);
                    }
                }
            });
        }
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(mContext).inflate(R.layout.item_menu_view, parent, false);
        return new MenuHolder(inflate);
    }

    @Override
    protected int getBaseItemViewType(int position) {
        return 0;
    }

    class MenuHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ll_menu_pointer)
        RelativeLayout rlMenuPointer;
        @BindView(R.id.iv_menu_pointer)
        TextView ivMenuPointer;
        @BindView(R.id.iv_menu_img)
        ImageView ivMenuImg;
        @BindView(R.id.tv_menu_name)
        TextView tvMenuName;

        MenuHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setOnItemClickListener(OnItemClickListener itemClickListener){
        this.itemClickListener = itemClickListener;
    }

}
