package com.lsz.atm.ui.pay;

import com.lsz.atm.json.OrderPayBean;
import com.util_code.base.mvp.MvpView;

public interface PayView extends MvpView {

    void onSuccess(OrderPayBean orderPayBean);

    void onStatusSuccess(boolean isTrue);

    void onPayError(String sub_code);

    void onFail();

}
