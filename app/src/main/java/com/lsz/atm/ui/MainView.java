package com.lsz.atm.ui;

import com.util_code.base.mvp.MvpView;
import com.lsz.atm.json.CheckUpdate;

public interface MainView extends MvpView {


    void OnVersionUpdate(CheckUpdate result);

    void OnVersionUpdateError();
}
