package com.lsz.atm.db.dao_manager;

import android.content.Context;

import com.lsz.atm.db.PackageBeanDao;
import com.lsz.atm.db.bean.PackageBean;
import com.lsz.atm.db.dao_help.BaseDao;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.ArrayList;
import java.util.List;

public class PackageManager extends BaseDao<PackageBean> {

    public PackageManager(Context context) {
        super(context);
    }

    public void insertPackageObject(PackageBean packageBean) {
        long count = loadCount();
        if (count == 0) {
            packageBean.index = 1;
        } else {
            packageBean.index = count + 1;
        }
        insertObject(packageBean);
    }

    public List<PackageBean> loadAll() {
        List<PackageBean> packageBeanList = queryAll(PackageBean.class);
        if (packageBeanList == null || packageBeanList.size() == 0) {
            return null;
        } else {
            return packageBeanList;
        }
    }

    public long loadCount() {
        try {
            return daoSession.getPackageBeanDao().count();
        } catch (IllegalStateException e) {
            return 0L;
        }
    }

    public PackageBean loadById(long id) {
        return queryById(id, PackageBean.class);
    }

    public PackageBean loadPackageById(long bundleId) {
        QueryBuilder queryBuilder = daoSession.getPackageBeanDao().queryBuilder();
        queryBuilder.where(PackageBeanDao.Properties.BundleId.eq(bundleId));
        int size = queryBuilder.list().size();
        if (size > 0) {
            return (PackageBean) queryBuilder.list().get(0);
        } else {
            return new PackageBean();
        }
    }

    public List<PackageBean> loadPackageByRandomId(long key) {
        QueryBuilder queryBuilder = daoSession.getPackageBeanDao().queryBuilder();
        queryBuilder.where(PackageBeanDao.Properties.RandomId.eq(key));
        if (queryBuilder.list() != null && queryBuilder.list().size() > 0) {
            return queryBuilder.list();
        } else {
            return new ArrayList<>();
        }
    }

    public List<PackageBean> loadPackageByRandomId(long randomId, String proComId) {
        QueryBuilder queryBuilder = daoSession.getPackageBeanDao().queryBuilder();
        queryBuilder.where(PackageBeanDao.Properties.RandomId.eq(randomId), PackageBeanDao.Properties.ProComId.eq(proComId));
        int size = queryBuilder.list().size();
        if (size > 0) {
            return queryBuilder.list();
        } else {
            return new ArrayList<>();
        }
    }

    public List<PackageBean> loadPackageByName(String key) {
        QueryBuilder queryBuilder = daoSession.getPackageBeanDao().queryBuilder();
        queryBuilder.where(PackageBeanDao.Properties.Name.eq(key));
        int size = queryBuilder.list().size();
        if (size > 0) {
            return queryBuilder.list();
        } else {
            return new ArrayList<>();
        }
    }

    public void deleteAll() {
        daoSession.getPackageBeanDao().deleteAll();
    }

    public void deleteObj(PackageBean packageBean) {
        deleteObject(packageBean);
    }

    public void deleteById(long id) {
        daoSession.getPackageBeanDao().deleteByKey(id);
    }

    public void closeDB() {
        closeDataBase();
    }
}
