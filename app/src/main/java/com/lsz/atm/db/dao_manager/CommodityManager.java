package com.lsz.atm.db.dao_manager;

import android.content.Context;
import android.text.TextUtils;

import com.common.util.RandomUtil;
import com.lsz.atm.db.CommodityBeanDao;
import com.lsz.atm.db.dao_help.BaseDao;
import com.lsz.atm.db.bean.CommodityBean;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.List;

public class CommodityManager extends BaseDao<CommodityBean> {

    public CommodityManager(Context context) {
        super(context);
    }

    public long insertCommodityObject(CommodityBean commodityBean) {
        commodityBean.randomId = RandomUtil.RandomNumber();
        insertObject(commodityBean);
        return commodityBean.randomId;
    }

    public void insertCommodityList(List<CommodityBean> commodityBeanList) {
        for (CommodityBean commodityBean : commodityBeanList) {
            commodityBean.randomId = RandomUtil.RandomNumber();
        }
        insertMultObject(commodityBeanList);
    }

    public void updateCommodity(CommodityBean commodityBean) {
        updateObject(commodityBean);
    }

    public List<CommodityBean> loadAll() {
        try {
            List<CommodityBean> commodityBeanList = daoSession.getCommodityBeanDao().loadAll();
//        List<CommodityBean> commodityBeanList = queryAll(CommodityBean.class);
            if (commodityBeanList == null || commodityBeanList.size() == 0) {
                return null;
            } else {
                return commodityBeanList;
            }
        } catch (Exception e) {
            return null;
        }
    }

    public CommodityBean loadById(Long id) {
        return daoSession.getCommodityBeanDao().load(id);
    }

    public CommodityBean loadById(String id) {
        try {
            QueryBuilder queryBuilder = daoSession.getCommodityBeanDao().queryBuilder();
            queryBuilder.where(CommodityBeanDao.Properties.Id.eq(id));
            CommodityBean commodityBean = (CommodityBean) queryBuilder.list().get(0);
            return commodityBean;
        } catch (Exception e) {
            return null;
        }
    }

    public List<CommodityBean> loadCommodityById(String key) {
        try {
            QueryBuilder queryBuilder = daoSession.getCommodityBeanDao().queryBuilder();
            queryBuilder.where(CommodityBeanDao.Properties.Id.eq(key));
            int size = queryBuilder.list().size();
            if (size > 0) {
                return queryBuilder.list();
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }

    public void deleteAll() {
        daoSession.getCommodityBeanDao().deleteAll();
    }

    public void deleteObj(CommodityBean commodityBean) {
        deleteObject(commodityBean);
    }

    public void deleteById(String id) {
        if (TextUtils.isEmpty(id)) {
            return;
        }
        daoSession.getCommodityBeanDao().deleteByKey(Long.parseLong(id));
    }

    public void deleteByIds(List<Long> ids) {
        daoSession.getCommodityBeanDao().deleteByKeyInTx(ids);
    }

    public void closeDB() {
        closeDataBase();
    }

}
