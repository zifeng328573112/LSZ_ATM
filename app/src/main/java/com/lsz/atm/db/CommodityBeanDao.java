package com.lsz.atm.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;

import com.lsz.atm.db.bean.CommodityBean;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "COMMODITY_BEAN".
*/
public class CommodityBeanDao extends AbstractDao<CommodityBean, Long> {

    public static final String TABLENAME = "COMMODITY_BEAN";

    /**
     * Properties of entity CommodityBean.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property RandomId = new Property(0, long.class, "randomId", true, "_id");
        public final static Property Id = new Property(1, String.class, "id", false, "ID");
        public final static Property MenuId = new Property(2, String.class, "menuId", false, "MENU_ID");
        public final static Property CSelectNumber = new Property(3, long.class, "cSelectNumber", false, "C_SELECT_NUMBER");
        public final static Property Name = new Property(4, String.class, "name", false, "NAME");
        public final static Property Alias = new Property(5, String.class, "alias", false, "ALIAS");
        public final static Property Desc = new Property(6, String.class, "desc", false, "DESC");
        public final static Property ImgIcon = new Property(7, String.class, "imgIcon", false, "IMG_ICON");
        public final static Property ImgMini = new Property(8, String.class, "imgMini", false, "IMG_MINI");
        public final static Property ImgFace = new Property(9, String.class, "imgFace", false, "IMG_FACE");
        public final static Property PriceList = new Property(10, String.class, "priceList", false, "PRICE_LIST");
        public final static Property TotalPriceList = new Property(11, String.class, "totalPriceList", false, "TOTAL_PRICE_LIST");
        public final static Property PriceSell = new Property(12, String.class, "priceSell", false, "PRICE_SELL");
        public final static Property Ranking = new Property(13, String.class, "ranking", false, "RANKING");
        public final static Property IsPackage = new Property(14, boolean.class, "isPackage", false, "IS_PACKAGE");
        public final static Property SelectPackageDetail = new Property(15, boolean.class, "selectPackageDetail", false, "SELECT_PACKAGE_DETAIL");
    }


    public CommodityBeanDao(DaoConfig config) {
        super(config);
    }
    
    public CommodityBeanDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"COMMODITY_BEAN\" (" + //
                "\"_id\" INTEGER PRIMARY KEY NOT NULL ," + // 0: randomId
                "\"ID\" TEXT NOT NULL ," + // 1: id
                "\"MENU_ID\" TEXT NOT NULL ," + // 2: menuId
                "\"C_SELECT_NUMBER\" INTEGER NOT NULL ," + // 3: cSelectNumber
                "\"NAME\" TEXT NOT NULL ," + // 4: name
                "\"ALIAS\" TEXT NOT NULL ," + // 5: alias
                "\"DESC\" TEXT NOT NULL ," + // 6: desc
                "\"IMG_ICON\" TEXT NOT NULL ," + // 7: imgIcon
                "\"IMG_MINI\" TEXT NOT NULL ," + // 8: imgMini
                "\"IMG_FACE\" TEXT NOT NULL ," + // 9: imgFace
                "\"PRICE_LIST\" TEXT NOT NULL ," + // 10: priceList
                "\"TOTAL_PRICE_LIST\" TEXT NOT NULL ," + // 11: totalPriceList
                "\"PRICE_SELL\" TEXT NOT NULL ," + // 12: priceSell
                "\"RANKING\" TEXT NOT NULL ," + // 13: ranking
                "\"IS_PACKAGE\" INTEGER NOT NULL ," + // 14: isPackage
                "\"SELECT_PACKAGE_DETAIL\" INTEGER NOT NULL );"); // 15: selectPackageDetail
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"COMMODITY_BEAN\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, CommodityBean entity) {
        stmt.clearBindings();
        stmt.bindLong(1, entity.getRandomId());
        stmt.bindString(2, entity.getId());
        stmt.bindString(3, entity.getMenuId());
        stmt.bindLong(4, entity.getCSelectNumber());
        stmt.bindString(5, entity.getName());
        stmt.bindString(6, entity.getAlias());
        stmt.bindString(7, entity.getDesc());
        stmt.bindString(8, entity.getImgIcon());
        stmt.bindString(9, entity.getImgMini());
        stmt.bindString(10, entity.getImgFace());
        stmt.bindString(11, entity.getPriceList());
        stmt.bindString(12, entity.getTotalPriceList());
        stmt.bindString(13, entity.getPriceSell());
        stmt.bindString(14, entity.getRanking());
        stmt.bindLong(15, entity.getIsPackage() ? 1L: 0L);
        stmt.bindLong(16, entity.getSelectPackageDetail() ? 1L: 0L);
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, CommodityBean entity) {
        stmt.clearBindings();
        stmt.bindLong(1, entity.getRandomId());
        stmt.bindString(2, entity.getId());
        stmt.bindString(3, entity.getMenuId());
        stmt.bindLong(4, entity.getCSelectNumber());
        stmt.bindString(5, entity.getName());
        stmt.bindString(6, entity.getAlias());
        stmt.bindString(7, entity.getDesc());
        stmt.bindString(8, entity.getImgIcon());
        stmt.bindString(9, entity.getImgMini());
        stmt.bindString(10, entity.getImgFace());
        stmt.bindString(11, entity.getPriceList());
        stmt.bindString(12, entity.getTotalPriceList());
        stmt.bindString(13, entity.getPriceSell());
        stmt.bindString(14, entity.getRanking());
        stmt.bindLong(15, entity.getIsPackage() ? 1L: 0L);
        stmt.bindLong(16, entity.getSelectPackageDetail() ? 1L: 0L);
    }

    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.getLong(offset + 0);
    }    

    @Override
    public CommodityBean readEntity(Cursor cursor, int offset) {
        CommodityBean entity = new CommodityBean( //
            cursor.getLong(offset + 0), // randomId
            cursor.getString(offset + 1), // id
            cursor.getString(offset + 2), // menuId
            cursor.getLong(offset + 3), // cSelectNumber
            cursor.getString(offset + 4), // name
            cursor.getString(offset + 5), // alias
            cursor.getString(offset + 6), // desc
            cursor.getString(offset + 7), // imgIcon
            cursor.getString(offset + 8), // imgMini
            cursor.getString(offset + 9), // imgFace
            cursor.getString(offset + 10), // priceList
            cursor.getString(offset + 11), // totalPriceList
            cursor.getString(offset + 12), // priceSell
            cursor.getString(offset + 13), // ranking
            cursor.getShort(offset + 14) != 0, // isPackage
            cursor.getShort(offset + 15) != 0 // selectPackageDetail
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, CommodityBean entity, int offset) {
        entity.setRandomId(cursor.getLong(offset + 0));
        entity.setId(cursor.getString(offset + 1));
        entity.setMenuId(cursor.getString(offset + 2));
        entity.setCSelectNumber(cursor.getLong(offset + 3));
        entity.setName(cursor.getString(offset + 4));
        entity.setAlias(cursor.getString(offset + 5));
        entity.setDesc(cursor.getString(offset + 6));
        entity.setImgIcon(cursor.getString(offset + 7));
        entity.setImgMini(cursor.getString(offset + 8));
        entity.setImgFace(cursor.getString(offset + 9));
        entity.setPriceList(cursor.getString(offset + 10));
        entity.setTotalPriceList(cursor.getString(offset + 11));
        entity.setPriceSell(cursor.getString(offset + 12));
        entity.setRanking(cursor.getString(offset + 13));
        entity.setIsPackage(cursor.getShort(offset + 14) != 0);
        entity.setSelectPackageDetail(cursor.getShort(offset + 15) != 0);
     }
    
    @Override
    protected final Long updateKeyAfterInsert(CommodityBean entity, long rowId) {
        entity.setRandomId(rowId);
        return rowId;
    }
    
    @Override
    public Long getKey(CommodityBean entity) {
        if(entity != null) {
            return entity.getRandomId();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasKey(CommodityBean entity) {
        throw new UnsupportedOperationException("Unsupported for entities with a non-null key");
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
}
