package com.lsz.atm.db.dao_utils;

import android.content.Context;

import com.lsz.atm.db.dao_manager.CommodityManager;
import com.lsz.atm.db.dao_manager.PackageManager;

public class DaoUtils {

    public static CommodityManager commodityManager;

    public static Context context;
    private static PackageManager packageManager;

    public static void init(Context context) {
        DaoUtils.context = context.getApplicationContext();
    }

    /**
     * 单列模式获取CustomerManager对象
     *
     * @return
     */
    public static CommodityManager getCommodityInstance() {
        if (commodityManager == null) {
            commodityManager = new CommodityManager(context);
        }
        return commodityManager;
    }

    public static PackageManager getPackageInstance() {
        if (packageManager == null) {
            packageManager = new PackageManager(context);
        }
        return packageManager;
    }

}
