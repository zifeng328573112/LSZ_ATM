package com.lsz.atm.db.bean;

import androidx.annotation.Keep;

import com.lsz.atm.json.McTemporary;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;

import java.io.Serializable;

@Keep
@Entity
public class CommodityBean implements McTemporary, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    public long randomId;
    @NotNull
    public String id;
    @NotNull
    public String menuId;
    @NotNull
    public long cSelectNumber;// 产品选择数量
    @NotNull
    public String name;
    @NotNull
    public String alias;
    @NotNull
    public String desc;
    @NotNull
    public String imgIcon;
    @NotNull
    public String imgMini;
    @NotNull
    public String imgFace;
    @NotNull
    public String priceList;
    @NotNull
    public String totalPriceList;//总金额
    @NotNull
    public String priceSell;
    @NotNull
    public String ranking;
    public boolean isPackage = false;
    public boolean selectPackageDetail = false;

    public CommodityBean() {
        super();
    }

    @Generated(hash = 1358749729)
    public CommodityBean(long randomId, @NotNull String id, @NotNull String menuId, long cSelectNumber, @NotNull String name, @NotNull String alias, @NotNull String desc, @NotNull String imgIcon, @NotNull String imgMini, @NotNull String imgFace, @NotNull String priceList,
            @NotNull String totalPriceList, @NotNull String priceSell, @NotNull String ranking, boolean isPackage, boolean selectPackageDetail) {
        this.randomId = randomId;
        this.id = id;
        this.menuId = menuId;
        this.cSelectNumber = cSelectNumber;
        this.name = name;
        this.alias = alias;
        this.desc = desc;
        this.imgIcon = imgIcon;
        this.imgMini = imgMini;
        this.imgFace = imgFace;
        this.priceList = priceList;
        this.totalPriceList = totalPriceList;
        this.priceSell = priceSell;
        this.ranking = ranking;
        this.isPackage = isPackage;
        this.selectPackageDetail = selectPackageDetail;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public long getRandomId() {
        return randomId;
    }

    public void setRandomId(long randomId) {
        this.randomId = randomId;
    }

    public long getCSelectNumber() {
        return cSelectNumber;
    }

    public void setCSelectNumber(long cSelectNumber) {
        this.cSelectNumber = cSelectNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImgIcon() {
        return imgIcon;
    }

    public void setImgIcon(String imgIcon) {
        this.imgIcon = imgIcon;
    }

    public String getImgMini() {
        return imgMini;
    }

    public void setImgMini(String imgMini) {
        this.imgMini = imgMini;
    }

    public String getImgFace() {
        return imgFace;
    }

    public void setImgFace(String imgFace) {
        this.imgFace = imgFace;
    }

    public String getPriceList() {
        return priceList;
    }

    public void setPriceList(String priceList) {
        this.priceList = priceList;
    }

    public String getTotalPriceList() {
        return totalPriceList;
    }

    public void setTotalPriceList(String totalPriceList) {
        this.totalPriceList = totalPriceList;
    }

    public String getPriceSell() {
        return priceSell;
    }

    public void setPriceSell(String priceSell) {
        this.priceSell = priceSell;
    }

    public String getRanking() {
        return ranking;
    }

    public void setRanking(String ranking) {
        this.ranking = ranking;
    }

    public boolean getIsPackage() {
        return isPackage;
    }

    public void setIsPackage(boolean aPackage) {
        isPackage = aPackage;
    }

    @Override
    public String toString() {
        return "CommodityBean{" +
                "id='" + id + '\'' +
                ", menuId='" + menuId + '\'' +
                ", randomId=" + randomId +
                ", cSelectNumber=" + cSelectNumber +
                ", name='" + name + '\'' +
                ", alias='" + alias + '\'' +
                ", desc='" + desc + '\'' +
                ", imgIcon='" + imgIcon + '\'' +
                ", imgMini='" + imgMini + '\'' +
                ", imgFace='" + imgFace + '\'' +
                ", priceList='" + priceList + '\'' +
                ", totalPriceList='" + totalPriceList + '\'' +
                ", priceSell='" + priceSell + '\'' +
                ", ranking='" + ranking + '\'' +
                '}';
    }

    @Override
    public int getmType() {
        return 0;
    }

    public boolean getSelectPackageDetail() {
        return this.selectPackageDetail;
    }

    public void setSelectPackageDetail(boolean selectPackageDetail) {
        this.selectPackageDetail = selectPackageDetail;
    }
}
