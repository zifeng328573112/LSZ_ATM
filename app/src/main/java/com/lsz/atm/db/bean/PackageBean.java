package com.lsz.atm.db.bean;

import androidx.annotation.Keep;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;

@Keep
@Entity
public class PackageBean {

    @Id
    public long index;
    @NotNull
    public String proComId;
    @NotNull
    public long randomId;
    @NotNull
    public String bundleId;
    @NotNull
    public String name;
    @NotNull
    public String priceList;
    @NotNull
    public String priceSell;
    @NotNull
    public String buyMin = "0";
    @NotNull
    public String buyMax = "0";
    @NotNull
    public String imgIcon;
    @NotNull
    public String imgMini;
    @NotNull
    public boolean showPrice;
    @NotNull
    public boolean selected;
    @NotNull
    public String ranking;

    public String commodiTotalPrice = "0";

    public PackageBean() {
        super();
    }

    @Generated(hash = 692215466)
    public PackageBean(long index, @NotNull String proComId, long randomId,
            @NotNull String bundleId, @NotNull String name,
            @NotNull String priceList, @NotNull String priceSell,
            @NotNull String buyMin, @NotNull String buyMax, @NotNull String imgIcon,
            @NotNull String imgMini, boolean showPrice, boolean selected,
            @NotNull String ranking, String commodiTotalPrice) {
        this.index = index;
        this.proComId = proComId;
        this.randomId = randomId;
        this.bundleId = bundleId;
        this.name = name;
        this.priceList = priceList;
        this.priceSell = priceSell;
        this.buyMin = buyMin;
        this.buyMax = buyMax;
        this.imgIcon = imgIcon;
        this.imgMini = imgMini;
        this.showPrice = showPrice;
        this.selected = selected;
        this.ranking = ranking;
        this.commodiTotalPrice = commodiTotalPrice;
    }

    public long getIndex() {
        return this.index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public String getProComId() {
        return this.proComId;
    }

    public void setProComId(String proComId) {
        this.proComId = proComId;
    }

    public long getRandomId() {
        return this.randomId;
    }

    public void setRandomId(long randomId) {
        this.randomId = randomId;
    }

    public String getBundleId() {
        return this.bundleId;
    }

    public void setBundleId(String bundleId) {
        this.bundleId = bundleId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPriceList() {
        return this.priceList;
    }

    public void setPriceList(String priceList) {
        this.priceList = priceList;
    }

    public String getPriceSell() {
        return this.priceSell;
    }

    public void setPriceSell(String priceSell) {
        this.priceSell = priceSell;
    }

    public String getBuyMin() {
        return this.buyMin;
    }

    public void setBuyMin(String buyMin) {
        this.buyMin = buyMin;
    }

    public String getBuyMax() {
        return this.buyMax;
    }

    public void setBuyMax(String buyMax) {
        this.buyMax = buyMax;
    }

    public String getImgIcon() {
        return this.imgIcon;
    }

    public void setImgIcon(String imgIcon) {
        this.imgIcon = imgIcon;
    }

    public String getImgMini() {
        return this.imgMini;
    }

    public void setImgMini(String imgMini) {
        this.imgMini = imgMini;
    }

    public boolean getShowPrice() {
        return this.showPrice;
    }

    public void setShowPrice(boolean showPrice) {
        this.showPrice = showPrice;
    }

    public boolean getSelected() {
        return this.selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getRanking() {
        return this.ranking;
    }

    public void setRanking(String ranking) {
        this.ranking = ranking;
    }

    public String getCommodiTotalPrice() {
        return this.commodiTotalPrice;
    }

    public void setCommodiTotalPrice(String commodiTotalPrice) {
        this.commodiTotalPrice = commodiTotalPrice;
    }


}
