package com.common.download;

import com.lsz.atm.json.CheckUpdate;

public interface VersionUpdatePresent {

    void onCheckUpdate();//检测版本更新

    void onApkDowloadDialog(CheckUpdate checkUpdate);//apk下载弹窗

    void requestDownload(String sUpdateUrl, String downLoadApkFolde, String downLoadApkFileName);//下载apk

    void installApp(String downLoadApkFolde, String downLoadApkFileName);//安装apk

    interface CheckVersionResult{

        void versionCheckError();//版本检测失败

        void versionNoNeedDowload();//无需下载更新

        void versionDowloadError();//下载失败

        void versionDowloadSuccess();//下载成功



    }

}
