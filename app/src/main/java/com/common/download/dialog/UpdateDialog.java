package com.common.download.dialog;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lsz.atm.R;
import com.util_code.base.BaseDialog;


public class UpdateDialog extends BaseDialog {

    protected LinearLayout confirmButton;
    protected LinearLayout cancelButton;
    protected TextView contentView;
    protected View buttonDividerView;
    protected FrameLayout backConfirmView;

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_layout_update;
    }

    @Override
    protected void setupView(View rootView) {
        confirmButton = rootView.findViewById(R.id.confirm);
        cancelButton = rootView.findViewById(R.id.cancel);
        contentView = rootView.findViewById(R.id.content);
        buttonDividerView = rootView.findViewById(R.id.button_divider);
        backConfirmView = rootView.findViewById(R.id.fl_confirm_bg);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    public void setContent(String content) {
        contentView.setText(content);
    }

    public void setConfirmOnClickListener(View.OnClickListener confirmOnClickListener) {
        if (confirmButton != null && confirmOnClickListener != null) {
            confirmButton.setOnClickListener(confirmOnClickListener);
        }
    }

    public void setCancelOnClickListener(View.OnClickListener cancelOnClickListener) {
        if (cancelButton != null && cancelOnClickListener != null) {
            cancelButton.setOnClickListener(cancelOnClickListener);
        }
    }

    public void hideCancelButton() {
        cancelButton.setVisibility(View.GONE);
        buttonDividerView.setVisibility(View.GONE);
        backConfirmView.setBackgroundResource(R.drawable.view_dialog_bottom_confirm);
        confirmButton.setBackgroundResource(R.drawable.view_dialog_bottom);
    }

    public void hideConfirmButton() {
        confirmButton.setVisibility(View.GONE);
        buttonDividerView.setVisibility(View.GONE);
        cancelButton.setBackgroundResource(R.drawable.view_dialog_bottom);
    }

    public void setConfirmButtonBackground(int backgroundResource) {
        confirmButton.setBackgroundResource(backgroundResource);
    }

    public void setCancelButtonBackground(int backgroundResource) {
        cancelButton.setBackgroundResource(backgroundResource);
    }

    public static UpdateDialog newInstance() {
        UpdateDialog messageDialog = new UpdateDialog();
        Bundle args = new Bundle();
        args.putString("param", "param");
        messageDialog.setArguments(args);
        return messageDialog;
    }
}
