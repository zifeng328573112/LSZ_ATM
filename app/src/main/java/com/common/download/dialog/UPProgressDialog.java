package com.common.download.dialog;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;

import com.lsz.atm.R;
import com.util_code.base.BaseDialog;
import com.util_code.utils.DisplayUtils;

public class UPProgressDialog extends BaseDialog {

    LinearLayout ll_progress_id;
    FrameLayout fl_progress_id;
    TextView tv_progress_number;
    ImageView iv_progress_mark;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.customDialogTheme);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_layout_updateprogress;
    }

    @Override
    protected void setupView(View rootView) {
        ll_progress_id = rootView.findViewById(R.id.ll_progress_id);
        fl_progress_id = rootView.findViewById(R.id.fl_progress_id);
        tv_progress_number = rootView.findViewById(R.id.tv_progress_number);
        iv_progress_mark = rootView.findViewById(R.id.iv_progress_mark);
        getDialog().setCanceledOnTouchOutside(false);
        setCancelable(false);
    }

    public void setProgress(int progress) {
        if (getContext() == null) {
            return;
        }
        ViewGroup.LayoutParams lp = fl_progress_id.getLayoutParams();
        lp.width = ll_progress_id.getWidth() * progress / 100;
        if (lp.width < DisplayUtils.dp2px(24)) {
            lp.width = DisplayUtils.dp2px(24);
        } else if (iv_progress_mark.getVisibility() == View.VISIBLE && lp.width > ll_progress_id
                .getWidth() - iv_progress_mark.getWidth() - DisplayUtils.dp2px(8)) {
            iv_progress_mark.setVisibility(View.GONE);
        }
        tv_progress_number.setText(progress + "%");
        fl_progress_id.requestLayout();
    }
}
