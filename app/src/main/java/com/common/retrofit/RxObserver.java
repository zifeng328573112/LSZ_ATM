package com.common.retrofit;

import android.net.ParseException;
import android.text.TextUtils;

import com.util_code.utils.ToastUtils;
import com.common.config.Constant;
import com.common.event.LoginInvalideEvent;
import com.common.retrofit.json.Data;
import com.google.gson.JsonIOException;
import com.google.gson.JsonParseException;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import retrofit2.HttpException;

public class RxObserver<T> implements Observer<T> {

    @Override
    public void onSubscribe(@NonNull Disposable d) {

    }

    @Override
    public void onNext(@NonNull T t) {

    }

    @Override
    public void onError(@NonNull Throwable e) {
        String msg = "";
        if (e instanceof ConnectException) {
            msg = "网络不可用";
        } else if (e instanceof UnknownHostException) {
            msg = "网络不可用";
        } else if (e instanceof SocketTimeoutException) {
            msg = "请求网络超时";
        } else if (e instanceof HttpException) {
            HttpException httpException = (HttpException) e;
            msg = convertStatusCode(httpException);
        } else if (e instanceof JsonParseException || e instanceof ParseException || e instanceof JSONException || e instanceof JsonIOException) {
            msg = "数据解析错误";
        }

        if (!msg.isEmpty()) {
            ToastUtils.showShortToastSafe(msg);
        }
    }

    @Override
    public void onComplete() {

    }

    private String convertStatusCode(HttpException httpException) {
        String msg;
        if (httpException.code() == 500) {
            msg = "服务器发生错误";
        } else if (httpException.code() == 404) {
            msg = "请求地址不存在";
        } else if (httpException.code() == 403) {
            msg = "请求被服务器拒绝";
        } else if (httpException.code() == 307) {
            msg = "请求被重定向到其他页面";
        } else {
            msg = httpException.message();
        }
        return msg;
    }

    public static <T> boolean checkJsonCode(T t, boolean showToast) {

        if (t == null) {
            return false;
        }

        if (((Data) t).success) {
            return true;
        } else if (((Data) t).result_code == 200) {
            return true;
        }

        if (showToast && !TextUtils.isEmpty(((Data) t).message)) {
            ToastUtils.showShortToast(((Data) t).message);
        }
        return false;
    }

//    public static <T> boolean checkJsonCode(T t, boolean showToast) {
//
//        if (t == null) {
//            return false;
//        }
//        if (((Data) t).getStatus().endsWith(Constant.ERROR_LOGOUT)) {
//            EventBus.getDefault().post(new LoginInvalideEvent());
//            return false;
//        }
//        if (t instanceof Data) {
//            if (((Data) t).getStatus().startsWith("2")) {
//                return true;
//            } else {
//                if (showToast) {
//                    ToastUtils.showShortToast(((Data) t).getMessage());
//                }
//            }
//        }
//
//        return false;
//    }
}
