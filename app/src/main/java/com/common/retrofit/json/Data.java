package com.common.retrofit.json;

import androidx.annotation.Keep;

import java.util.Arrays;

@Keep
public class Data<T> {

    public boolean success;
    public String message;
    public T data;
    public String code;
    public String[] i18nArgs;
    public int result_code = 0;
    public String result_message;
    public String sub_code;

    @Override
    public String toString() {
        return "Data{" +
                "success=" + success +
                ", message='" + message + '\'' +
                ", data=" + data +
                ", code='" + code + '\'' +
                ", i18nArgs=" + Arrays.toString(i18nArgs) +
                ", result_code=" + result_code +
                ", result_message='" + result_message + '\'' +
                ", sub_code='" + sub_code + '\'' +
                '}';
    }
}
