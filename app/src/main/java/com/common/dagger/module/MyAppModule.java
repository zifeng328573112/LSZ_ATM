package com.common.dagger.module;

import com.common.config.PayApi;
import com.common.dagger.qualifier.PayUrl;
import com.common.retrofit.PayOkHttpHelper;
import com.util_code.dagger.help.OkhttpHelper;
import com.util_code.dagger.help.RetrofitHelper;
import com.common.config.Api;
import com.common.config.MyApi;
import com.common.config.OtherApi;
import com.common.retrofit.MyOkhttpHelper;
import com.common.dagger.qualifier.MyUrl;
import com.common.dagger.qualifier.OtherUrl;
import com.common.dagger.scope.MyAppScope;
import com.util_code.utils.NetUtils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;

@Module
public class MyAppModule {

    @MyAppScope
    @Provides
    @PayUrl
    Retrofit providePayUrlRetrofit(PayOkHttpHelper okhttpHelper, RetrofitHelper retrofitHelper) {
        return createPayApiRetrofit(okhttpHelper, retrofitHelper, Api.PAY_URL);
    }

    @MyAppScope
    @Provides
    @MyUrl
    Retrofit provideMyUrlRetrofit(MyOkhttpHelper okhttpHelper, RetrofitHelper retrofitHelper) {
        return createMyApiRetrofit(okhttpHelper, retrofitHelper, Api.BASE_URL);
    }



    @MyAppScope
    @Provides
    @OtherUrl
    Retrofit provideOtherUrlRetrofit(OkhttpHelper okhttpHelper, RetrofitHelper retrofitHelper) {
        return createOtherApiRetrofit(okhttpHelper, retrofitHelper, Api.OTHER_URL);
    }

    private Retrofit createMyApiRetrofit(MyOkhttpHelper okhttpHelper, RetrofitHelper retrofitHelper, String url) {
        Retrofit.Builder builder = retrofitHelper.getRetrofit().newBuilder();
        OkHttpClient.Builder client = okhttpHelper.getHttpClient().newBuilder();
        return builder.baseUrl(url).client(client.build()).build();
    }

    private Retrofit createPayApiRetrofit(PayOkHttpHelper okhttpHelper, RetrofitHelper retrofitHelper, String url) {
        Retrofit.Builder builder = retrofitHelper.getRetrofit().newBuilder();
        OkHttpClient.Builder client = okhttpHelper.getHttpClient().newBuilder();
        return builder.baseUrl(url).client(client.build()).build();
    }

    private Retrofit createOtherApiRetrofit(OkhttpHelper okhttpHelper, RetrofitHelper retrofitHelper, String url) {
        Retrofit.Builder retrofitBuilder = retrofitHelper.getRetrofit().newBuilder();
        OkHttpClient.Builder clientBuilder = okhttpHelper.getHttpClient().newBuilder();

        Interceptor cacheInterceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                if (!NetUtils.isConnected()) {
                    request = request.newBuilder()
                            .cacheControl(CacheControl.FORCE_CACHE)
                            .build();
                }
                Response response = chain.proceed(request);
                if (NetUtils.isConnected()) {
                    int maxAge = 0;
                    response.newBuilder()
                            .header("Cache-Control", "public, max-age=" + maxAge)
                            .removeHeader("Pragma")
                            .build();
                } else {
                    int maxStale = 60 * 60 * 24 * 7;
                    response.newBuilder()
                            .header("Cache-Control", "public, only-if-cached, max-stale=" + maxStale)
                            .removeHeader("Pragma")
                            .build();
                }
                return response;
            }
        };
        clientBuilder.addNetworkInterceptor(cacheInterceptor);
        clientBuilder.addInterceptor(cacheInterceptor);

        clientBuilder.connectTimeout(10, TimeUnit.SECONDS);
        clientBuilder.readTimeout(20, TimeUnit.SECONDS);
        clientBuilder.writeTimeout(20, TimeUnit.SECONDS);

        clientBuilder.retryOnConnectionFailure(true);

        return retrofitBuilder.baseUrl(url).client(clientBuilder.build()).build();
    }

    @MyAppScope
    @Provides
    MyApi provideMyApi(@MyUrl Retrofit retrofit) {
        return retrofit.create(MyApi.class);
    }

    @MyAppScope
    @Provides
    PayApi providePayApi(@MyUrl Retrofit retrofit) {
        return retrofit.create(PayApi.class);
    }

    @MyAppScope
    @Provides
    OtherApi provideOtherApi(@OtherUrl Retrofit retrofit) {
        return retrofit.create(OtherApi.class);
    }

}
