package com.common.dagger.component;

import android.app.Activity;

import com.lsz.atm.ui.MainActivity;
import com.lsz.atm.ui.commodity.CommodityDetailActivity;
import com.lsz.atm.ui.login.LoginActivity;
import com.lsz.atm.ui.login.StartOrderActivity;
import com.lsz.atm.ui.menu.MenuListActivity;
import com.lsz.atm.ui.order.OrderListActivity;
import com.lsz.atm.ui.pay.PayActivity;
import com.lsz.atm.ui.pay.PaySuccessActivity;
import com.util_code.dagger.module.ActivityModule;
import com.util_code.dagger.scope.ActivityScope;

import dagger.Component;

@ActivityScope
@Component(dependencies = MyAppComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {
    Activity getActivity();

    void inject(MainActivity mainActivity);

    void inject(LoginActivity loginActivity);

    void inject(StartOrderActivity startOrderActivity);

    void inject(MenuListActivity menuListActivity);

    void inject(OrderListActivity orderListActivity);

    void inject(PayActivity payActivity);

    void inject(PaySuccessActivity paySuccessActivity);

    void inject(CommodityDetailActivity commodityDetailActivity);

}
