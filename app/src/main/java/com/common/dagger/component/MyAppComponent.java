package com.common.dagger.component;

import com.common.config.PayApi;
import com.util_code.dagger.component.AppComponent;
import com.common.config.MyApi;
import com.common.config.OtherApi;
import com.common.dagger.module.MyAppModule;
import com.common.dagger.scope.MyAppScope;

import dagger.Component;

@MyAppScope
@Component(dependencies = AppComponent.class, modules = MyAppModule.class
        /*modules = {MyAppModule.class, AppModule.class}*/)
public interface MyAppComponent {

    PayApi retrofitPayApiHelper();

    MyApi retrofitMyApiHelper();

    OtherApi retrofitOtherApiHelper();
}
