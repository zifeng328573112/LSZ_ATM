package com.common.dagger.component;

import android.app.Activity;

import com.util_code.dagger.module.FragmentModule;
import com.util_code.dagger.scope.FragmentScope;

import dagger.Component;

@FragmentScope
@Component(dependencies = MyAppComponent.class, modules = FragmentModule.class)
public interface FragmentComponent {

    Activity getActivity();

    // void inject(Test1AppFragment test1appfragment);
    // void inject(Test2AppFragment test2appfragment);
}
