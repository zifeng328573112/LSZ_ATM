package com.common.thirdparty.aiyin;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import com.android_print_sdk.PrinterType;
import com.android_print_sdk.usb.USBPrinter;
import com.common.thirdparty.PrintBuilder;
import com.lsz.atm.R;
import com.util_code.utils.ImageUtils;
import com.util_code.utils.ToastUtils;

import java.util.List;

public class AiYinParty {
    private final static String TAG = "AiYinParty";
    private static Context context;
    private static final String ACTION_USB_PERMISSION = "com.android.usb.USB_PERMISSION";
    private static PendingIntent pendingIntent;
    private static USBPrinter mPrinter;

    public static void init(Context context) {
        AiYinParty.context = context.getApplicationContext();
    }

    public static void initPrinter() {
        mPrinter = new USBPrinter(context);
        mPrinter.setCurrentPrintType(PrinterType.Printer_80);
        mPrinter.setEncoding("GBK");
        mPrinter.getDeviceList(mHandler);

        pendingIntent = PendingIntent.getBroadcast(context, 0, new Intent(ACTION_USB_PERMISSION), 0);
        IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        context.registerReceiver(mUsbReceiver, filter);
    }

    static Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            switch (message.what) {
                case USBPrinter.Handler_Get_Device_List_Found:
                    UsbDevice device = (UsbDevice) message.obj;
                    Log.e(TAG, "device----" + device.toString());
                    UsbManager mUsbManager = (UsbManager) context.getSystemService(context.USB_SERVICE);
                    mUsbManager.requestPermission(device, pendingIntent); // 该代码执行后，系统弹出一个对话框
                    if (mUsbManager.hasPermission(device)) {
                        Log.d(TAG, "initPrinter() has permission and conn.");
//                        if (mPrinter.openConnection(device)) {
//                            ToastUtils.showLongToast(context.getString(R.string.activity_main_connecterr));
//                        } else {
//                            ToastUtils.showLongToast(context.getString(R.string.activity_main_connected));
//                        }
                    } else {
                        Log.d(TAG, "initPrinter() requestPermission");
                        // 没有权限询问用户是否授予权限
                        mUsbManager.requestPermission(device, pendingIntent); // 该代码执行后，系统弹出一个对话框
                    }
                    break;
                case USBPrinter.Handler_Get_Device_List_No_Device:
                case USBPrinter.Handler_Get_Device_List_Completed:
                case USBPrinter.Handler_Get_Device_List_Error:
                    break;

                default:
                    break;
            }
            return false;
        }
    });

    private static final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.e(TAG, action);
//            Toast.makeText(context, action, Toast.LENGTH_SHORT).show();
            if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                    UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        Log.e(TAG, "获取权限成功：" + device.getDeviceName());
                        if (mPrinter.openConnection(device)) {
                            ToastUtils.showLongToast(context.getString(R.string.activity_main_connected));
                        } else {
                            ToastUtils.showLongToast(context.getString(R.string.activity_main_connecterr));
                        }
                    } else {
                        Log.d(TAG, "permission denied for device " + device);
                    }
                }
            } else if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
                UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (device.getProductId() == 22336 && device.getVendorId() == 1155) {
                    UsbManager mUsbManager = (UsbManager) context.getSystemService(context.USB_SERVICE);
                    mUsbManager.requestPermission(device, pendingIntent); // 该代码执行后，系统弹出一个对话框
                }
            } else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (device != null && device.equals(mPrinter.getCurrentDevice())) {
                    mPrinter.closeConnection();
                }
            }
        }
    };

    public static void checkIsOpened() {
        if (!mPrinter.isConnected()) {
            initPrinter();
        }
    }

    public static void onStartParty(List<PrintBuilder> printList) {
        mPrinter.init();//初始化
        for (PrintBuilder print : printList) {
            if (!TextUtils.isEmpty(print.sText)) {
                onTextPrint(print.sText, print.iLeftMargin, print.iAlignment, print.textHeigh, print.textWidth);
            }
            if (!TextUtils.isEmpty(print.qrCodeData)) {
                onQRCode(print.qrCodeData);
            }
            if (print.drawable != null) {
                onImagePrint(print.drawable);
            }
        }
        //打印换行
        mPrinter.setPrinter(USBPrinter.COMM_PRINT_AND_NEWLINE);
        mPrinter.cutPaper();  //切纸
    }

    /**
     * 文本打印
     *
     * @param sText       文本数据
     * @param iLeftMargin 左边距
     * @param iAlignment  0：左 1：中 2：右
     */
    public static void onTextPrint(String sText, int iLeftMargin, int iAlignment, int heigh, int width) {
        mPrinter.setPrinter(USBPrinter.COMM_ALIGN, iAlignment);
        mPrinter.setCharacterMultiple(width, heigh);
        mPrinter.setLeftMargin(iLeftMargin, 0);
        mPrinter.printText(sText);
    }

    /**
     * 图片打印
     *
     * @param drawable
     */
    public static void onImagePrint(Drawable drawable) {
        Bitmap grayBitmap = BitmapConvertor.bitmap2Gray(ImageUtils.drawable2Bitmap(drawable));
        Bitmap convertToBMW = BitmapConvertor.convertToBMW(grayBitmap, 0, 0, 85);
        mPrinter.printImage(convertToBMW);
    }

    /**
     * 二维码
     * <p>
     * param1：表示每行字符数，1<=n<=30。
     * param2：表示纠错等级，0<=n<=8。
     * param3：表示纵向放大倍数。
     *
     * @param qrCodeData
     */
    public static void onQRCode(String qrCodeData) {
        mPrinter.setPrinter(USBPrinter.COMM_ALIGN, USBPrinter.COMM_ALIGN_CENTER);
        mPrinter.setBarCode(2, 3, 8, USBPrinter.BAR_CODE_TYPE_QRCODE);
        mPrinter.printBarCode(qrCodeData);//mPrinter 实例化的打印机实体类，且打印机已经连接
    }

}
