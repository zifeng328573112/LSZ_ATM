package com.common.thirdparty.sdkUtil;

import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.http.POST;
import timber.log.Timber;

/**
 * 检测usb设备的方法
 *
 * @author yiuhet
 * @date:2019/11/31
 **/
public class UsbDeviceUtils {

    /**
     * 小票打印机 对应的VID、PID
     */
    private static final int PRINTER_VID = 1155;
    private static final List<Integer> PRINTER_PID_LISTS = Arrays.asList(22304);


    /**
     * 标签打印机 对应的VID、PID
     */
    private static final int LABEL_PRINTER_VID = 8401;
    private static final List<Integer> LABEL_PRINTER_PID_LISTS = Arrays.asList(28680);

    /**
     * 获取打印机（第一个）
     *
     * @param context
     * @return
     */
    public static UsbDevice getPrintDevice(Context context) {
        UsbManager manager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
        assert manager != null;
        HashMap<String, UsbDevice> deviceList = manager.getDeviceList();
        for (UsbDevice usbDevice : deviceList.values()) {
            Timber.e("PID = " + usbDevice.getProductId() + "VID = " + usbDevice.getVendorId());
//            Timber.e("HEX : PID = " + numToHex16(usbDevice.getProductId()) + " VID = " + numToHex16(usbDevice.getVendorId()));
            if (PRINTER_VID == usbDevice.getVendorId()
                    && PRINTER_PID_LISTS.contains(usbDevice.getProductId())) {
                Timber.e("find Printer ： PID = " + usbDevice.getProductId() + "VID = " + usbDevice.getVendorId());
                return usbDevice;
            }
        }
        return null;
    }

    /* 获取标签打印机（第一个）
     *
     * @param context
     * @return
     */
    public static UsbDevice getLabelPrintDevice(Context context) {
        UsbManager manager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
        assert manager != null;
        HashMap<String, UsbDevice> deviceList = manager.getDeviceList();
        for (UsbDevice usbDevice : deviceList.values()) {
            Timber.e("PID = " + usbDevice.getProductId() + "VID = " + usbDevice.getVendorId());
//            Timber.e("HEX : PID = " + numToHex16(usbDevice.getProductId()) + " VID = " + numToHex16(usbDevice.getVendorId()));
            if (LABEL_PRINTER_VID == usbDevice.getVendorId()
                    && LABEL_PRINTER_PID_LISTS.contains(usbDevice.getProductId())) {
                Timber.e("find LabelPrinter ： PID = " + usbDevice.getProductId() + "VID = " + usbDevice.getVendorId());
                return usbDevice;
            }
        }
        return null;
    }

    public static ArrayList<UsbDevice> getAllUsbDevice(Context context) {
        ArrayList<UsbDevice> result = new ArrayList<>();
        UsbManager manager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
        assert manager != null;
        HashMap<String, UsbDevice> deviceList = manager.getDeviceList();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            deviceList.entrySet()
                    .stream()
                    .map(Map.Entry::getValue)
                    .forEach(usbDevice -> {
                                Timber.e("PID = " + usbDevice.getProductId() + " VID = " + usbDevice.getVendorId());
                                Timber.e("HEX : PID = " + numToHex16(usbDevice.getProductId()) + " VID = " + numToHex16(usbDevice.getVendorId()));
                                result.add(usbDevice);
                            }
                    );

            Timber.e(" 匹配的devices 的数量 " + result.size());
        } else {
            for (UsbDevice usbDevice : deviceList.values()) {
                Timber.e("PID = " + usbDevice.getProductId() + "VID = " + usbDevice.getVendorId());
                Timber.e("HEX : PID = " + numToHex16(usbDevice.getProductId()) + " VID = " + numToHex16(usbDevice.getVendorId()));
                result.add(usbDevice);
            }
        }
        return result;
    }

    public static ArrayList<DeviceBean> getUsbDeviceList(Context context) {
        ArrayList<DeviceBean> devicesList = new ArrayList<>();
        UsbManager manager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
        assert manager != null;
        HashMap<String, UsbDevice> deviceList = manager.getDeviceList();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            deviceList.entrySet()
                    .stream()
                    .map(Map.Entry::getValue)
                    .forEach(usbDevice -> {
                                Timber.e("PID = " + usbDevice.getProductId() + " VID = " + usbDevice.getVendorId());
                                Timber.e("HEX : PID = " + numToHex16(usbDevice.getProductId()) + " VID = " + numToHex16(usbDevice.getVendorId()));
                                devicesList.add(new DeviceBean(numToHex16(usbDevice.getVendorId()), numToHex16(usbDevice.getProductId())));
                            }
                    );

            Timber.e(" 匹配的devices 的数量 " + devicesList.size());
        } else {
            for (UsbDevice usbDevice : deviceList.values()) {
                Timber.e("PID = " + usbDevice.getProductId() + "VID = " + usbDevice.getVendorId());
                Timber.e("HEX : PID = " + numToHex16(usbDevice.getProductId()) + " VID = " + numToHex16(usbDevice.getVendorId()));
                devicesList.add(new DeviceBean(numToHex16(usbDevice.getVendorId()), numToHex16(usbDevice.getProductId())));
            }
        }
        if (devicesList.size() == 0) {
            devicesList.add(new DeviceBean("", ""));
        }
        return devicesList;
    }

    public static class DeviceBean {
        public String vid;
        public String pid;

        public DeviceBean(String vid, String pid) {
            this.vid = vid;
            this.pid = pid;
        }
    }

    public static String numToHex16(int b) {
        return "0x" + String.format("%04X", b);
    }
}
