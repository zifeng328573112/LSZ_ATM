package com.common.thirdparty.sdkUtil;

import android.text.TextUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 判断支付二维码类型，如支付宝，微信，银联
 *
 * @author zdy
 */

public class QRCodeTypeUtils {
    /**
     * 未知
     */
    public static final int TYPE_NO = 0x00;
    /**
     * 微信类型
     */
    public static final int TYPE_WeChat = 0x01;
    /**
     * 支付宝类型
     */
    public static final int TYPE_AliPay = 0x02;
    /**
     * 百度钱包类型
     */
    public static final int TYPE_Baidu_Wallet = 0x03;
    /**
     * 银联二维码类型
     */
    public static final int TYPE_UNION = 0x04;
    /**
     * 小米钱包二维码类型
     */
    public static final int TYPE_MiPay = 0x05;

    private static Matcher weChatTypeMather = Pattern.compile("^1[0-5]\\d{16}$").matcher("");
    private static Matcher aliPayTypeMather = Pattern.compile("^(2[5-9]|30)\\d{14,22}$").matcher("");
    private static Matcher seriesDigitMather = Pattern.compile("^\\d++$").matcher("");
    private static Matcher digitLetterMather = Pattern.compile("[a-zA-Z0-9]*").matcher("");
    private static Matcher sunmiCodeMather = Pattern.compile("^[A-F0-9]{22}$").matcher("");


    public static boolean isDigitStr(String qrCode) {//判断是否为数字字符串
        return !TextUtils.isEmpty(qrCode) && seriesDigitMather.reset(qrCode).matches();
    }

    public static boolean isSunmiCode(String qrCode) {//判断是否为sunmi订单号
        return !TextUtils.isEmpty(qrCode) && sunmiCodeMather.reset(qrCode).matches();
    }

    /**
     * 判断字符串code是否只包含数字和英文字符
     *
     * @param code
     * @return true:是；false:否
     */
    public static boolean isDigitLetterStr(String code) {
        return !TextUtils.isEmpty(code) && digitLetterMather.reset(code).matches();
    }

    /**
     * 根据付款码的特征来判断其所属的渠道
     *
     * @param qrCode
     * @return
     */
    public static int checkQRCodeType(String qrCode) {
        //只匹配全部的连续数字
        if (!isDigitStr(qrCode)) {
            return TYPE_NO;
        } else if (weChatTypeMather.reset(qrCode).matches()) {
            //微信付款码以10-15开头，长度为18位
            return TYPE_WeChat;
        } else if (aliPayTypeMather.reset(qrCode).matches()) {
            //支付宝付款码以25-30开头，长度为16-24位
            return TYPE_AliPay;
        } else if (qrCode.startsWith("31") && qrCode.length() < 19) {
            return TYPE_Baidu_Wallet;
        } else if (qrCode.startsWith("62") && qrCode.length() == 19) {
            return TYPE_UNION;
        } else if (qrCode.startsWith("18") && qrCode.length() == 18) {
            return TYPE_MiPay;
        } else {
            return TYPE_NO;
        }
    }

}
