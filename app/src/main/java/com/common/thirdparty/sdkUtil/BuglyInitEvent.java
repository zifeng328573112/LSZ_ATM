package com.common.thirdparty.sdkUtil;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import com.common.config.Constant;
import com.lsz.atm.BuildConfig;
import com.tencent.bugly.crashreport.CrashReport;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class BuglyInitEvent {

    Application application;

    public BuglyInitEvent(Application application){
        this.application = application;
    }

    public void initBugly() {
        Context context = application.getApplicationContext();
        // 获取当前包名
        String packageName = context.getPackageName();
        // 获取当前进程名
        String processName = getProcessName(android.os.Process.myPid());
        // 设置是否为上报进程
        CrashReport.UserStrategy strategy = new CrashReport.UserStrategy(context);
        strategy.setUploadProcess(processName == null || processName.equals(packageName));
        // 初始化Bugly
        if (BuildConfig.DEBUG) {
            CrashReport.initCrashReport(application.getApplicationContext(), Constant.BUGLY_APP_ID, true, strategy);
        } else {
            CrashReport.initCrashReport(application.getApplicationContext(), Constant.BUGLY_APP_ID, false, strategy);
        }
    }

    /**
     * 获取进程号对应的进程名
     *
     * @param pid 进程号
     * @return 进程名
     */
    private String getProcessName(int pid) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("/proc/" + pid + "/cmdline"));
            String processName = reader.readLine();
            if (!TextUtils.isEmpty(processName)) {
                processName = processName.trim();
            }
            return processName;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
        return null;
    }

}
