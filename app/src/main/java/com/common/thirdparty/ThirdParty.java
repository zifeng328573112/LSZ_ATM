package com.common.thirdparty;

import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;

import com.common.thirdparty.aiyin.AiYinParty;
import com.common.thirdparty.taiyun.TaiYunParty;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ThirdParty {
    public static int PRINT_TYPE = -1;


    public static void init(Context context) {
        UsbManager mUsbManager = (UsbManager) context.getSystemService(context.USB_SERVICE);
        HashMap<String, UsbDevice> deviceList = mUsbManager.getDeviceList();
        Iterator iter = deviceList.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();
            Object key = entry.getKey();
            UsbDevice usbDevice = (UsbDevice)entry.getValue();
            if (usbDevice.getProductId() == 22336 && usbDevice.getVendorId() == 1155) {
                PRINT_TYPE = PrintBuilder.PRINT_AIYIN_TYPE;
                AiYinParty.init(context);
                break;
            } else {
                PRINT_TYPE = PrintBuilder.PRINT_TAIYUN_TYPE;
                TaiYunParty.init(context);
            }
        }
        ThirdParty.initPrinter();
    }

    public static void checkIsOpened() {
        if (PRINT_TYPE == PrintBuilder.PRINT_AIYIN_TYPE) {
            AiYinParty.checkIsOpened();
        } else {
            TaiYunParty.checkIsOpened();
        }
    }

    public static void initPrinter() {
        if (PRINT_TYPE == PrintBuilder.PRINT_AIYIN_TYPE) {
            AiYinParty.initPrinter();
        } else {
            TaiYunParty.initPrinter();
        }
    }

    public static void onStartPrint(List<PrintBuilder> printList) {
        if (PRINT_TYPE == PrintBuilder.PRINT_AIYIN_TYPE) {
            AiYinParty.onStartParty(printList);
        } else {
            TaiYunParty.onStartParty(printList);
        }
    }
}
