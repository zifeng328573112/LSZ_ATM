package com.common.thirdparty;

import android.graphics.drawable.Drawable;
import android.text.TextUtils;

import com.common.config.Global;
import com.common.util.EmojiUtils;
import com.lsz.atm.json.ATMBean;
import com.lsz.atm.viewutil.SpannableUtil;

import java.util.ArrayList;
import java.util.List;

public class PrintBuilder {
    public static int PRINT_AIYIN_TYPE = 0;
    public static int PRINT_TAIYUN_TYPE = 1;

    public String qrCodeData;
    public String sText;
    public int iLeftMargin;//左边距
    public int iAlignment;//0：左 1：中 2：右
    public int iAttribute;//1, 2, 4, 8, 16, 32
    public int iTextSize = 0;//1 2 3 4 5 6 7 8
    public Drawable drawable;
    public int textHeigh;
    public int textWidth;

    public void setTextHW(int heigh, int width) {
        textHeigh = heigh;
        textWidth = width;
    }

    public void setTextSize(int heigh, int width) {
        iTextSize = heigh + width * 0x10;
    }

    public static List<PrintBuilder> getPrintList(int sdkType,
                                                  String mealNumber,
                                                  String takeDine,
                                                  String orderNumber,
                                                  String orderTime,
                                                  String qrCode,
                                                  String amountTotal,
                                                  String actualAmount,
                                                  Drawable drawable,
                                                  List<String> textContent) {
        List<PrintBuilder> arrayList = new ArrayList<>();
        PrintBuilder print = new PrintBuilder();
        print.sText = Global.getAtmBean().storeName + "\n";
        print.iLeftMargin = 0;
        print.iAlignment = 0;
        print.iAttribute = 0;
        arrayList.add(print);
        PrintBuilder print1 = new PrintBuilder();
        print1.iLeftMargin = 0;
        print1.iAlignment = 1;
        print1.iAttribute = 0;
        if (sdkType == PRINT_AIYIN_TYPE) {
            print1.sText = mealNumber + "\n";
            print1.setTextHW(2, 2);
        } else {
            print1.sText = mealNumber;
            print1.setTextSize(2, 2);
        }
        arrayList.add(print1);
        PrintBuilder print2 = new PrintBuilder();
//        print2.sText = "请凭小票到取餐处等候取餐\n\n";
        print2.sText = "请凭下方二维码到取餐处等候扫码取餐\n\n";
        print2.iLeftMargin = 0;
        print2.iAlignment = 1;
        print2.iAttribute = 0;
        arrayList.add(print2);
        PrintBuilder print3 = new PrintBuilder();
        print3.sText = TextUtils.equals(takeDine, ATMBean.PICK_UP) ? "打包带走\n\n" : "店内用餐\n\n";
        print3.iLeftMargin = 0;
        print3.iAlignment = 1;
        print3.iAttribute = 0;
        if (sdkType == PRINT_AIYIN_TYPE) {
            print3.textHeigh = 1;
        } else {
            print3.iTextSize = 1;
        }
//        print3.setTextSize(1, 1);
        arrayList.add(print3);
        PrintBuilder print4 = new PrintBuilder();
        print4.sText = "订单号：" + orderNumber + "\n";
        print4.iLeftMargin = 0;
        print4.iAlignment = 1;
        print4.iAttribute = 0;
        arrayList.add(print4);
        PrintBuilder print5 = new PrintBuilder();
        print5.sText = "下单时间：" + orderTime + "\n";
        print5.iLeftMargin = 0;
        print5.iAlignment = 1;
        print5.iAttribute = 0;
        arrayList.add(print5);
        PrintBuilder print6 = new PrintBuilder();
        print6.sText = "-----------------------------------------------" + "\n";
        print6.iLeftMargin = 0;
        print6.iAlignment = 0;
        print6.iAttribute = 0;
        arrayList.add(print6);
        PrintBuilder print7 = new PrintBuilder();
        print7.sText = "商品名称\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000\u3000数量\u3000\u3000\u3000\u3000单价" + "\n";
        print7.iLeftMargin = 0;
        print7.iAlignment = 0;
        print7.iAttribute = 0;
        arrayList.add(print7);
        for (String sText : textContent) {
            PrintBuilder print8 = new PrintBuilder();
            print8.sText = sText + "\n";
            print8.iLeftMargin = 0;
            print8.iAlignment = 0;
            print8.iAttribute = 0;
            arrayList.add(print8);
        }
        PrintBuilder print9 = new PrintBuilder();
        print9.sText = "-----------------------------------------------" + "\n";
        print9.iLeftMargin = 0;
        print9.iAlignment = 0;
        print9.iAttribute = 0;
        arrayList.add(print9);
        PrintBuilder print10 = new PrintBuilder();
        String textStyle = getTextStyle("合计", Global.getOrderNumber().orderTotalNumber, "￥" + amountTotal, "");
        print10.sText = textStyle + "\n";
        print10.iLeftMargin = 0;
        print10.iAlignment = 0;
        print10.iAttribute = 0;
        arrayList.add(print10);
        PrintBuilder print11 = new PrintBuilder();
        print11.sText = "-----------------------------------------------" + "\n";
        print11.iLeftMargin = 0;
        print11.iAlignment = 0;
        print11.iAttribute = 0;
        arrayList.add(print11);
        PrintBuilder print12 = new PrintBuilder();
        print12.sText = "实付：￥" + actualAmount + "\n";
        print12.iLeftMargin = 0;
        print12.iAlignment = 0;
        print12.iAttribute = 0;
        if (sdkType == PRINT_AIYIN_TYPE) {
            print12.textHeigh = 1;
        } else {
            print12.iTextSize = 1;
        }
        arrayList.add(print12);
        PrintBuilder print13 = new PrintBuilder();
        if (sdkType == PRINT_AIYIN_TYPE) {
            print13.sText = "-----------------------------------------------" + "\n\n";
        } else {
            print13.sText = "-----------------------------------------------" + "\n\n\n\n";
        }
        print13.iLeftMargin = 0;
        print13.iAlignment = 0;
        print13.iAttribute = 0;
        arrayList.add(print13);
        PrintBuilder print14 = new PrintBuilder();
        print14.qrCodeData = qrCode;
        arrayList.add(print14);
        if (sdkType == PRINT_AIYIN_TYPE) {
        } else {
            PrintBuilder print15 = new PrintBuilder();
            print15.sText = "\n\n";
            arrayList.add(print15);
        }
        PrintBuilder print16 = new PrintBuilder();
        if (sdkType == PRINT_AIYIN_TYPE) {
            print16.sText = "关注粮手抓公众号，获取新品和优惠信息" + "\n";
        } else {
            print16.sText = "关注粮手抓公众号，获取新品和优惠信息" + "\n\n\n";
        }
        print16.iLeftMargin = 0;
        print16.iAlignment = 1;
        print16.iAttribute = 0;
        arrayList.add(print16);
        PrintBuilder print17 = new PrintBuilder();
        print17.drawable = drawable;
        arrayList.add(print17);
        return arrayList;
    }

    private static int v1 = 14;
    private static int v2 = 5;
    private static String kg = "\u3000";

    public static String getTextStyle(String value1, String value2, String value3, String value4) {
        if (TextUtils.isEmpty(value1)) {
            return "";
        }
        boolean isWrap = false;
        value1 = EmojiUtils.emojiChange(value1);
        String textWrap = value1;
        StringBuffer stringBuffer = new StringBuffer();
        int value1Size = value1.length();
        if (value1Size > 12) {
            isWrap = true;
            textWrap = textWrap.substring(12, value1Size);
            value1 = value1.substring(0, 12);
            stringBuffer.append(value1 + kg + kg);
        } else if (value1Size <= 12) {
            stringBuffer.append(value1);
            int iKg = v1 - value1Size;
            for (int i = 0; i < iKg; i++) {
                stringBuffer.append(kg);
            }
        }
        int value2Size = value2.length();
        if (value2Size > 3) {
            value2 = SpannableUtil.addSpace(value2, "\n" + kg + kg, 3);
            stringBuffer.append(kg + value2);
        } else if (value2Size <= 3) {
            stringBuffer.append(kg + value2);
            int jKg = v2 - value2Size;
            for (int i = 0; i < jKg; i++) {
                stringBuffer.append(kg);
            }
        }
        if (isWrap) {
            stringBuffer.append(value3 + "\n" + textWrap);
        } else {
            stringBuffer.append(value3);
        }
        if (!TextUtils.isEmpty(value4)) {
            stringBuffer.append("\n" + value4);
        }
        return stringBuffer.toString();
    }
}