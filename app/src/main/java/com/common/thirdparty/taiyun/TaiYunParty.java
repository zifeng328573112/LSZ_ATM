package com.common.thirdparty.taiyun;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.text.TextUtils;
import android.util.Log;

import com.common.thirdparty.PrintBuilder;
import com.common.thirdparty.ThirdParty;
import com.facebook.stetho.common.LogUtil;
import com.lsz.atm.R;
import com.util_code.utils.ImageUtils;
import com.util_code.utils.ToastUtils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import print.Print;

public class TaiYunParty {

    private static Context context;

    private static final String ACTION_USB_PERMISSION = "com.PRINTSDKSample";
    private static String[] PERMISSIONS_STORAGE = {"android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"};
    private static PendingIntent mPermissionIntent = null;
    private static UsbDevice device;

    public static void init(Context context) {
        TaiYunParty.context = context.getApplicationContext();
    }

    public static void checkIsOpened() {
        if (!Print.IsOpened()) {
            TaiYunParty.initPrinter();
        }
    }

    /**
     * 初始化打印机(注册usb设备监听、扫描usb设备并连接打印设备)
     */
    public static void initPrinter() {
        mPermissionIntent = PendingIntent.getBroadcast(context, 0, new Intent(ACTION_USB_PERMISSION), 0);
        IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        context.registerReceiver(mUsbReceiver, filter);
        UsbManager mUsbManager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
        HashMap<String, UsbDevice> deviceList = mUsbManager.getDeviceList();
        Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();
        boolean havePrinter = false;
        while (deviceIterator.hasNext()) {
            device = deviceIterator.next();
            int count = device.getInterfaceCount();
            for (int i = 0; i < count; i++) {
                UsbInterface intf = device.getInterface(i);
                if (intf.getInterfaceClass() == 7) {
                    havePrinter = true;
                    Log.d("PRINT_TAG", "vendorID--" + device.getVendorId() + "ProductId--" + device.getProductId());
                    mUsbManager.requestPermission(device, mPermissionIntent);
                }
            }
        }
        if (!havePrinter) {
            ToastUtils.showLongToast(context.getString(R.string.activity_main_connect_usb_printer));
        }
    }

    /**
     * usb设备状态监听广播（获取到usb权限、插入usb设备、拔出usb设备）
     */
    private static final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            try {
                String action = intent.getAction();
                if (ACTION_USB_PERMISSION.equals(action)) {
                    synchronized (this) {
                        device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                        if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                            if (Print.PortOpen(context, device) != 0) {
                                ToastUtils.showLongToast(context.getString(R.string.activity_main_connecterr));
                                return;
                            } else {
                                ToastUtils.showLongToast(context.getString(R.string.activity_main_connected));
                            }
                        } else {
                            return;
                        }
                    }
                }
                if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                    device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (device != null) {
                        int count = device.getInterfaceCount();
                        for (int i = 0; i < count; i++) {
                            UsbInterface intf = device.getInterface(i);
                            //Class ID 7代表打印机
                            if (intf.getInterfaceClass() == 7) {
                                Print.PortClose();
                                ToastUtils.showLongToast(R.string.activity_main_tips);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                LogUtil.e((new StringBuilder("Activity_Main --> mUsbReceiver ")).append(e.getMessage()).toString());
            }
        }
    };

    public static void onStartParty(List<PrintBuilder> printList) {
        try {
            Print.Initialize();//初始化
            for (PrintBuilder print : printList) {
                if (!TextUtils.isEmpty(print.sText)) {
                    onTextPrint(print.sText, print.iLeftMargin, print.iAlignment, print.iAttribute, print.iTextSize);
                }
                if (!TextUtils.isEmpty(print.qrCodeData)) {
                    onQRCode(print.qrCodeData);
                }
                if (print.drawable != null) {
                    onImagePrint(print.drawable);
                }
            }
            Print.PrintDataInPageMode();//打印
            onPartialCut();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static int QRCodeSize = 8;
    private static int QRCodeModel = 0;
    private static int QRCodeLevel = 0;//0:L 1:M 2:Q 3:H
    private static int justification = 1;//0:左 1：中 2：右

    /**
     * 二维码
     *
     * @param qrCodeData
     */
    public static void onQRCode(String qrCodeData) {
        try {
            PublicAction PAct = new PublicAction(context);
            PAct.BeforePrintAction();
            Print.PrintQRCode(qrCodeData, (QRCodeSize + 1), (QRCodeLevel + 0x30), justification);
            PAct.AfterPrintAction();
        } catch (Exception e) {
            Log.d("HPRTSDKSample", (new StringBuilder("Activity_QRCode --> onClickPrint ")).append(e.getMessage()).toString());
        }
    }

    /**
     * 文本打印
     *
     * @param sText       文本数据
     * @param iLeftMargin 左边距
     * @param iAlignment  0：左 1：中 2：右
     * @param iAttribute
     * @param iTextSize
     */
    public static void onTextPrint(String sText, int iLeftMargin, int iAlignment, int iAttribute, int iTextSize) {
        try {
            PublicAction PAct = new PublicAction(context);
//	    	PAct.LanguageEncode();
            PAct.BeforePrintAction();
            Print.SetLeftMargin(iLeftMargin);
            Print.PrintText(sText, iAlignment, iAttribute, iTextSize);
//	    	byte[] data1=new byte[]{0x1D,0x0C};
//			Print.WriteData(data1);
            PAct.AfterPrintAction();
        } catch (Exception e) {
            Log.d("HPRTSDKSample", (new StringBuilder("Activity_TextFormat --> onClickPrint ")).append(e.getMessage()).toString());
        }
    }

    public static void onImagePrint(Drawable drawable) throws Exception {
        Bitmap drawable2Bitmap = ImageUtils.drawable2Bitmap(drawable);
        byte halftoneType = (byte) 0;
        byte scaleMode = (byte) 0;
        int printdpi = 203;
        Print.PrintPageRectangle(0, 0, 624, 2000, 2);//矩形框（起始x，起始Y，终点x，终点Y，线条宽度）
        Print.SetPageModeAbsolutePosition(300, 500);
        Print.PrintBitmap(drawable2Bitmap, halftoneType, scaleMode, printdpi);
    }

    /**
     * 切纸
     */
    public static void onPartialCut() {
        try {
            Print.CutPaper(1, 8);
        } catch (Exception e) {

        }
    }

}
