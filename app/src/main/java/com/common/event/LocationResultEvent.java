package com.common.event;


import androidx.annotation.Keep;

@Keep
public class LocationResultEvent {

    boolean mbResult;

    public LocationResultEvent(boolean bresult) {
        this.mbResult = bresult;
    }

    public boolean isMbResult() {
        return mbResult;
    }

    public void setMbResult(boolean mbResult) {
        this.mbResult = mbResult;
    }
}