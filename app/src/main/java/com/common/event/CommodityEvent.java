package com.common.event;


import androidx.annotation.Keep;

@Keep
public class CommodityEvent {

    public boolean isOrder = false;

    public CommodityEvent() {
        super();
    }

    public CommodityEvent(boolean isOrder) {
        this.isOrder = isOrder;
    }
}
