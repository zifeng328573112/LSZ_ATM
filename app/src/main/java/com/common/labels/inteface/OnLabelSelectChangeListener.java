package com.common.labels.inteface;

import android.widget.TextView;

import com.common.labels.LabelsView;

public interface OnLabelSelectChangeListener {

        /**
         * @param label    标签
         * @param data     标签对应的数据
         * @param isSelect 是否选中
         * @param position 标签位置
         */
        void onLabelSelectChange(LabelsView labelsView, TextView label, Object data, boolean isSelect, int position);
    }