package com.common.labels.inteface;

import android.widget.TextView;

public interface OnLabelClickListener {

        /**
         * @param label    标签
         * @param data     标签对应的数据
         * @param position 标签位置
         */
        void onLabelClick(TextView label, Object data, int position);
    }
