package com.common.labels.inteface;

import android.widget.TextView;

/**
     * 点击选中/取消选中时，拦截事件，返回true时，表示事件被拦截，不会改变标签的选中状态。
     * 当希望某个标签在特定条件下不被选中/取消选中时，可以使用事件拦截。
     * 只有用户点击改变标签选中状态时才会回调拦截，用其他方法改变时不会回调这个方法，不会被拦截。
     */
    public interface OnSelectChangeIntercept {

        /**
         * @param label     标签
         * @param data      标签对应的数据
         * @param oldSelect 旧选中状态
         * @param newSelect 新选中状态
         * @param position  标签位置
         */
        boolean onIntercept(TextView label, Object data, boolean oldSelect, boolean newSelect, int position);
    }