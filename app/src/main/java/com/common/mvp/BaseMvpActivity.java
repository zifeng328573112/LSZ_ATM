package com.common.mvp;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.WindowManager;

import androidx.annotation.NonNull;

import com.common.UIApplication;
import com.common.dagger.component.ActivityComponent;
import com.common.dagger.component.DaggerActivityComponent;
import com.common.dagger.component.MyAppComponent;
import com.util_code.base.BaseApplication;
import com.util_code.base.BaseDoubleClickExitHelper;
import com.util_code.base.mvp.MvpActivity;
import com.util_code.base.mvp.MvpPresenter;
import com.util_code.base.mvp.MvpView;
import com.util_code.dagger.module.ActivityModule;
import com.util_code.statusbar.StatusTopBarUtil;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseMvpActivity<V extends MvpView, P extends MvpPresenter<V>> extends MvpActivity<V, P> {

    private Unbinder mUnbinder;
    public static BaseDoubleClickExitHelper mDoubleClickExitHelper;
    private ActivityComponent mActivityComponent;

    /**
     * Instantiate a presenter instance
     *
     * @return The {@link MvpPresenter} for this view
     */
    @NonNull
    public P createPresenter() {
        setupComponent(UIApplication.getMyAppComponent());
        initInject();
        return presenter;
    }

    protected void setupComponent(MyAppComponent applicationComponent) {
        mActivityComponent = DaggerActivityComponent.builder()
                .myAppComponent(applicationComponent)
                .activityModule(getActivityModule())
                .build();
    }

    protected ActivityModule getActivityModule() {
        return new ActivityModule(this);
    }

    public ActivityComponent getActivityComponent() {
        return mActivityComponent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        mUnbinder = ButterKnife.bind(this);
        setup(savedInstanceState);
        BaseApplication.getAppManager().addActivity(this);
        mDoubleClickExitHelper = new BaseDoubleClickExitHelper(this);
        setTranslateStatusBar(true);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE);
    }

    private void setup(Bundle savedInstanceState) {
        setupView();
        setupData(savedInstanceState);
    }

    protected abstract int getLayoutId();

    protected abstract void initInject();

    protected abstract void setupView();

    protected abstract void setupData(Bundle savedInstanceState);

    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    protected void setTranslateStatusBar(boolean barColor) {
//        StatusBarHelper.translateStatusBar(BaseMvpActivity.this);
        //当FitsSystemWindows设置 true 时，会在屏幕最上方预留出状态栏高度的 padding
        StatusTopBarUtil.setRootViewFitsSystemWindows(BaseMvpActivity.this, barColor);
        //设置状态栏透明
        StatusTopBarUtil.setTranslucentStatus(this);
        if (barColor) {
            //一般的手机的状态栏文字和图标都是白色的, 可如果你的应用也是纯白色的, 或导致状态栏文字看不清
            //所以如果你是这种情况,请使用以下代码, 设置状态使用深色文字图标风格, 否则你可以选择性注释掉这个if内容
            if (!StatusTopBarUtil.setStatusBarDarkTheme(BaseMvpActivity.this, true)) {
                //如果不支持设置深色风格 为了兼容总不能让状态栏白白的看不清, 于是设置一个状态栏颜色为半透明,
                //这样半透明+白=灰, 状态栏的文字能看得清
                StatusTopBarUtil.setStatusBarColor(BaseMvpActivity.this, 0x55000000);
            }
        }
    }

    public void accessNextPage(Class<?> name) {
        Intent intent = new Intent(this, name);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    public void accessNextPage(Class<?> name, Bundle extras) {
        Intent intent = new Intent(this, name);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtras(extras);
        startActivity(intent);
    }

    public void accessNextPageForResult(Class<?> name, int requestCode) {
        Intent intent = new Intent(this, name);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, requestCode);
    }

    public void accessNextPageForResult(Class<?> name, int requestCode, Bundle extras) {
        Intent intent = new Intent(this, name);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtras(extras);
        startActivityForResult(intent, requestCode);
    }

    /*@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                //return mDoubleClickExitHelper.onKeyDown(keyCode, event);
                break;

            default:
                break;
        }
        return super.onKeyDown(keyCode, event);
    }*/

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        switch (event.getAction()) {
            case KeyEvent.ACTION_DOWN:
                if (event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_UP) {
                    return true;
                }
                if (event.getKeyCode() == KeyEvent.KEYCODE_VOLUME_DOWN) {
                    return true;
                }
                if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                    return true;
                }
                if (event.getKeyCode() == KeyEvent.KEYCODE_MENU) {
                    return true;
                }
                if (event.getKeyCode() == KeyEvent.KEYCODE_HOME) {
                    return true;
                }
                if (event.getKeyCode() == KeyEvent.KEYCODE_POWER) {
                    return true;
                }
                if (event.getKeyCode() == KeyEvent.KEYCODE_NUMPAD_ENTER) {

                    return true;
                }
            default:
                return true;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
        BaseApplication.getAppManager().removeActivity(this);
    }
}
