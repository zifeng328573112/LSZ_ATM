package com.common.mvp;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.util_code.base.mvp.MvpPresenter;
import com.util_code.base.mvp.MvpView;

public abstract class BaseLceMvpFragment<V extends MvpView, P extends MvpPresenter<V>> extends BaseMvpFragment<V, P> {

    protected View contentView;
    protected View loadingView;
    protected View emptyView;
    protected View errorView;

    @CallSuper
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        contentView = createContentView(view);
        loadingView = createLoadingView(view);
        emptyView = createEmptyView(view);
        errorView = createErrorView(view);

        if (loadingView == null) {
            throw new NullPointerException("Loading view is null! Have you specified a loading view in your layout xml file?" + " You have to give your loading View the id R.id.loadingView");
        }

        if (contentView == null) {
            throw new NullPointerException("Content view is null! Have you specified a content view in your layout xml file?" + " You have to give your content View the id R.id.contentView");
        }

        if (emptyView == null) {
            throw new NullPointerException("emptyView view is null! Have you specified an emptyView view in your layout xml file?" + " You have to give your emptyView View the id R.id.emptyView");
        }

        if (errorView == null) {
            throw new NullPointerException("Error view is null! Have you specified an error view in your layout xml file?" + " You have to give your error View the id R.id.errorView");
        }

        errorView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onErrorViewClicked();
            }
        });
    }

    @NonNull
    protected View createContentView(View view) {
        return view.findViewById(com.util_code.R.id.contentView);
    }

    @NonNull
    protected View createLoadingView(View view) {
        return view.findViewById(com.util_code.R.id.loadingView);
    }

    protected View createEmptyView(View view) {
        return view.findViewById(com.util_code.R.id.emptyView);
    }

    @NonNull
    protected View createErrorView(View view) {
        return view.findViewById(com.util_code.R.id.errorView);
    }

    protected abstract void onErrorViewClicked();

    public void showLoading(boolean pullToRefresh) {
        if (!pullToRefresh) {
            animateLoadingViewIn();
        }
        // otherwise the pull to refresh widget will already display a loading animation
    }

    protected void animateLoadingViewIn() {
        BaseLceAnimator.showLoading(loadingView, contentView, emptyView, errorView);
    }

    public void showContent() {
        animateContentViewIn();
    }

    protected void animateContentViewIn() {
        BaseLceAnimator.showContent(loadingView, contentView, emptyView, errorView);
    }

    public void showError(boolean pullToRefresh) {
        if (!pullToRefresh) {
            animateErrorViewIn();
        }
    }

    protected void animateErrorViewIn() {
        BaseLceAnimator.showErrorView(loadingView, contentView, emptyView, errorView);
    }
}
