package com.common.config;

import androidx.annotation.Keep;

@Keep
public class Constant {

    /**
     * api status code
     */
    public static final int WEB_SUCCESS = 200;

    /**
     * 登录超时错误码
     */
    public static final String ERROR_LOGOUT = "40008201";

    /**
     * long time
     */
    public static final long TIME_ONE_SECOND = 1000;
    public static final long TIME_ONE_MINITUE = 60000;
    public static final long TIME_ONE_HOUR = 3600000;

    public static final int VALIDETE_CODE_WATING_TIME_MAX = 60;

    public static final String PUSH_TYPE_TEXT = "01";
    public static final String PUSH_TYPE_URL = "02";
    public static final String PUSH_TYPE_ACTIVITY = "03";

    public static String ANDROIDVERSION = "ANDROID_CRM_V2_VERSION";

    public static String BUGLY_APP_ID = "b214b3e98f";

    /**
     * 支付
     */
    public static final String PAY_APP_ID = "20191031101818409480";
    public static final String PAY_APP_SECRET = "2e7fac88aab7c5a66ff9a1fc991dd087";
    public static final String PAY_APP_METHOD = "openapi.payment.order.swipe";
    public static final String PAY_APP_QUERY = "openapi.payment.order.query";

}