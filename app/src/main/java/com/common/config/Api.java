package com.common.config;

/**
 * Created by hdb on 2018/4/18.
 */

public class Api {

    //https://json-card.oss-cn-hangzhou.aliyuncs.com/menu.json
//    public final static String BASE_URL = "https://json-card.oss-cn-hangzhou.aliyuncs.com/";
//    public final static String BASE_URL = "http://192.168.1.78:9020/";
//    public final static String BASE_URL = "http://192.168.1.128:9020/";
//    public final static String BASE_URL = "http://dev-atm.lianglife.com/";
//    public final static String BASE_URL = "http://rc-atm.lianglife.com/";
    public final static String BASE_URL = "https://atm.lianglife.com/";

    public final static String OTHER_URL = "https://shq-api.51fubei.com/";

    public final static String PAY_URL = "https://shq-api.51fubei.com/";

    /**
     * 付呗 http://docs.51fubei.com/open-api/
     * 测试环境： https://shq-api-test.51fubei.com/gateway
     * 正式环境： https://shq-api.51fubei.com/gateway
     *
     */

    public final static String QR_CODE_DATA = "http://weixin.qq.com/r/AxxVTTbEpmvZrXIu90kR";


}
