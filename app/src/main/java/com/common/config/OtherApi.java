package com.common.config;

import com.common.retrofit.json.Data;
import com.lsz.atm.json.OrderBean;
import com.lsz.atm.json.OrderPayBean;
import com.lsz.atm.json.PayRequestBean;

import java.util.Map;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface OtherApi {

    @Multipart
    @POST("file/upload.do")
    Observable<ResponseBody> uploadFileWithPartMap(@PartMap() Map<String, RequestBody> partMap, @Part MultipartBody.Part file);

    /**
     * 支付
     * @return
     */
    @POST("gateway")
    Observable<Data<OrderPayBean>> payOrderSwipe(@Body PayRequestBean payRequest);
}
