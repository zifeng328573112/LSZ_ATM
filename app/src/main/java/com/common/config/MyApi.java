package com.common.config;

import com.common.retrofit.json.Data;
import com.lsz.atm.json.AndroidGitLib;
import com.lsz.atm.json.CheckUpdate;
import com.lsz.atm.json.GitUser;
import com.lsz.atm.json.LoginEntity;
import com.lsz.atm.json.MenuBean;
import com.lsz.atm.json.OrderBean;
import com.lsz.atm.json.OrderPayBean;
import com.lsz.atm.json.PayRequestBean;
import com.lsz.atm.json.StartOrderBean;
import com.lsz.atm.json.VIPMemberBean;
import com.lsz.atm.json.commodity.CommodityDataBean;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MyApi {

    @FormUrlEncoded
    @POST("v1/trade/version/getVersionInfo")
    Observable<Data<CheckUpdate>> checkUpdate(@FieldMap Map<String, String> updateMap);

    @GET("https://api.github.com/search/repositories")
    Observable<AndroidGitLib> getUsersPagesByPage(@Query("q") String keyword, @Query("page") int page, @Query("per_page") int perPage);

    //    https://json-card.oss-cn-hangzhou.aliyuncs.com/apk_update.json
    @GET("http://devapk.lianglife.com/atm/apk_update.json")
    Observable<Data<CheckUpdate>> checkUpdates();


    @POST("clerk/login")
    Observable<Data<LoginEntity>> postLogin(@Body RequestBody requestBody);

    /**
     * 首页菜单list
     *
     * @return
     */
    @GET("scenario/get.json")
//    @GET("scenario.json")
    Observable<Data<List<MenuBean>>> getMenuList();

    /**
     * 首页商品list
     *
     * @return
     */
    @GET("proposal/get.json")
//    @GET("commodity.json")
    Observable<Data<List<CommodityDataBean>>> getCommodityList();

    @GET("homePage/get.json")
    Observable<Data<StartOrderBean>> getHomePage();

    /**
     * 创建订单
     *
     * @return
     */
    @POST("order/create.json")
    Observable<Data<OrderBean>> postOrderCreate(@Body RequestBody requestBody);

    /**
     * 扫码优惠券
     * @param couponCode
     * @return
     */
    @GET("coupon/get.json")
    Observable<Data<String>> getCouponGet(@Query("couponCode") String couponCode);

    /**
     * 支付
     *
     * @return
     */
    @POST("https://shq-api.51fubei.com/gateway")
    Observable<Data<OrderPayBean>> payOrderSwipe(@Body PayRequestBean payRequest);

    /**
     * 订单查询接口
     *
     * @return
     */
    @POST("https://shq-api.51fubei.com/gateway")
    Observable<Data<OrderPayBean>> payOrderQuery(@Body PayRequestBean payRequest);

    /**
     * 消息通知
     *
     * @param requestBody
     * @return
     */
    @POST("order/pay-notice.json")
    Observable<Data<String>> payOrderNotice(@Body RequestBody requestBody);


    /**
     * 查询订单状态
     *
     * @param orderSeq
     * @return
     */
    @GET("order/paid.json")
    Observable<Data<String>> getOrderStatus(@Query("orderSeq") String orderSeq);

    /**
     * 获取会员码
     *
     * @param id
     * @return
     */
    @GET("http://192.168.1.128:8080/buyer/member/{id}")
    Observable<Data<VIPMemberBean>> getMember(@Path("id") String id);
}
