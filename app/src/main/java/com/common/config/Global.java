package com.common.config;

import com.lsz.atm.json.ATMBean;
import com.lsz.atm.json.LoginEntity;
import com.lsz.atm.json.MenuBean;
import com.lsz.atm.json.OrderNumberBean;
import com.lsz.atm.json.commodity.CommodityDataBean;
import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.List;

public class Global {

    private static String PREFERENCE_KEY_LOGIN_INFO = "login_info";
    private static String PREFERENCE_KEY_USER_INFO = "user_info";
    private static String PREFERENCE_KEY_MENU_DATA = "menu_data";
    private static String PREFERENCE_KEY_COMMODITY_DATA = "commodity_data";
    private static String PREFERENCE_KEY_ORDER_TOTAL_NUMBER = "order_total_number";

    public static LoginEntity getLoginBean() {
        return Hawk.get(PREFERENCE_KEY_LOGIN_INFO, new LoginEntity());
    }

    public static void setLoginBean(LoginEntity atmBean) {
        Hawk.put(PREFERENCE_KEY_LOGIN_INFO, atmBean);
    }

    public static ATMBean getAtmBean() {
        return Hawk.get(PREFERENCE_KEY_USER_INFO, new ATMBean());
    }

    public static void setAtmBean(ATMBean atmBean) {
        Hawk.put(PREFERENCE_KEY_USER_INFO, atmBean);
    }

    public static List<MenuBean> getMenuList(){
        return Hawk.get(PREFERENCE_KEY_MENU_DATA, new ArrayList<>());
    }

    public static void setMenuList(List<MenuBean> menuList){
        Hawk.put(PREFERENCE_KEY_MENU_DATA, menuList);
    }

    public static List<CommodityDataBean> getCommodityList(){
        return Hawk.get(PREFERENCE_KEY_COMMODITY_DATA, new ArrayList<>());
    }

    public static void setCommodityList(List<CommodityDataBean> commodityList){
        Hawk.put(PREFERENCE_KEY_COMMODITY_DATA, commodityList);
    }

    public static OrderNumberBean getOrderNumber(){
        return Hawk.get(PREFERENCE_KEY_ORDER_TOTAL_NUMBER, new OrderNumberBean());
    }

    public static void setOrderNumber(OrderNumberBean commodityList){
        Hawk.put(PREFERENCE_KEY_ORDER_TOTAL_NUMBER, commodityList);
    }

    public static void loginOutclear() {
        setLoginBean(new LoginEntity());
        setAtmBean(new ATMBean());
        setMenuList(new ArrayList<>());
        setCommodityList(new ArrayList<>());
        setOrderNumber(new OrderNumberBean());
    }

}
