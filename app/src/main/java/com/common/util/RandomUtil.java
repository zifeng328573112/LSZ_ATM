package com.common.util;

import java.util.Random;

public class RandomUtil {

    public static int RandomNumber() {
        int min = 1;    //定义随机数的最小值
        int max = 1000000;    //定义随机数的最大值
        //产生一个2~100的数
        int s = (int) min + (int) (Math.random() * (max - min));
        if (s % 2 == 0)    //如果是偶数就输出
            return s;
        else    //如果是奇数就加1后输出
            return s + 1;
    }

    public static String getRandomNumber(){
        String sources = "0a1b2c3d4e5f6g7h8i9jklmopqrstuvwxyz"; // 加上一些字母，就可以生成pc站的验证码了
        Random rand = new Random();
        StringBuffer flag = new StringBuffer();
        for (int j = 0; j < 6; j++) {
            flag.append(sources.charAt(rand.nextInt(9)) + "");
        }
        return flag.toString();
    }

}
