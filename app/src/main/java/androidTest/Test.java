package androidTest;

import android.os.Build;
import android.util.Base64;

import androidx.annotation.RequiresApi;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lsz.atm.viewutil.BigDecimalUtils;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

public class Test {

    public static class R<T> {
        private boolean success = true;
        private String message;
        private String code;
        private T data;

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public T getData() {
            return data;
        }

        public void setData(T data) {
            this.data = data;
        }
    }

    public static class Data {
        private String name;
        private String phone;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static void main(String[] args) throws Exception {
        String str = "123456";
//base64编码
//String strBase64 = new String(Base64.encode(str.getBytes(), Base64.DEFAULT));
//        String strBase64 = Base64.encodeToString(str.getBytes(), Base64.DEFAULT);
        String strBase64 = Base64.encodeToString(str.getBytes(), Base64.NO_WRAP);
        System.out.print(strBase64);
//base64解码
//        String str2 = new String(Base64.decode(strBase64.getBytes(), Base64.DEFAULT));
//        System.out.print(strBase64);
    }

    public static String getRandomNumber(){
        String sources = "0a1b2c3d4e5f6g7h8i9jklmopqrstuvwxyz"; // 加上一些字母，就可以生成pc站的验证码了
        Random rand = new Random();
        StringBuffer flag = new StringBuffer();
        for (int j = 0; j < 6; j++) {
            flag.append(sources.charAt(rand.nextInt(9)) + "");
        }
        return flag.toString();
    }

    /**
     * 如果time1早于time2返回true，否则返回false  
     * @param time1
     * @return
     * @throws ParseException
     */
    public static boolean compare(String time1) throws ParseException {
        //如果想比较日期则写成"yyyy-MM-dd"就可以了  
        SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
        //将字符串形式的时间转化为Date类型的时间  
        Date a = sdf.parse(time1);
        String format = sdf.format(new Date());
        Date b = sdf.parse(format);
        //Date类的一个方法，如果a早于b返回true，否则返回false  
        return a.before(b);
    }



}
