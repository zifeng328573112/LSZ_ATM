# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in E:\Android Eclipse\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

#标识符压缩为更短名称
#资源压缩移除未使用资源
#代码压缩移除未使用代码
#keep——保留所有匹配类规范的类和方法
#keepclassmembers——类的指定成员变量将被保护
#keepclasseswithmembers——拥有指定成员的类将被保护，根据类成员确定一些将要被保护的类

# Exceptions, Signature, Deprecated, SourceFile, SourceDir, LinepublicberTable, LocalVariableTable,
# LocalVariableTypeTable, Synthetic, EnclosingMethod, RuntimeVisibleAnnotations, RuntimeInvisibleAnnotations,
# RuntimeVisibleParameterAnnotations, RuntimeInvisibleParameterAnnotations, and AnnotationDefault
# 进行发送CrashReport,定位问题所在行数
# -keepattributes **
-keepattributes Signature
-keepattributes SourceFile,LineNumberTable
# -ignorewarnings

# 保持哪些类不被混淆
-keep public class * extends android.app.Fragment
-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference
-keep public class com.android.vending.licensing.ILicensingService

# support
-keep class android.support.**{*;}
-dontwarn android.support.**
-keep class android.arch.**{*;}
-dontwarn android.arch.**

# ProGuard configurations for squareup
-keep class retrofit2.** { *; }
-keep class com.squareup.** { *; }
-keep class okhttp3.** { *; }
-keep class okio.** { *; }
-dontwarn retrofit2.**
-dontwarn com.squareup.**
-dontwarn okhttp3.**
-dontwarn okio.**
# End squareup

# EventBus
-keepattributes *Annotation*
-keepclassmembers class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }
# End EventBus

# Tencent、Baidu、Sina、Alipay
-keep class com.tencent.**{*;}
-dontwarn com.tencent.**
-keep class com.baidu.**{*;}
-dontwarn com.baidu.**
-keep class com.sina.**{*;}
-dontwarn com.sina.**
-keep class com.alipay.**{*;}
-dontwarn com.alipay.**
# End Tencent、Baidu、Alipay

# Persistentcookiejar
-dontwarn com.franmontiel.persistentcookiejar.**
-keep class com.franmontiel.persistentcookiejar.**
-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    !static !transient <fields>;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}
# End Persistentcookiejar

# Glide
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.module.AppGlideModule
-keep public enum com.bumptech.glide.load.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}
-dontwarn com.bumptech.glide.load.resource.bitmap.VideoDecoder
# for DexGuard only
# VideoDecoder uses API >= 27 APIs which may cause proguard warnings even though the newer
# APIs won’t be called on devices with older versions of Android.
#-keepresourcexmlelements manifest/application/meta-data@value=GlideModule
# End Glide

# JPush
-dontoptimize
-dontpreverify

-dontwarn cn.jpush.**
-keep class cn.jpush.** { *; }
-keep class * extends cn.jpush.android.helpers.JPushMessageReceiver { *; }
-dontwarn cn.jiguang.**
-keep class cn.jiguang.** { *; }
# End JPush

# Bugly
-dontwarn com.tencent.bugly.**
-keep public class com.tencent.bugly.**{*;}
# End Bugly

# GreenDao
-keepclassmembers class * extends org.greenrobot.greendao.AbstractDao {
    public static java.lang.String TABLENAME;
}
-keep class **$Properties

# If you do not use SQLCipher:
-dontwarn org.greenrobot.greendao.database.**
# If you do not use Rx:
-dontwarn rx.**

# Only required if you use AsyncExecutor
-keepclassmembers class * extends org.greenrobot.eventbus.util.ThrowableFailureEvent {
    <init>(Java.lang.Throwable);
}
# End GreenDao

################gson###############
-keepattributes Signature
-keepattributes *Annotation*
-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.stream.** { *; }
# Application classes that will be serialized/deserialized over Gson
-keep class com.sunloto.shandong.bean.** { *; }

# Zxing
-dontwarn com.google.zxing.**
-keep public class com.google.zxing.**{*;}
# End Zxing

# AMap
-keep public class com.amap.api.**{*;}
-dontwarn com.amap.api.**
-keep public class com.autonavi.amap.mapcore2d.**{*;}
-dontwarn com.autonavi.amap.mapcore2d.**
-keep public class com.autonavi.aps.amapapi.model.**{*;}
-dontwarn com.autonavi.aps.amapapi.model.**
-keep public class com.loc.**{*;}
-dontwarn com.loc.**
-keep public class com.amap.api.services.**{*;}
-dontwarn com.amap.api.services.**
-keep   class com.amap.api.maps.**{*;}
-keep   class com.autonavi.**{*;}
-keep   class com.amap.api.trace.**{*;}
# End AMap

# Banner
-keep class com.youth.banner.** {*;}
# End Banner

# Renderscript Support
-keepclasseswithmembernames class * {
    native <methods>;
}
-keep class android.support.v8.renderscript.** { *; }
# End Renderscript Support

# 第三方R资源文件
-keep class **.R$* {*;}

#自己项目特殊处理代码
#保留一个完整的包
-keep class com.lsz.atm.db.** {
    *;
 }

#保留一个完整的类
#-keep class com.android.module.http.PreferencesCookieStore { *;}
